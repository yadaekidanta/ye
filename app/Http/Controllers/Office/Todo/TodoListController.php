<?php

namespace App\Http\Controllers\Office\Todo;

use App\Http\Controllers\Controller;
use App\Models\Todo\TodoList;
use Illuminate\Http\Request;

class TodoListController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.todo.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(TodoList $todoList)
    {
        //
    }
    public function edit(TodoList $todoList)
    {
        //
    }
    public function update(Request $request, TodoList $todoList)
    {
        //
    }
    public function destroy(TodoList $todoList)
    {
        //
    }
    public function list()
    {
        $collection = TodoList::get();
        return view('pages.office.todo.list',compact('collection'));
    }
}
