<?php

namespace App\Http\Controllers\Office\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Memo;
use Illuminate\Http\Request;

class MemoController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Memo $memo)
    {
        //
    }
    public function edit(Memo $memo)
    {
        //
    }
    public function update(Request $request, Memo $memo)
    {
        //
    }
    public function destroy(Memo $memo)
    {
        //
    }
    public function list()
    {
        $collection = Memo::get();
        return view('pages.office.memo.list',compact('collection'));
    }
}
