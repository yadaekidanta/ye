<div class="app-page-loader flex-column">
    <img alt="Logo" class="mh-75px" src="{{asset('img/icon.png')}}" />
    <div class="d-flex align-items-center mt-5">
        <span class="spinner-border text-primary" role="status"></span>
        <span class="text-muted fs-6 fw-semibold ms-5">Loading...</span>
    </div>
</div>