<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.main');
        }
        return view('pages.office.theme');
    }
    public function list()
    {
        return view('pages.office.documentation.list');
    }
    public function install(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.install.main');
        }
        return view('pages.office.theme');
    }
    public function install_list()
    {
        return view('pages.office.documentation.install.list');
    }
    public function utility(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.utilities.main');
        }
        return view('pages.office.theme');
    }
    public function utility_list()
    {
        return view('pages.office.documentation.utilities.list');
    }
    public function component(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.component.main');
        }
        return view('pages.office.theme');
    }
    public function component_list()
    {
        return view('pages.office.documentation.component.list');
    }
    public function plugin(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.plugin.main');
        }
        return view('pages.office.theme');
    }
    public function plugin_list()
    {
        return view('pages.office.documentation.plugin.list');
    }
    public function icon(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.icon.main');
        }
        return view('pages.office.theme');
    }
    public function icon_list()
    {
        return view('pages.office.documentation.icon.list');
    }
    public function illustration(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.documentation.illustration.main');
        }
        return view('pages.office.theme');
    }
    public function illustration_list()
    {
        return view('pages.office.documentation.illustration.list');
    }
}
