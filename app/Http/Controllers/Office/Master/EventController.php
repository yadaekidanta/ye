<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.master.calendar.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Event $event)
    {
        //
    }
    public function edit(Event $event)
    {
        //
    }
    public function update(Request $request, Event $event)
    {
        //
    }
    public function destroy(Event $event)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Event::get();
        return view('pages.office.master.calendar.list',compact('collection'));
    }
}
