<script>
    if ( document.documentElement ) {
        const defaultThemeMode = "system";
        const name = document.body.getAttribute("data-kt-name");
        let themeMode = localStorage.getItem("kt_" + ( name !== null ? name + "_" : "" ) + "theme_mode_value");
        if ( themeMode === null ) {
            if ( defaultThemeMode === "system" ) {
                themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
            } else {
                themeMode = defaultThemeMode;
            }
        }
        document.documentElement.setAttribute("data-theme", themeMode);
    }
</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('keenthemes/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/scripts.bundle.js')}}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Vendors Javascript(used by this page)-->
@auth
<script src="{{asset('keenthemes/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/custom/documentation/documentation.js')}}"></script>
<script src="{{asset('keenthemes/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
@endauth
<!--end::Vendors Javascript-->
<!--begin::Custom Javascript(used by this page)-->
<script src="{{asset('js/flatpickr.js')}}"></script>
<script src="{{asset('js/language.js')}}"></script>
@guest
<script src="{{asset('js/auth.js')}}"></script>
@endguest
@auth
<script src="{{asset('js/plugin-office.js')}}"></script>
<script src="{{asset('js/method-office.js')}}"></script>
@endauth
<!--end::Custom Javascript-->