<div class="card card-docs flex-row-fluid mb-2">
    <!--begin::Card Body-->
    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
        <!--begin::Section-->
        <div class="pb-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="overview" data-kt-scroll-offset="50">
            <a href="#overview"></a>Overview</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Stepper is an exclusive plugin of Metronic for handling forms with multiple steps.</div>
            <!--end::Block-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="usage" data-kt-scroll-offset="50">
            <a href="#usage"></a>Usage</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Stepper's CSS and Javascript files are bundled in the global style and scripts bundles and are globally included in all pages:</div>
            <!--end::Block-->
            <!--begin::Code-->
            <div class="py-5">
                <!--begin::Highlight-->
                <div class="highlight">
                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                    <div class="highlight-code">
                        <pre class="language-html">
<code class="language-html">&lt;link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/&gt;
&lt;script src="assets/js/scripts.bundle.js"&gt;&lt;/script&gt;</code>
</pre>
                    </div>
                </div>
                <!--end::Highlight-->
            </div>
            <!--end::Code-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="initialization" data-kt-scroll-offset="50">
            <a href="#initialization"></a>Initialization</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">
                <ul class="py-0">
                    <li class="py-2">Stepper does not come with its own custom CSS and it uses global input controls and related elements.</li>
                    <li class="py-2">Stepper elements are defined with
                    <code>data-kt-stepper-*</code>HTML attributes.</li>
                    <li class="py-2">Stepper instances can be created programmatically. See below for more info.</li>
                </ul>
            </div>
            <!--end::Block-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="basic" data-kt-scroll-offset="50">
            <a href="#basic"></a>Basic</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Use
            <code>.stepper</code>and
            <code>.stepper-pills</code>classes to enable a basic form steps with title, number and icon elements:</div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <div class="rounded border p-10 p-lg-20">
                    <!--begin::Stepper-->
                    <div class="stepper stepper-pills" id="kt_stepper_example_basic">
                        <!--begin::Nav-->
                        <div class="stepper-nav flex-center flex-wrap mb-10">
                            <!--begin::Step 1-->
                            <div class="stepper-item mx-8 my-4 current" data-kt-stepper-element="nav">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">1</span>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 1</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 1-->
                            <!--begin::Step 2-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">2</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 2</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 2-->
                            <!--begin::Step 3-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">3</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 3</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 3-->
                            <!--begin::Step 4-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">4</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 4</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 4-->
                        </div>
                        <!--end::Nav-->
                        <!--begin::Form-->
                        <form class="form w-lg-500px mx-auto" novalidate="novalidate">
                            <!--begin::Group-->
                            <div class="mb-5">
                                <!--begin::Step 1-->
                                <div class="flex-column current" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 1</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 3</label>
                                        <!--end::Label-->
                                        <!--begin::Switch-->
                                        <label class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" checked="checked" value="1" />
                                            <span class="form-check-label">Switch</span>
                                        </label>
                                        <!--end::Switch-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 1</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""></textarea>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 3</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" checked="checked" type="checkbox" value="1" />
                                            <span class="form-check-label">Checkbox</span>
                                        </label>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 3</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input3" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                            </div>
                            <!--end::Group-->
                            <!--begin::Actions-->
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="me-2">
                                    <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">Back</button>
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Wrapper-->
                                <div>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                        <span class="indicator-label">Submit</span>
                                        <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next">Continue</button>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Stepper-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Code-->
            <div class="py-5">
                <!--begin::Highlight-->
                <div class="highlight">
                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e2e547550" role="tab">JAVASCRIPT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e2e547556" role="tab">HTML</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="kt_highlight_62c3e2e547550" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-javascript">
<code class="language-javascript">// Stepper lement
var element = document.querySelector("#kt_stepper_example_basic");

// Initialize Stepper
var stepper = new KTStepper(element);

// Handle next step
stepper.on("kt.stepper.next", function (stepper) {
stepper.goNext(); // go next step
});

// Handle previous step
stepper.on("kt.stepper.previous", function (stepper) {
stepper.goPrevious(); // go previous step
});</code>
</pre>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_highlight_62c3e2e547556" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-html" style="height:400px">
<code class="language-html">&lt;!--begin::Stepper--&gt;
&lt;div class="stepper stepper-pills" id="kt_stepper_example_basic"&gt;
&lt;!--begin::Nav--&gt;
&lt;div class="stepper-nav flex-center flex-wrap mb-10"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="stepper-item mx-8 my-4 current" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;1&lt;/span&gt;
&lt;/div&gt;
&lt;!--end::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 1
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 1--&gt;

&lt;!--begin::Step 2--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;2&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 2
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 2--&gt;

&lt;!--begin::Step 3--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;3&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 3
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 3--&gt;

&lt;!--begin::Step 4--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;4&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 4
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 4--&gt;
&lt;/div&gt;
&lt;!--end::Nav--&gt;

&lt;!--begin::Form--&gt;
&lt;form class="form w-lg-500px mx-auto" novalidate="novalidate" id="kt_stepper_example_basic_form"&gt;
&lt;!--begin::Group--&gt;
&lt;div class="mb-5"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column current" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Switch--&gt;
&lt;label class="form-check form-switch form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" type="checkbox" checked="checked" value="1"/&gt;
&lt;span class="form-check-label"&gt;
Switch
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Switch--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""&gt;&lt;/textarea&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;label class="form-check form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" checked="checked" type="checkbox" value="1"/&gt;
&lt;span class="form-check-label"&gt;
Checkbox
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 3
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input3" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;
&lt;/div&gt;
&lt;!--end::Group--&gt;

&lt;!--begin::Actions--&gt;
&lt;div class="d-flex flex-stack"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="me-2"&gt;
&lt;button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous"&gt;
Back
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Wrapper--&gt;
&lt;div&gt;
&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="submit"&gt;
&lt;span class="indicator-label"&gt;
Submit
&lt;/span&gt;
&lt;span class="indicator-progress"&gt;
Please wait... &lt;span class="spinner-border spinner-border-sm align-middle ms-2"&gt;&lt;/span&gt;
&lt;/span&gt;
&lt;/button&gt;

&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="next"&gt;
Continue
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;
&lt;/div&gt;
&lt;!--end::Actions--&gt;
&lt;/form&gt;
&lt;!--end::Form--&gt;
&lt;/div&gt;
&lt;!--end::Stepper--&gt;</code>
</pre>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Highlight-->
            </div>
            <!--end::Code-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="vertical" data-kt-scroll-offset="50">
            <a href="#vertical"></a>Vertical</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Use
            <code>.stepper-column</code>class to enable a stepper with vertical navigation layout:</div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <div class="rounded border p-10 p-lg-20">
                    <!--begin::Stepper-->
                    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-lg-row" id="kt_stepper_example_vertical">
                        <!--begin::Aside-->
                        <div class="d-flex flex-row-auto w-100 w-lg-300px">
                            <!--begin::Nav-->
                            <div class="stepper-nav flex-cente">
                                <!--begin::Step 1-->
                                <div class="stepper-item me-5 current" data-kt-stepper-element="nav">
                                    <!--begin::Wrapper-->
                                    <div class="stepper-wrapper d-flex align-items-center">
                                        <!--begin::Icon-->
                                        <div class="stepper-icon w-40px h-40px">
                                            <i class="stepper-check fas fa-check"></i>
                                            <span class="stepper-number">1</span>
                                        </div>
                                        <!--end::Icon-->
                                        <!--begin::Label-->
                                        <div class="stepper-label">
                                            <h3 class="stepper-title">Step 1</h3>
                                            <div class="stepper-desc">Description</div>
                                        </div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Line-->
                                    <div class="stepper-line h-40px"></div>
                                    <!--end::Line-->
                                </div>
                                <!--end::Step 1-->
                                <!--begin::Step 2-->
                                <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                    <!--begin::Wrapper-->
                                    <div class="stepper-wrapper d-flex align-items-center">
                                        <!--begin::Icon-->
                                        <div class="stepper-icon w-40px h-40px">
                                            <i class="stepper-check fas fa-check"></i>
                                            <span class="stepper-number">2</span>
                                        </div>
                                        <!--begin::Icon-->
                                        <!--begin::Label-->
                                        <div class="stepper-label">
                                            <h3 class="stepper-title">Step 2</h3>
                                            <div class="stepper-desc">Description</div>
                                        </div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Line-->
                                    <div class="stepper-line h-40px"></div>
                                    <!--end::Line-->
                                </div>
                                <!--end::Step 2-->
                                <!--begin::Step 3-->
                                <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                    <!--begin::Wrapper-->
                                    <div class="stepper-wrapper d-flex align-items-center">
                                        <!--begin::Icon-->
                                        <div class="stepper-icon w-40px h-40px">
                                            <i class="stepper-check fas fa-check"></i>
                                            <span class="stepper-number">3</span>
                                        </div>
                                        <!--begin::Icon-->
                                        <!--begin::Label-->
                                        <div class="stepper-label">
                                            <h3 class="stepper-title">Step 3</h3>
                                            <div class="stepper-desc">Description</div>
                                        </div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Line-->
                                    <div class="stepper-line h-40px"></div>
                                    <!--end::Line-->
                                </div>
                                <!--end::Step 3-->
                                <!--begin::Step 4-->
                                <div class="stepper-item me-5" data-kt-stepper-element="nav">
                                    <!--begin::Wrapper-->
                                    <div class="stepper-wrapper d-flex align-items-center">
                                        <!--begin::Icon-->
                                        <div class="stepper-icon w-40px h-40px">
                                            <i class="stepper-check fas fa-check"></i>
                                            <span class="stepper-number">4</span>
                                        </div>
                                        <!--begin::Icon-->
                                        <!--begin::Label-->
                                        <div class="stepper-label">
                                            <h3 class="stepper-title">Step 4</h3>
                                            <div class="stepper-desc">Description</div>
                                        </div>
                                        <!--end::Label-->
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Step 4-->
                            </div>
                            <!--end::Nav-->
                        </div>
                        <!--begin::Content-->
                        <div class="flex-row-fluid">
                            <!--begin::Form-->
                            <form class="form w-lg-500px mx-auto" novalidate="novalidate">
                                <!--begin::Group-->
                                <div class="mb-5">
                                    <!--begin::Step 1-->
                                    <div class="flex-column current" data-kt-stepper-element="content">
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 1</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 2</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 3</label>
                                            <!--end::Label-->
                                            <!--begin::Switch-->
                                            <label class="form-check form-switch form-check-custom form-check-solid">
                                                <input class="form-check-input" type="checkbox" checked="checked" value="1" />
                                                <span class="form-check-label">Switch</span>
                                            </label>
                                            <!--end::Switch-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                    <!--begin::Step 1-->
                                    <!--begin::Step 1-->
                                    <div class="flex-column" data-kt-stepper-element="content">
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 1</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 2</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""></textarea>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Example Label 3</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <label class="form-check form-check-custom form-check-solid">
                                                <input class="form-check-input" checked="checked" type="checkbox" value="1" />
                                                <span class="form-check-label">Checkbox</span>
                                            </label>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                    <!--begin::Step 1-->
                                    <!--begin::Step 1-->
                                    <div class="flex-column" data-kt-stepper-element="content">
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label d-flex align-items-center">
                                                <span class="required">Input 1</span>
                                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                            </label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Input 2</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                    <!--begin::Step 1-->
                                    <!--begin::Step 1-->
                                    <div class="flex-column" data-kt-stepper-element="content">
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label d-flex align-items-center">
                                                <span class="required">Input 1</span>
                                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                            </label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Input 2</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                        <!--begin::Input group-->
                                        <div class="fv-row mb-10">
                                            <!--begin::Label-->
                                            <label class="form-label">Input 3</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <input type="text" class="form-control form-control-solid" name="input3" placeholder="" value="" />
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                    <!--begin::Step 1-->
                                </div>
                                <!--end::Group-->
                                <!--begin::Actions-->
                                <div class="d-flex flex-stack">
                                    <!--begin::Wrapper-->
                                    <div class="me-2">
                                        <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">Back</button>
                                    </div>
                                    <!--end::Wrapper-->
                                    <!--begin::Wrapper-->
                                    <div>
                                        <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                            <span class="indicator-label">Submit</span>
                                            <span class="indicator-progress">Please wait...
                                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                        </button>
                                        <button type="button" class="btn btn-primary" data-kt-stepper-action="next">Continue</button>
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Actions-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Stepper-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Code-->
            <div class="py-5">
                <!--begin::Highlight-->
                <div class="highlight">
                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e2e548028" role="tab">JAVASCRIPT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e2e54802d" role="tab">HTML</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="kt_highlight_62c3e2e548028" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-javascript">
<code class="language-javascript">// Stepper lement
var element = document.querySelector("#kt_stepper_example_vertical");

// Initialize Stepper
var stepper = new KTStepper(element);

// Handle next step
stepper.on("kt.stepper.next", function (stepper) {
stepper.goNext(); // go next step
});

// Handle previous step
stepper.on("kt.stepper.previous", function (stepper) {
stepper.goPrevious(); // go previous step
});</code>
</pre>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_highlight_62c3e2e54802d" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-html" style="height:400px">
<code class="language-html">&lt;!--begin::Stepper--&gt;
&lt;div class="stepper stepper-pills stepper-column d-flex flex-column flex-lg-row" id="kt_stepper_example_vertical"&gt;
&lt;!--begin::Aside--&gt;
&lt;div class="d-flex flex-row-auto w-100 w-lg-300px"&gt;
&lt;!--begin::Nav--&gt;
&lt;div class="stepper-nav flex-cente"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="stepper-item me-5 current" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;1&lt;/span&gt;
&lt;/div&gt;
&lt;!--end::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 1
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 1--&gt;

&lt;!--begin::Step 2--&gt;
&lt;div class="stepper-item me-5" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;2&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 2
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 2--&gt;

&lt;!--begin::Step 3--&gt;
&lt;div class="stepper-item me-5" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;3&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 3
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 3--&gt;

&lt;!--begin::Step 4--&gt;
&lt;div class="stepper-item me-5" data-kt-stepper-element="nav"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;4&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 4
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;
&lt;/div&gt;
&lt;!--end::Step 4--&gt;
&lt;/div&gt;
&lt;!--end::Nav--&gt;
&lt;/div&gt;

&lt;!--begin::Content--&gt;
&lt;div class="flex-row-fluid"&gt;
&lt;!--begin::Form--&gt;
&lt;form class="form w-lg-500px mx-auto" novalidate="novalidate"&gt;
&lt;!--begin::Group--&gt;
&lt;div class="mb-5"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column current" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Switch--&gt;
&lt;label class="form-check form-switch form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" type="checkbox" checked="checked" value="1"/&gt;
&lt;span class="form-check-label"&gt;
    Switch
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Switch--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""&gt;&lt;/textarea&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;label class="form-check form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" checked="checked" type="checkbox" value="1"/&gt;
&lt;span class="form-check-label"&gt;
    Checkbox
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 3
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input3" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;
&lt;/div&gt;
&lt;!--end::Group--&gt;

&lt;!--begin::Actions--&gt;
&lt;div class="d-flex flex-stack"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="me-2"&gt;
&lt;button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous"&gt;
Back
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Wrapper--&gt;
&lt;div&gt;
&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="submit"&gt;
&lt;span class="indicator-label"&gt;
Submit
&lt;/span&gt;
&lt;span class="indicator-progress"&gt;
Please wait... &lt;span class="spinner-border spinner-border-sm align-middle ms-2"&gt;&lt;/span&gt;
&lt;/span&gt;
&lt;/button&gt;

&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="next"&gt;
Continue
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;
&lt;/div&gt;
&lt;!--end::Actions--&gt;
&lt;/form&gt;
&lt;!--end::Form--&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Stepper--&gt;</code>
</pre>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Highlight-->
            </div>
            <!--end::Code-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="clickable-navigation" data-kt-scroll-offset="50">
            <a href="#clickable-navigation"></a>Clickable Navigation</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Add
            <code>data-kt-stepper-action="step"</code>attribute to Stepper navigation links to setup a Stepper with clickable navigation:</div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <div class="rounded border p-10 p-lg-20">
                    <!--begin::Stepper-->
                    <div class="stepper stepper-pills" id="kt_stepper_example_clickable">
                        <!--begin::Nav-->
                        <div class="stepper-nav flex-center flex-wrap mb-10">
                            <!--begin::Step 1-->
                            <div class="stepper-item mx-8 my-4 current" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">1</span>
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 1</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 1-->
                            <!--begin::Step 2-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">2</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 2</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 2-->
                            <!--begin::Step 3-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">3</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 3</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Line-->
                                <div class="stepper-line h-40px"></div>
                                <!--end::Line-->
                            </div>
                            <!--end::Step 3-->
                            <!--begin::Step 4-->
                            <div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <!--begin::Wrapper-->
                                <div class="stepper-wrapper d-flex align-items-center">
                                    <!--begin::Icon-->
                                    <div class="stepper-icon w-40px h-40px">
                                        <i class="stepper-check fas fa-check"></i>
                                        <span class="stepper-number">4</span>
                                    </div>
                                    <!--begin::Icon-->
                                    <!--begin::Label-->
                                    <div class="stepper-label">
                                        <h3 class="stepper-title">Step 4</h3>
                                        <div class="stepper-desc">Description</div>
                                    </div>
                                    <!--end::Label-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Step 4-->
                        </div>
                        <!--end::Nav-->
                        <!--begin::Form-->
                        <form class="form w-lg-500px mx-auto" novalidate="novalidate">
                            <!--begin::Group-->
                            <div class="mb-5">
                                <!--begin::Step 1-->
                                <div class="flex-column current" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 1</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 3</label>
                                        <!--end::Label-->
                                        <!--begin::Switch-->
                                        <label class="form-check form-switch form-check-custom form-check-solid">
                                            <input class="form-check-input" type="checkbox" checked="checked" value="1" />
                                            <span class="form-check-label">Switch</span>
                                        </label>
                                        <!--end::Switch-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 1</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""></textarea>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Example Label 3</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <label class="form-check form-check-custom form-check-solid">
                                            <input class="form-check-input" checked="checked" type="checkbox" value="1" />
                                            <span class="form-check-label">Checkbox</span>
                                        </label>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                                <!--begin::Step 1-->
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label d-flex align-items-center">
                                            <span class="required">Input 1</span>
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"></i>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input1" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 2</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input2" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <!--begin::Label-->
                                        <label class="form-label">Input 3</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid" name="input3" placeholder="" value="" />
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Input group-->
                                </div>
                                <!--begin::Step 1-->
                            </div>
                            <!--end::Group-->
                            <!--begin::Actions-->
                            <div class="d-flex flex-stack">
                                <!--begin::Wrapper-->
                                <div class="me-2">
                                    <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">Back</button>
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Wrapper-->
                                <div>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                        <span class="indicator-label">Submit</span>
                                        <span class="indicator-progress">Please wait...
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="next">Continue</button>
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Stepper-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Code-->
            <div class="py-5">
                <!--begin::Highlight-->
                <div class="highlight">
                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                    <ul class="nav nav-pills" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e2e548f0d" role="tab">JAVASCRIPT</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e2e548f16" role="tab">HTML</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="kt_highlight_62c3e2e548f0d" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-javascript">
<code class="language-javascript">// Stepper lement
var element = document.querySelector("#kt_stepper_example_clickable");

// Initialize Stepper
var stepper = new KTStepper(element);

// Handle navigation click
stepper.on("kt.stepper.click", function (stepper) {
stepper.goTo(stepper.getClickedStepIndex()); // go to clicked step
});

// Handle next step
stepper.on("kt.stepper.next", function (stepper) {
stepper.goNext(); // go next step
});

// Handle previous step
stepper.on("kt.stepper.previous", function (stepper) {
stepper.goPrevious(); // go previous step
});</code>
</pre>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="kt_highlight_62c3e2e548f16" role="tabpanel">
                            <div class="highlight-code">
                                <pre class="language-html" style="height:400px">
<code class="language-html">&lt;!--begin::Stepper--&gt;
&lt;div class="stepper stepper-pills" id="kt_stepper_example_clickable"&gt;
&lt;!--begin::Nav--&gt;
&lt;div class="stepper-nav flex-center flex-wrap mb-10"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="stepper-item mx-8 my-4 current" data-kt-stepper-element="nav" data-kt-stepper-action="step"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;1&lt;/span&gt;
&lt;/div&gt;
&lt;!--end::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 1
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 1--&gt;

&lt;!--begin::Step 2--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;2&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 2
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 2--&gt;

&lt;!--begin::Step 3--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;3&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 3
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Line--&gt;
&lt;div class="stepper-line h-40px"&gt;&lt;/div&gt;
&lt;!--end::Line--&gt;
&lt;/div&gt;
&lt;!--end::Step 3--&gt;

&lt;!--begin::Step 4--&gt;
&lt;div class="stepper-item mx-8 my-4" data-kt-stepper-element="nav" data-kt-stepper-action="step"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="stepper-wrapper d-flex align-items-center"&gt;
&lt;!--begin::Icon--&gt;
&lt;div class="stepper-icon w-40px h-40px"&gt;
&lt;i class="stepper-check fas fa-check"&gt;&lt;/i&gt;
&lt;span class="stepper-number"&gt;4&lt;/span&gt;
&lt;/div&gt;
&lt;!--begin::Icon--&gt;

&lt;!--begin::Label--&gt;
&lt;div class="stepper-label"&gt;
&lt;h3 class="stepper-title"&gt;
Step 4
&lt;/h3&gt;

&lt;div class="stepper-desc"&gt;
Description
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Label--&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;
&lt;/div&gt;
&lt;!--end::Step 4--&gt;
&lt;/div&gt;
&lt;!--end::Nav--&gt;

&lt;!--begin::Form--&gt;
&lt;form class="form w-lg-500px mx-auto" novalidate="novalidate" id="kt_stepper_example_basic_form"&gt;
&lt;!--begin::Group--&gt;
&lt;div class="mb-5"&gt;
&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column current" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Switch--&gt;
&lt;label class="form-check form-switch form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" type="checkbox" checked="checked" value="1"/&gt;
&lt;span class="form-check-label"&gt;
Switch
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Switch--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 1&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 2&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;textarea class="form-control form-control-solid" rows="3" name="input2" placeholder=""&gt;&lt;/textarea&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;Example Label 3&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;label class="form-check form-check-custom form-check-solid"&gt;
&lt;input class="form-check-input" checked="checked" type="checkbox" value="1"/&gt;
&lt;span class="form-check-label"&gt;
Checkbox
&lt;/span&gt;
&lt;/label&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;

&lt;!--begin::Step 1--&gt;
&lt;div class="flex-column" data-kt-stepper-element="content"&gt;
&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label d-flex align-items-center"&gt;
&lt;span class="required"&gt;Input 1&lt;/span&gt;
&lt;i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Example tooltip"&gt;&lt;/i&gt;
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input1" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 2
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input2" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;

&lt;!--begin::Input group--&gt;
&lt;div class="fv-row mb-10"&gt;
&lt;!--begin::Label--&gt;
&lt;label class="form-label"&gt;
Input 3
&lt;/label&gt;
&lt;!--end::Label--&gt;

&lt;!--begin::Input--&gt;
&lt;input type="text" class="form-control form-control-solid" name="input3" placeholder="" value=""/&gt;
&lt;!--end::Input--&gt;
&lt;/div&gt;
&lt;!--end::Input group--&gt;
&lt;/div&gt;
&lt;!--begin::Step 1--&gt;
&lt;/div&gt;
&lt;!--end::Group--&gt;

&lt;!--begin::Actions--&gt;
&lt;div class="d-flex flex-stack"&gt;
&lt;!--begin::Wrapper--&gt;
&lt;div class="me-2"&gt;
&lt;button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous"&gt;
Back
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;

&lt;!--begin::Wrapper--&gt;
&lt;div&gt;
&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="submit"&gt;
&lt;span class="indicator-label"&gt;
Submit
&lt;/span&gt;
&lt;span class="indicator-progress"&gt;
Please wait... &lt;span class="spinner-border spinner-border-sm align-middle ms-2"&gt;&lt;/span&gt;
&lt;/span&gt;
&lt;/button&gt;

&lt;button type="button" class="btn btn-primary" data-kt-stepper-action="next"&gt;
Continue
&lt;/button&gt;
&lt;/div&gt;
&lt;!--end::Wrapper--&gt;
&lt;/div&gt;
&lt;!--end::Actions--&gt;
&lt;/form&gt;
&lt;!--end::Form--&gt;
&lt;/div&gt;
&lt;!--end::Stepper--&gt;</code>
</pre>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Highlight-->
            </div>
            <!--end::Code-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="markup-reference" data-kt-scroll-offset="50">
            <a href="#markup-reference"></a>Markup Reference</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">For the sake of design abstraction Stepper does not depend on design structure with CSS classes. Instead, it defines it's dependant elements via HTML attributes explained in the below table:</div>
            <!--end::Block-->
            <!--begin::Table-->
            <div class="py-5">
                <div class="table-responsive border rounded">
                    <table class="table table-striped align-middle mb-0 g-5">
                        <thead>
                            <tr class="fs-4 fw-bold text-dark p-6">
                                <th class="min-w-300px">Name</th>
                                <th class="min-w-500px">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-element="nav"</code>
                                </td>
                                <td>Represents a step's navigation indicator element that may contain of step title, description, icon and number.</td>
                            </tr>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-element="content"</code>
                                </td>
                                <td>Represents a step content element that may contain of form elements.</td>
                            </tr>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-action="next"</code>
                                </td>
                                <td>Represents a next button.</td>
                            </tr>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-action="previous"</code>
                                </td>
                                <td>Represents a previous button.</td>
                            </tr>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-action="submit"</code>
                                </td>
                                <td>Represents a submit button that is shown in final step.</td>
                            </tr>
                            <tr>
                                <td>
                                    <code>data-kt-stepper-action="step"</code>
                                </td>
                                <td>Enables clickable steps when set to stepper navigation links.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--end::Table-->
            <!--begin::Notice-->
            <div class="py-5">
                <!--begin::Information-->
                <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                    <!--begin::Icon-->
                    <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                    <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <!--end::Icon-->
                    <!--begin::Description-->
                    <div class="text-gray-700 fw-bold fs-6">
                    <code>.current</code>,
                    <code>.pending</code>and
                    <code>.completed</code>classes are applied to
                    <code>data-kt-stepper-element="nav"</code>and
                    <code>data-kt-stepper-element="content"</code>elements to highlight current, pending and competed step states.</div>
                    <!--end::Description-->
                </div>
                <!--end::Information-->
            </div>
            <!--end::Notice-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="methods" data-kt-scroll-offset="50">
            <a href="#methods"></a>Methods</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">The following are the Stepper's functionality methods for more control.</div>
            <!--end::Block-->
            <!--begin::Table-->
            <div class="pt-2 pb-10">
                <!--begin::Table wrapper-->
                <div class="table-responsive border rounded">
                    <!--begin::Table-->
                    <table class="table table-striped mb-0 g-6">
                        <!--begin::Head-->
                        <thead>
                            <tr class="fs-4 fw-bold p-6">
                                <th class="min-w-400px">Name</th>
                                <th class="min-w-500px">Description</th>
                            </tr>
                        </thead>
                        <!--end::Head-->
                        <!--begin::Body-->
                        <tbody>
                            <tr>
                                <td>
                                    <code>KTStepper(DOMElement element, Object options)</code>
                                </td>
                                <td>Constructs a stepper object by given DOM element and options.
                                <code>startIndex</code>is starting steps index. Default value is
                                <code>startIndex: 1</code>
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">var stepperEl = document.querySelector("#kt_stepper_example");
var options = {startIndex: 1};
var stepper = new KTStepper(stepperEl, options);</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>goTo(Integer index)</code>
                                </td>
                                <td>Activates a step by given index.
                                <code>index</code>is an integer value(e.g: 1, 2, 3 ...) that represents the step's order number.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.goTo(index);</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>goNext()</code>
                                </td>
                                <td>Activates next step.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.goNext();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>goPrevious()</code>
                                </td>
                                <td>Activates previous step.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.goPrevious();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>goFirst()</code>
                                </td>
                                <td>Activates the first step.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.goFirst();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>goLast()</code>
                                </td>
                                <td>Activates the last step.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.goLast();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>getClickedStepIndex()</code>
                                </td>
                                <td>Returns the clicked step index number as integer when stepper nav link is set with
                                <code>data-kt-stepper-action="step"</code>attribute.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.getClickedStepIndex();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>getCurrentStepIndex()</code>
                                </td>
                                <td>Returns the current step index number as integer.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.getCurrentStepIndex();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>getNextStepIndex()</code>
                                </td>
                                <td>Returns the next step's index number as integer.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.getNextStepIndex();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>getPreviousStepIndex()</code>
                                </td>
                                <td>Returns the previous step's index number as integer.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.getPreviousStepIndex();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>getElement()</code>
                                </td>
                                <td>Returns the Stepper's attached DOM element.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">var element = stepper.getElement();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>destroy()</code>
                                </td>
                                <td>Removes the component instance from element.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">blockUI.destroy();</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                        </tbody>
                        <!--end::Body-->
                        <!--begin::Head-->
                        <thead>
                            <tr class="fs-4 fw-bold p-6">
                                <th colspan="2">Static Methods</th>
                            </tr>
                        </thead>
                        <!--end::Head-->
                        <!--begin::Body-->
                        <tbody>
                            <tr>
                                <td>
                                    <code>getInstance(DOMElement element)</code>
                                </td>
                                <td>Get the Stepper instance created
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">var stepperElement = document.querySelector("#kt_stepper_example_1");
var stepper = KTStepper.getInstance(stepperElement);</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                        </tbody>
                        <!--end::Body-->
                    </table>
                </div>
            </div>
            <!--end::Table-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="pt-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="events" data-kt-scroll-offset="50">
            <a href="#events"></a>Events</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">Below are few events for hooking into the Stepper functionality.</div>
            <!--end::Block-->
            <!--begin::Table-->
            <div class="pt-2 pb-5">
                <!--begin::Table wrapper-->
                <div class="table-responsive border rounded">
                    <!--begin::Table-->
                    <table class="table table-striped align-middle mb-0 g-5">
                        <!--begin::Head-->
                        <thead>
                            <tr class="fs-4 fw-bold text-dark p-6">
                                <th class="min-w-300px">Event Type</th>
                                <th class="min-w-500px">Description</th>
                            </tr>
                        </thead>
                        <!--end::Head-->
                        <!--begin::Body-->
                        <tbody>
                            <tr>
                                <td>
                                    <code>kt.stepper.next</code>
                                </td>
                                <td>This event fires on next navigation button click.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">var stepperEl = document.querySelector("#kt_stepper_example");
var stepper = new KTStepper(stepperEl);
stepper.on("kt.stepper.next", function() {
// console.log("kt.stepper.next event is fired");
});</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>kt.stepper.previous</code>
                                </td>
                                <td>This event fires on previous navigation button click.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.on("kt.stepper.previous", function() {
// console.log("kt.stepper.previous event is fired");
});</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>kt.stepper.change</code>
                                </td>
                                <td>This event fires before current step change.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.on("kt.stepper.change", function() {
// console.log("kt.stepper.change event is fired");
});</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>kt.stepper.changed</code>
                                </td>
                                <td>This event fires after current step change.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.on("kt.stepper.changed", function() {
// console.log("kt.stepper.changed event is fired");
});</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                            <tr>
                                <td>
                                    <code>kt.stepper.click</code>
                                </td>
                                <td>This event fires on clickable navigation link click.
                                <div class="pt-3">
                                    <!--begin::Highlight-->
                                    <div class="highlight">
                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                        <div class="highlight-code">
                                            <pre class="language-javascript">
<code class="language-javascript">stepper.on("kt.stepper.click", function() {
stepper.goTo(stepper.getClickedStepIndex()); // go to clicked step
});</code>
</pre>
                                        </div>
                                    </div>
                                    <!--end::Highlight-->
                                </div></td>
                            </tr>
                        </tbody>
                        <!--end::Body-->
                    </table>
                </div>
            </div>
            <!--end::Table-->
        </div>
        <!--end::Section-->
    </div>
    <!--end::Card Body-->
</div>