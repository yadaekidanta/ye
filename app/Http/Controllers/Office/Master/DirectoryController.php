<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Directory;
use Illuminate\Http\Request;

class DirectoryController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.master.directory.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Directory $directory)
    {
        //
    }
    public function edit(Directory $directory)
    {
        //
    }
    public function update(Request $request, Directory $directory)
    {
        //
    }
    public function destroy(Directory $directory)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Directory::get();
        return view('pages.office.master.directory.list',compact('collection'));
    }
}
