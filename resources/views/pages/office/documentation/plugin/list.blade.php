<div class="card card-docs flex-row-fluid mb-2">
    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
        <ul class="nav nav-stretch nav-line-tabs fw-bold fs-4 border-bottom-2 border-transparent flex-nowrap mb-8" role="tablist" id="kt_plugin_tabs">
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2 active" data-bs-toggle="tab" href="#kt_plugin_overview" role="tab">Overview</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_autosize" role="tab">Autosize</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_bsmaxlength" role="tab">Bootstrap Maxlength</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_clipboard" role="tab">Clipboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_daterangepicker" role="tab">Daterangepicker</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_dialer" role="tab">Dialer</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_dropzone" role="tab">Dropzone</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_flatpickr" role="tab">Flatpickr</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_formvalidation" role="tab">Form Validation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_formrepeater" role="tab">Form Repeater</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_inputmask" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Multi Select Splitter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_plugin_multiselectsplitter" role="tab">Inputmask</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade show active" id="kt_plugin_overview">
            </div>
            <div class="tab-pane fade" id="kt_plugin_autosize">
                <div class="card card-docs flex-row-fluid mb-2">
                    <!--begin::Card Body-->
                    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
                        <!--begin::Section-->
                        <div class="pb-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="overview" data-kt-scroll-offset="50">
                            <a href="#overview"></a>Overview</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">
                            <strong class="me-1">Autosize</strong>is a small, stand-alone script to automatically adjust textarea height. For full documentation please check the
                            <a href="http://www.jacklmoore.com/autosize/" target="_blank" class="fw-semibold">plugin's site</a>.</div>
                            <!--end::Block-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="usage" data-kt-scroll-offset="50">
                            <a href="#usage"></a>Usage</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Autosize's Javascript files are bundled in the global plugin bundles and globally included in all pages:</div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <div class="highlight-code">
                                        <pre class="language-html">
<code class="language-html">&lt;link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/&gt;
&lt;script src="assets/plugins/global/plugins.bundle.js"&gt;&lt;/script&gt;</code>
</pre>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="initialization" data-kt-scroll-offset="50">
                            <a href="#initialization"></a>Initialization</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <ul class="py-0">
                                    <li class="py-2">Autosize's Javascript is globally included and bundled without our plugins bundle.</li>
                                    <li class="py-2">Autosize's is initialized by adding
                                    <code>data-kt-autosize="true"</code>HTML attribute within any
                                    <code>textarea</code>HTML element.</li>
                                    <li class="py-2">Autosize instances can also be controlled programmatically.
                                    <a href="#methods" data-kt-scroll-toggle="">See below for more info</a>.</li>
                                    <li class="py-2">Autosize also supports event hooks. Please refer to the
                                    <a href="http://www.jacklmoore.com/autosize/" target="_blank" class="me-1">official documentation</a>for more info.</li>
                                </ul>
                            </div>
                            <!--end::Block-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="basic" data-kt-scroll-offset="50">
                            <a href="#basic"></a>Basic</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Here's an example of the
                            <code>textarea</code>automatically resizing base on the amount of text written in it.</div>
                            <!--end::Block-->
                            <div class="py-5">
                                <!--begin::Information-->
                                <div class="d-flex align-items-center rounded py-5 px-5 bg-light-info">
                                    <!--begin::Icon-->
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                                    <span class="svg-icon svg-icon-3x svg-icon-info me-5">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                    <!--end::Icon-->
                                    <!--begin::Description-->
                                    <div class="text-gray-700 fw-bold fs-6">Combine our extended
                                    <code>.form-control</code>CSS classes to create quick and solid
                                    <code>textarea</code>layouts.</div>
                                    <!--end::Description-->
                                </div>
                                <!--end::Information-->
                            </div>
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border d-flex flex-column p-10">
                                    <label for="" class="form-label">Basic autosize textarea</label>
                                    <textarea class="form-control" data-kt-autosize="true"></textarea>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border d-flex flex-column p-10">
                                    <label for="" class="form-label">Solid autosize textarea</label>
                                    <textarea class="form-control form-control form-control-solid" data-kt-autosize="true"></textarea>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <div class="highlight-code">
                                        <pre class="language-html">
<code class="language-html">&lt;!--begin::basic autosize textarea--&gt;
&lt;div class="rounded border d-flex flex-column p-10"&gt;
&lt;label for="" class="form-label"&gt;Basic autosize textarea&lt;/label&gt;
&lt;textarea class="form-control" data-kt-autosize="true"&gt;&lt;/textarea&gt;
&lt;/div&gt;
&lt;!--end::basic autosize textarea--&gt;

&lt;!--begin::solid autosize textarea--&gt;
&lt;div class="rounded border d-flex flex-column p-10"&gt;
&lt;label for="" class="form-label"&gt;Solid autosize textarea&lt;/label&gt;
&lt;textarea class="form-control form-control form-control-solid" data-kt-autosize="true"&gt;&lt;/textarea&gt;
&lt;/div&gt;
&lt;!--end::solid autosize textarea--&gt;</code>
</pre>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="markup-reference" data-kt-scroll-offset="50">
                            <a href="#markup-reference"></a>Markup Reference</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Autosize uses HTML attributes to initialize the autosize function. Here are the references for each below</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="fw-bold fs-2">HTML Attribute references</div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Table-->
                            <div class="py-5">
                                <div class="table-responsive border rounded">
                                    <table class="table table-striped align-top mb-0 g-5">
                                        <thead>
                                            <tr class="fs-4 fw-bold text-dark p-6">
                                                <th class="min-w-200px">Name</th>
                                                <th class="min-w-100px">Type</th>
                                                <th class="min-w-500px">Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <code>data-kt-autosize</code>
                                                </td>
                                                <td>
                                                    <code class="ms-0">mandatory</code>
                                                </td>
                                                <td>Enables the current element as the Autosize component. Accepts
                                                <code>true</code>or
                                                <code>false</code>values.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="pt-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="methods" data-kt-scroll-offset="50">
                            <a href="#methods"></a>Methods</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">All Autosize components can be initialized automatically through HTML attributes. However, it can also be programmatically intialized by ignoring the HTML attribute and manually initializing it via Javascript.</div>
                            <!--end::Block-->
                            <!--begin::Table-->
                            <div class="pt-2">
                                <!--begin::Table wrapper-->
                                <div class="table-responsive border rounded">
                                    <!--begin::Table-->
                                    <table class="table table-striped mb-0 g-6">
                                        <!--begin::Head-->
                                        <thead>
                                            <tr class="fs-4 fw-bold p-6">
                                                <th class="min-w-200px">Name</th>
                                                <th class="min-w-500px">Description</th>
                                            </tr>
                                        </thead>
                                        <!--end::Head-->
                                        <!--begin::Head-->
                                        <thead>
                                            <tr class="fs-4 fw-bold p-6">
                                                <th colspan="2">Static Methods</th>
                                            </tr>
                                        </thead>
                                        <!--end::Head-->
                                        <!--begin::Body-->
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <code>Initialization</code>
                                                </td>
                                                <td>Initializes Autosize instances by selecting the element required. This method can be used to initialize dynamicly populated Autosize instances(e.g: after Ajax request).
                                                <div class="pt-3">
                                                    <!--begin::Highlight-->
                                                    <div class="highlight">
                                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                                        <div class="highlight-code">
                                                            <pre class="language-javascript">
<code class="language-javascript">// Single node
const autosize_element = document.querySelector('#kt_autosize');
autosize(autosize_element);

// Nodelist
const autosize_elements = document.querySelectorAll('.kt_autosize');
autosize(autosize_elements);

// jQuery
autosize($('.kt_autosize'));</code>
</pre>
                                                        </div>
                                                    </div>
                                                    <!--end::Highlight-->
                                                </div></td>
                                            </tr>
                                        </tbody>
                                        <!--end::Body-->
                                        <!--begin::Head-->
                                        <thead>
                                            <tr class="fs-4 fw-bold p-6">
                                                <th colspan="2">Public Methods</th>
                                            </tr>
                                        </thead>
                                        <!--end::Head-->
                                        <!--begin::Body-->
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <code>update</code>
                                                </td>
                                                <td>Triggers the height adjustment for an assigned textarea element. Autosize will automatically adjust the textarea height on keyboard and window resize events. There is no efficient way for Autosize to monitor for when another script has changed the textarea value or for changes in layout that impact the textarea element.
                                                <div class="pt-3">
                                                    <!--begin::Highlight-->
                                                    <div class="highlight">
                                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                                        <div class="highlight-code">
                                                            <pre class="language-javascript">
<code class="language-javascript">// Change value
autosize_element.vlaue = "some value";
autosize.update(autosize_element);</code>
</pre>
                                                        </div>
                                                    </div>
                                                    <!--end::Highlight-->
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <code>destroy</code>
                                                </td>
                                                <td>Removes Autosize and reverts any changes it made to the textarea element.
                                                <div class="pt-3">
                                                    <!--begin::Highlight-->
                                                    <div class="highlight">
                                                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                                        <div class="highlight-code">
                                                            <pre class="language-javascript">
<code class="language-javascript">autosize.destroy(autosize_element);</code>
</pre>
                                                        </div>
                                                    </div>
                                                    <!--end::Highlight-->
                                                </div></td>
                                            </tr>
                                        </tbody>
                                        <!--end::Body-->
                                    </table>
                                </div>
                            </div>
                            <!--end::Table-->
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Card Body-->
                </div>
            </div>
            <div class="tab-pane fade" id="kt_plugin_bsmaxlength">
                <div class="card card-docs flex-row-fluid mb-2">
                    <!--begin::Card Body-->
                    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
                        <!--begin::Section-->
                        <div class="pb-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="overview" data-kt-scroll-offset="50">
                            <a href="#overview"></a>Overview</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Bootstrap Maxlength is a visual feedback indicator for the maxlength attribute. For more info please visit the plugin's
                            <a class="me-1" href="https://mimo84.github.io/bootstrap-maxlength/" target="_blank">Home</a>or
                            <a href="https://github.com/mimo84/bootstrap-maxlength/" target="_blank">Github Repo</a>.</div>
                            <!--end::Block-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="usage" data-kt-scroll-offset="50">
                            <a href="#usage"></a>Usage</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Bootstrap Maxlength's Javascript files are bundled in the global plugin bundles and globally included in all pages. However, the maxlength functionality requires a manual Javascript initialization.</div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <div class="highlight-code">
                                        <pre class="language-html">
<code class="language-html">&lt;script src="assets/plugins/global/plugins.bundle.js"&gt;&lt;/script&gt;</code>
</pre>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="basic-example" data-kt-scroll-offset="50">
                            <a href="#basic-example"></a>Basic Example</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Basic example of a simple input with the
                            <code>maxlength</code>HTML attribute to control the total number of characters allowed.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Basic example</label>
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_basic" maxlength="10" />
                                    <span class="fs-6 text-muted">The badge will show up by default when the remaining chars are 10 or less</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d5087" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d508f" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d5087" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_basic').maxlength({
warningClass: "badge badge-warning",
limitReachedClass: "badge badge-success"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d508f" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Basic example&lt;/label&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_basic" maxlength="10" /&gt;
&lt;span class="fs-6 text-muted"&gt;The badge will show up by default when the remaining chars are 3 or less&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="threshold" data-kt-scroll-offset="50">
                            <a href="#threshold"></a>Threshold</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">An example of Bootstrap Maxlength with a treshold option. The
                            <code>threshold</code>option is to set the minimum number of characters that will display the badge.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Threshold example</label>
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_threshold" maxlength="25" />
                                    <span class="fs-6 text-muted">Set threshold value to show there are 20 chars or less left</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d5c0a" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d5c0f" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d5c0a" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_threshold').maxlength({
threshold: 20,
warningClass: "badge badge-primary",
limitReachedClass: "badge badge-success"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d5c0f" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Threshold example&lt;/label&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_threshold" maxlength="25" /&gt;
&lt;span class="fs-6 text-muted"&gt;Set threshold value to show there are 20 chars or less left&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="always-show" data-kt-scroll-offset="50">
                            <a href="#always-show"></a>Always Show</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">An example of Bootstrap Maxlength with an always show option. Add
                            <code>alwaysShow: true</code>in the Javascript.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Always show example</label>
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_always_show" maxlength="20" />
                                    <span class="fs-6 text-muted">Show the counter on input focus</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d6716" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d671b" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d6716" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_always_show').maxlength({
alwaysShow: true,
threshold: 20,
warningClass: "badge badge-primary",
limitReachedClass: "badge badge-success"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d671b" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Threshold example&lt;/label&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_always_show" maxlength="20" /&gt;
&lt;span class="fs-6 text-muted"&gt;Show the counter on input focus&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="custom-text" data-kt-scroll-offset="50">
                            <a href="#custom-text"></a>Custom Text</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">An example of Bootstrap Maxlength with some custom text in the badge. Please refer to the code sample below for the JS options.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Custom text example</label>
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_custom_text" maxlength="20" />
                                    <span class="fs-6 text-muted">Display custom text on input focus</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d722d" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d7231" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d722d" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_custom_text').maxlength({
threshold: 20,
warningClass: "badge badge-danger",
limitReachedClass: "badge badge-success",
separator: ' of ',
preText: 'You have ',
postText: ' chars remaining.',
validate: true
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d7231" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Custom text example&lt;/label&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_custom_text" maxlength="20" /&gt;
&lt;span class="fs-6 text-muted"&gt;Display custom text on input focus&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="textarea" data-kt-scroll-offset="50">
                            <a href="#textarea"></a>Textarea</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Bootstrap Maxlength can also be applied to
                            <code>textarea</code>.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Textarea example</label>
                                    <textarea class="form-control mb-3" id="kt_docs_maxlength_textarea" maxlength="20" placeholder="" rows="6"></textarea>
                                    <span class="fs-6 text-muted">Bootstrap maxlength supports textarea as well as inputs</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d7c6e" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d7c72" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d7c6e" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_textarea').maxlength({
warningClass: "badge badge-primary",
limitReachedClass: "badge badge-success"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d7c72" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Textarea example&lt;/label&gt;
&lt;textarea class="form-control" id="kt_docs_maxlength_textarea" maxlength="20" placeholder="" rows="6"&gt;&lt;/textarea&gt;
&lt;span class="fs-6 text-muted"&gt;Bootstrap maxlength supports textarea as well as inputs&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="py-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="set-position" data-kt-scroll-offset="50">
                            <a href="#set-position"></a>Set Position</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Here's an example of predefining the badge position, relative to the input.</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <label class="form-label">Set Position example</label>
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_position_top_left" maxlength="20" />
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_position_top_right" maxlength="20" />
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_position_bottom_left" maxlength="20" />
                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_position_bottom_right" maxlength="20" />
                                    <span class="fs-6 text-muted">The field counter can be positioned at the top, bottom, left or right.</span>
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="pt-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d880c" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d8812" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d880c" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_position_top_left').maxlength({
placement: 'top-left',
warningClass: "badge badge-danger",
limitReachedClass: "badge badge-primary"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d8812" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html">
<code class="language-html">&lt;label class="form-label"&gt;Set Position example&lt;/label&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_position_top_left" maxlength="20" /&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_position_top_right" maxlength="20" /&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_position_bottom_left" maxlength="20" /&gt;
&lt;input type="text" class="form-control" id="kt_docs_maxlength_position_bottom_right" maxlength="20" /&gt;
&lt;span class="fs-6 text-muted"&gt;The field counter can be positioned at the top, bottom, left or right.&lt;/span&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="pt-10">
                            <!--begin::Heading-->
                            <h1 class="anchor fw-bold mb-5" id="modal-example" data-kt-scroll-offset="50">
                            <a href="#modal-example"></a>Modal Example</h1>
                            <!--end::Heading-->
                            <!--begin::Block-->
                            <div class="py-5">Use Date Range Picker within Bootstrap Modal:</div>
                            <!--end::Block-->
                            <!--begin::Block-->
                            <div class="py-5">
                                <div class="rounded border p-10">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_1">Launch demo modal</button>
                                    <!--begin::Modal-->
                                    <div class="modal fade" tabindex="-1" id="kt_modal_1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Modal title</h5>
                                                    <!--begin::Close-->
                                                    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                                        <span class="svg-icon svg-icon-2x">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="currentColor" />
                                                            </svg>
                                                        </span>
                                                        <!--end::Svg Icon-->
                                                    </div>
                                                    <!--end::Close-->
                                                </div>
                                                <div class="modal-body">
                                                    <label class="form-label">Basic example</label>
                                                    <input type="text" class="form-control mb-3" id="kt_docs_maxlength_basic_modal" maxlength="10" />
                                                    <span class="fs-6 text-muted">The badge will show up by default when the remaining chars are 10 or less</span>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Modal-->
                                </div>
                            </div>
                            <!--end::Block-->
                            <!--begin::Code-->
                            <div class="py-5">
                                <!--begin::Highlight-->
                                <div class="highlight">
                                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                                    <ul class="nav nav-pills" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#kt_highlight_62c3e291d93e3" role="tab">JAVASCRIPT</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#kt_highlight_62c3e291d93e8" role="tab">HTML</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="kt_highlight_62c3e291d93e3" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-javascript">
<code class="language-javascript">$('#kt_docs_maxlength_basic_modal').maxlength({
warningClass: "badge badge-primary",
limitReachedClass: "badge badge-success"
});</code>
</pre>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kt_highlight_62c3e291d93e8" role="tabpanel">
                                            <div class="highlight-code">
                                                <pre class="language-html" style="height:400px">
<code class="language-html">&lt;!--begin::Modal--&gt;
&lt;div class="modal fade" tabindex="-1" id="kt_modal_1"&gt;
&lt;div class="modal-dialog"&gt;
&lt;div class="modal-content"&gt;
&lt;div class="modal-header"&gt;
    &lt;h5 class="modal-title"&gt;Modal title&lt;/h5&gt;

    &lt;!--begin::Close--&gt;
    &lt;div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close"&gt;
        &lt;span class="svg-icon svg-icon-2x"&gt;&lt;/span&gt;
    &lt;/div&gt;
    &lt;!--end::Close--&gt;
&lt;/div&gt;

&lt;div class="modal-body"&gt;
    &lt;label class="form-label"&gt;Basic example&lt;/label&gt;
    &lt;input type="text" class="form-control mb-3" id="kt_docs_maxlength_basic" maxlength="10" /&gt;
    &lt;span class="fs-6 text-muted"&gt;The badge will show up by default when the remaining chars are 10 or less&lt;/span&gt;
&lt;/div&gt;

&lt;div class="modal-footer"&gt;
    &lt;button type="button" class="btn btn-light" data-bs-dismiss="modal"&gt;Close&lt;/button&gt;
    &lt;button type="button" class="btn btn-primary"&gt;Save changes&lt;/button&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;!--end::Modal--&gt;</code>
</pre>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Highlight-->
                            </div>
                            <!--end::Code-->
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Card Body-->
                </div>
            </div>
            <div class="tab-pane fade" id="kt_plugin_clipboard">
            </div>
            <div class="tab-pane fade" id="kt_plugin_daterangepicker">
            </div>
            <div class="tab-pane fade" id="kt_plugin_dialer">
            </div>
            <div class="tab-pane fade" id="kt_plugin_dropzone">
            </div>
            <div class="tab-pane fade" id="kt_plugin_flatpickr">
            </div>
            <div class="tab-pane fade" id="kt_plugin_formvalidation">
            </div>
            <div class="tab-pane fade" id="kt_plugin_formrepeater">
            </div>
            <div class="tab-pane fade" id="kt_plugin_inputmask">
            </div>
            <div class="tab-pane fade" id="kt_plugin_multiselectsplitter">
            </div>
            <div class="tab-pane fade" id="kt_plugin_nouislider">
            </div>
            <div class="tab-pane fade" id="kt_plugin_passwordmeter">
            </div>
            <div class="tab-pane fade" id="kt_plugin_recaptcha">
            </div>
            <div class="tab-pane fade" id="kt_plugin_select2">
            </div>
            <div class="tab-pane fade" id="kt_plugin_tagify">
            </div>
            <div class="tab-pane fade" id="kt_plugin_ckeditor">
            </div>
            <div class="tab-pane fade" id="kt_plugin_quill">
            </div>
            <div class="tab-pane fade" id="kt_plugin_tinymce">
            </div>
            <div class="tab-pane fade" id="kt_plugin_cropper">
            </div>
            <div class="tab-pane fade" id="kt_plugin_drawer">
            </div>
            <div class="tab-pane fade" id="kt_plugin_fullcalendar">
            </div>
            <div class="tab-pane fade" id="kt_plugin_lightbox">
            </div>
            <div class="tab-pane fade" id="kt_plugin_jkanban">
            </div>
            <div class="tab-pane fade" id="kt_plugin_jstree">
            </div>
            <div class="tab-pane fade" id="kt_plugin_sweetalert">
            </div>
            <div class="tab-pane fade" id="kt_plugin_slider">
            </div>
            <div class="tab-pane fade" id="kt_plugin_toastr">
            </div>
            <div class="tab-pane fade" id="kt_plugin_typedjs">
            </div>
            <div class="tab-pane fade" id="kt_plugin_vistimeline">
            </div>
        </div>
    </div>
</div>