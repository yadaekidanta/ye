<?php

namespace App\Http\Controllers\Office\Corporate;

use App\Http\Controllers\Controller;
use App\Models\Corporate\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.corporate.history.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.corporate.history.input',['data' => new History]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(History $history)
    {
        //
    }
    public function edit(History $history)
    {
        return view('pages.office.corporate.history.input',['data' => $history]);
    }
    public function update(Request $request, History $history)
    {
        //
    }
    public function destroy(History $history)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = History::get();
        return view('pages.office.corporate.history.list',compact('collection'));
    }
}
