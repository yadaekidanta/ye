<?php

namespace App\Models\Regional;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }
    public function village()
    {
        return $this->hasMany(Village::class);
    }
}
