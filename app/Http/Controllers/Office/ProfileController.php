<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.overview.main');
        }
        return view('pages.office.theme');
    }
    public function overview()
    {
        return view('pages.office.profile.overview.overview');
    }
    public function index_setting(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.setting.main');
        }
        return view('pages.office.theme');
    }
    public function setting()
    {
        return view('pages.office.profile.setting.setting');
    }
    public function index_security(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.security.main');
        }
        return view('pages.office.theme');
    }
    public function security()
    {
        return view('pages.office.profile.security.security');
    }
    public function index_billing(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.billing.main');
        }
        return view('pages.office.theme');
    }
    public function billing()
    {
        return view('pages.office.profile.billing.billing');
    }
    public function index_statement(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.statement.main');
        }
        return view('pages.office.theme');
    }
    public function statement()
    {
        return view('pages.office.profile.statement.statement');
    }
    public function index_referral(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.referral.main');
        }
        return view('pages.office.theme');
    }
    public function referral()
    {
        return view('pages.office.profile.referral.referral');
    }
    public function index_log(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.log.main');
        }
        return view('pages.office.theme');
    }
    public function log()
    {
        return view('pages.office.profile.log.log');
    }
    public function index_project(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.project.main');
        }
        return view('pages.office.theme');
    }
    public function project()
    {
        return view('pages.office.profile.project.project');
    }
    public function index_campaign(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.campaign.main');
        }
        return view('pages.office.theme');
    }
    public function campaign()
    {
        return view('pages.office.profile.campaign.campaign');
    }
    public function index_follower(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.follower.main');
        }
        return view('pages.office.theme');
    }
    public function follower()
    {
        return view('pages.office.profile.follower.follower');
    }
    public function index_activity(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.profile.activity.main');
        }
        return view('pages.office.theme');
    }
    public function activity()
    {
        return view('pages.office.profile.activity.activity');
    }
}
