<head>
    <title>
        {{config('app.name') ?? config('app.name')}}
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8" />
    <meta name="description" content="YE is one of the Enteprise Digital Transfromation Consultant & Integrator company in Indonesia, specializing in software solutions and application delivery & managed services. Our mission is to enable enterprises to ride the wave of digital era with tech-enabled innovation & business process automation." />
    <meta name="keywords" content="YE, Yada, yada, Ekidanta, ekidanta, Konsultan, konsultan, it, IT, bandung, Bandung, Indonesia, indonesia" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{config('app.name') ?? config('app.name')}}" />
    <meta property="og:url" content="https://yadaekidanta.com" />
    <meta property="og:site_name" content="{{config('app.name') ?? config('app.name')}}" />
    <link rel="canonical" href="https://yadaekidanta.com" />
    <link rel="shortcut icon" href="{{asset('img/icon.png')}}" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Vendor Stylesheets(used by this page)-->
    @auth
    <link href="{{asset('keenthemes/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    @endauth
    <!--end::Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{asset('keenthemes/plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('keenthemes/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('keenthemes/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/flatpickr.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <style>
        .inactive .ql-editor {
            padding:0;
            line-height:inherit;
        }

        .inactive .ql-editor p {
            padding:inherit;
            margin-bottom:10px;
        }

        .inactive .ql-toolbar {
            display:none;
        }

        .inactive .ql-container.ql-snow {
            border:none;
            font-family:inherit;
            font-size:inherit;
        }
    </style>
</head>