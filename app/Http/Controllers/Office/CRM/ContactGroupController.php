<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Lead;
use Illuminate\Http\Request;
use App\Models\CRM\ContactGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ContactGroupController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:contact_groups,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $group = new ContactGroup;
        $group->name = $request->name;
        $group->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Group Created',
        ]);
    }
    public function show(ContactGroup $contactGroup)
    {
        //
    }
    public function edit(ContactGroup $contactGroup)
    {
        //
    }
    public function update(Request $request, ContactGroup $contactGroup)
    {
        //
    }
    public function destroy(ContactGroup $group)
    {
        $group->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Group Deleted',
        ]);
    }
    public function list(Request $request)
    {
        $lead = Lead::get();
        $collection = ContactGroup::where('name','LIKE','%'.$request->keyword.'%')->get();
        return view('pages.office.crm.group.list',compact('collection','lead'));
    }
}
