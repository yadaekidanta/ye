<x-web-layout title="{{__('menu.case')}}">
    <section class="wrapper bg-light">
        <div class="container pt-10 pt-md-14 text-center">
            <div class="row">
                <div class="col-md-8 col-lg-7 col-xl-6 col-xxl-5 mx-auto">
                    <h1 class="display-1 mb-3">Case Studies</h1>
                    <p class="lead fs-lg px-lg-10 px-xxl-8">Projects from our various amazing clients.</p>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container pt-9 pt-md-11 pb-14 pb-md-16">
            <div class="projects-overflow mt-md-10 mb-10 mb-lg-15">
                @foreach ($collection as $key => $item)
                @php
                if(strlen($item->title) < 30){
                    $title = 'h2';
                }else{
                    $title = 'h5';
                }
                if ((++$key % 2) == 0){
                    $class = "col-lg-7 offset-lg-5 col-xl-6 offset-xl-5 rounded";
                    $style = "left: 18%; bottom: 25%;";
                }else{
                    $class = "col-lg-8 col-xl-7 offset-xl-1 rounded";
                    $style = "right: 10%; bottom: 25%;";
                }
                @endphp
                <div class="project item">
                    <div class="row">
                        <figure class="{{$class}}"> <img src="{{$item->image}}" alt="" /></figure>
                        <div class="project-details d-flex justify-content-center flex-column" style="{{$style}}">
                            <div class="card shadow rellax" data-rellax-xs-speed="0" data-rellax-mobile-speed="0">
                                <div class="card-body">
                                    <div class="post-header">
                                        <div class="post-category text-line text-purple mb-3">{{$item->client->company_name}}</div>
                                        <{{$title}} class="post-title mb-3">{{$item->title}}</{{$title}}>
                                    </div>
                                    <!-- /.post-header -->
                                    <div class="post-content">
                                        {!!Str::limit($item->description, 150, $end='...')!!}
                                        <a href="{{route('web.show_case',$item->slug)}}" class="more hover link-purple">See Project</a>
                                    </div>
                                    <!-- /.post-content -->
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.project-details -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.project -->
                @endforeach
            </div>
            <!-- /.projects-overflow -->
            <!-- /nav -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
</x-web-layout>