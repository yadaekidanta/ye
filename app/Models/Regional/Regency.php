<?php

namespace App\Models\Regional;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function province()
    {
        return $this->belongsTo(Province::class);
    }
    public function district()
    {
        return $this->hasMany(District::class);
    }
}
