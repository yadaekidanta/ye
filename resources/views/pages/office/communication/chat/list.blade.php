@foreach($collection as $key => $item)
@if($item->from_id != Auth::guard('office')->user()->id)
<div class="d-flex flex-stack py-4">
    <div class="d-flex align-items-center">
        {{-- <div class="symbol symbol-45px symbol-circle">
            <img alt="Pic" src="assets/media/avatars/300-5.jpg" />
        </div> --}}
        <div class="symbol symbol-45px symbol-circle">
            <span class="symbol-label bg-light-danger text-danger fs-6 fw-bolder">O</span>
            <div class="symbol-badge bg-success start-100 top-100 border-4 h-15px w-15px ms-n2 mt-n2"></div>
        </div>
        <div class="ms-5">
            <a href="#" class="fs-5 fw-bold text-gray-900 text-hover-primary mb-2">{{$item->pengirim->name}}</a>
            <div class="fw-semibold text-muted">{{$item->pengirim->email}}</div>
        </div>
    </div>
    <div class="d-flex flex-column align-items-end ms-2">
        <span class="text-muted fs-7 mb-1">{{$item->created_at->diffForHumans()}}</span>
        <span class="badge badge-sm badge-circle badge-light-success">6</span>
    </div>
</div>
<div class="separator separator-dashed d-none"></div>
@endif
@endforeach