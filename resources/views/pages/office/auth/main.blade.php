<x-office-layout>
    <div class="d-flex justify-content-between flex-column-fluid flex-column w-100 mw-450px">
        <div id="login_page">
            <div class="d-flex flex-stack py-2 mb-15">
                <div class="me-2"></div>
                <div class="m-0">
                    <span class="text-gray-400 fw-bold fs-5 me-2" data-kt-translate="sign-in-head-desc">Not a Member yet?</span>
                    <a href="javascript:;" onclick="auth_content('register_page');" class="link-primary fw-bold fs-5" data-kt-translate="sign-in-head-link">Sign Up</a>
                </div>
            </div>
            <div class="py-20 mt-15">
                <form class="form w-100" novalidate="novalidate" id="form_login" data-kt-redirect-url="dashboard">
                    <div class="card-body">
                        <div class="text-start mb-10 mt-16">
                            <h1 class="text-dark mb-3 fs-3x" data-kt-translate="sign-in-title">Sign In</h1>
                            <div class="text-gray-400 fw-semibold fs-6" data-kt-translate="general-desc">Simplify Your Future</div>
                        </div>
                        <div class="fv-row mb-8">
                            <input type="email" placeholder="Email" id="email_login" name="email" data-login="1" autocomplete="off" data-kt-translate="sign-in-input-email" class="form-control form-control-solid" data-validation="Kami perlu mengetahui email Anda" data-format="Harap masukkan email dengan benar" />
                        </div>
                        <div class="fv-row mb-7">
                            <input type="password" placeholder="Password" name="password" data-login="2" autocomplete="off" data-kt-translate="sign-in-input-password" class="form-control form-control-solid" data-validation="Kami perlu mengetahui Kata Sandi Anda" data-format="Kata Sandi minimal 8 karakter" />
                        </div>
                        <div class="d-flex flex-stack flex-wrap gap-3 fs-base fw-semibold mb-10">
                            <div></div>
                            <a href="javascript:;" onclick="auth_content('forgot_page');" class="link-primary" data-kt-translate="sign-in-forgot-password">Forgot Password ?</a>
                        </div>
                        <div class="d-flex flex-stack">
                            <button id="tombol_login" onclick="handle_post('#tombol_login','#form_login','{{route('office.auth.login')}}','POST');" class="btn btn-primary me-2 flex-shrink-0">
                                <span class="indicator-label" data-kt-translate="sign-in-submit">Sign In</span>
                                <span class="indicator-progress">
                                    <span data-kt-translate="general-progress">Please wait...</span>
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                </span>
                            </button>
                            <div class="d-flex align-items-center">
                                <div class="text-gray-400 fw-semibold fs-6 me-3 me-md-6" data-kt-translate="general-or">Or</div>
                                <a href="javascript:;" class="symbol symbol-circle symbol-45px w-45px bg-light me-3">
                                    <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/google-icon.svg')}}" class="p-4" />
                                </a>
                                <a href="javascript:;" class="d-none symbol symbol-circle symbol-45px w-45px bg-light me-3">
                                    <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/facebook-3.svg')}}" class="p-4" />
                                </a>
                                <a href="javascript:;" class="d-none symbol symbol-circle symbol-45px w-45px bg-light">
                                    <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/apple-black.svg')}}" class="theme-light-show p-4" />
                                    <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/apple-black-dark.svg')}}" class="theme-dark-show p-4" />
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="register_page">
            <div class="d-flex flex-stack py-2">
                <div class="me-2">
                    <a href="javascript:;" onclick="auth_content('login_page');" class="btn btn-icon bg-light rounded-circle">
                        <span class="svg-icon svg-icon-2 svg-icon-gray-800">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.60001 11H21C21.6 11 22 11.4 22 12C22 12.6 21.6 13 21 13H9.60001V11Z" fill="currentColor" />
                                <path opacity="0.3" d="M9.6 20V4L2.3 11.3C1.9 11.7 1.9 12.3 2.3 12.7L9.6 20Z" fill="currentColor" />
                            </svg>
                        </span>
                    </a>
                </div>
                <div class="m-0">
                    <span class="text-gray-400 fw-bold fs-5 me-2" data-kt-translate="sign-up-head-desc">Have you joined ?</span>
                    <a href="javascript:;" onclick="auth_content('login_page');" class="link-primary fw-bold fs-5" data-kt-translate="sign-up-head-link">Sign In</a>
                </div>
            </div>
            <div class="py-20">
                <form class="form w-100 mt-18" novalidate="novalidate" id="form_register" data-kt-redirect-url="javascript:;">
                    <div class="text-start mb-10">
                        <h1 class="text-dark mb-3 fs-3x" data-kt-translate="sign-up-title">Create an Account</h1>
                        <div class="text-gray-400 fw-semibold fs-6" data-kt-translate="general-desc">Simplify Your Future</div>
                    </div>
                    <div class="fv-row mb-10">
                        <input class="form-control form-control-lg form-control-solid" type="text" placeholder="Fullname" id="fullname" name="fullname" autocomplete="off" data-kt-translate="sign-up-input-full-name" />
                    </div>
                    <div class="row fv-row mb-10">
                        <div class="col-xl-6">
                            <input class="form-control form-control-lg form-control-solid" type="email" placeholder="Eg : johndoe@yadaekidanta.com" name="email" autocomplete="off" data-kt-translate="sign-up-input-email" />
                        </div>
                        <div class="col-xl-6">
                            <input class="form-control form-control-lg form-control-solid" type="tel" placeholder="08XXXXXXXXXX" name="phone" autocomplete="off" data-kt-translate="sign-up-input-phone" />
                        </div>
                    </div>
                    <div class="fv-row mb-10" data-kt-password-meter="true">
                        <div class="mb-1">
                            <div class="position-relative mb-3">
                                <input class="form-control form-control-lg form-control-solid" type="password" placeholder="Password" name="password" autocomplete="off" data-kt-translate="sign-up-input-password" />
                                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                    <i class="bi bi-eye-slash fs-2"></i>
                                    <i class="bi bi-eye fs-2 d-none"></i>
                                </span>
                            </div>
                            <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                            </div>
                        </div>
                        <div class="text-muted" data-kt-translate="sign-up-hint">Use 8 or more characters with a mix of letters, numbers &amp; symbols.</div>
                    </div>
                    <div class="fv-row mb-10">
                        <input class="form-control form-control-lg form-control-solid" type="password" placeholder="Confirm Password" name="confirm-password" autocomplete="off" data-kt-translate="sign-up-input-confirm-password" />
                    </div>
                    <div class="d-flex flex-stack">
                        <button id="kt_sign_up_submit" class="btn btn-primary" data-kt-translate="sign-up-submit">
                            <span class="indicator-label">Submit</span>
                            <span class="indicator-progress">Please wait...
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                        <div class="d-flex align-items-center">
                            <div class="text-gray-400 fw-semibold fs-6 me-6">Or</div>
                            <a href="javascript:;" class="symbol symbol-circle symbol-45px w-45px bg-light me-3">
                                <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/google-icon.svg')}}" class="p-4" />
                            </a>
                            <a href="javascript:;" class="d-none symbol symbol-circle symbol-45px w-45px bg-light me-3">
                                <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/facebook-3.svg')}}" class="p-4" />
                            </a>
                            <a href="javascript:;" class="d-none symbol symbol-circle symbol-45px w-45px bg-light">
                                <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/apple-black.svg')}}" class="theme-light-show p-4" />
                                <img alt="Logo" src="{{asset('keenthemes/media/svg/brand-logos/apple-black-dark.svg')}}" class="theme-dark-show p-4" />
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="forgot_page">
            <div class="d-flex flex-stack py-2 mb-20">
                <div class="me-2">
                    <a href="javascript:;" onclick="auth_content('login_page');" class="btn btn-icon bg-light rounded-circle">
                        <span class="svg-icon svg-icon-2 svg-icon-gray-800">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.60001 11H21C21.6 11 22 11.4 22 12C22 12.6 21.6 13 21 13H9.60001V11Z" fill="currentColor" />
                                <path opacity="0.3" d="M9.6 20V4L2.3 11.3C1.9 11.7 1.9 12.3 2.3 12.7L9.6 20Z" fill="currentColor" />
                            </svg>
                        </span>
                    </a>
                </div>
                <div class="m-0">
                    <span class="text-gray-400 fw-bold fs-5 me-2" data-kt-translate="password-reset-head-desc">Already remember your password ?</span>
                    <a href="javascript:;" onclick="auth_content('login_page');" class="link-primary fw-bold fs-5" data-kt-translate="password-reset-head-link">Sign In</a>
                </div>
            </div>
            <div class="py-20 mt-20">
                <form class="form w-100 mt-20" novalidate="novalidate" id="form_forgot" data-kt-redirect-url="../../demo1/dist/authentication/layouts/fancy/new-password.html" action="#">
                    <div class="text-start mb-10">
                        <h1 class="text-dark mb-3 fs-3x" data-kt-translate="password-reset-title">Forgot Password ?</h1>
                        <div class="text-gray-400 fw-semibold fs-6" data-kt-translate="password-reset-desc">Enter your email to reset your password.</div>
                    </div>
                    <div class="fv-row mb-10">
                        <input class="form-control form-control-solid" type="email" placeholder="Email" id="email_forgot" name="email" autocomplete="off" data-kt-translate="password-reset-input-email" />
                    </div>
                    <div class="d-flex flex-stack">
                        <div class="m-0">
                            <button id="kt_password_reset_submit" class="btn btn-primary me-2" data-kt-translate="password-reset-submit">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                            <a href="javascript:;" onclick="auth_content('login_page');" class="btn btn-lg btn-light-primary fw-bold" data-kt-translate="password-reset-cancel">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="m-0">
            <button class="btn btn-flex btn-link rotate" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" data-kt-menu-offset="0px, 0px">
                <img data-kt-element="current-lang-flag" class="w-25px h-25px rounded-circle me-3" src="{{asset('keenthemes/media/flags/united-states.svg')}}" alt="" />
                <span data-kt-element="current-lang-name" class="me-2">English</span>
                <span class="svg-icon svg-icon-3 svg-icon-muted rotate-180 m-0">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="currentColor" />
                    </svg>
                </span>
            </button>
            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-4" data-kt-menu="true" id="kt_auth_lang_menu">
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="English">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/united-states.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">English</span>
                    </a>
                </div>
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="Spanish">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/spain.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">Spanish</span>
                    </a>
                </div>
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="German">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/germany.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">German</span>
                    </a>
                </div>
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="Japanese">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/japan.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">Japanese</span>
                    </a>
                </div>
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="French">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/france.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">French</span>
                    </a>
                </div>
                <div class="menu-item px-3">
                    <a href="javascript:;" class="menu-link d-flex px-5" data-kt-lang="Indonesia">
                        <span class="symbol symbol-20px me-4">
                            <img data-kt-element="lang-flag" class="rounded-1" src="{{asset('keenthemes/media/flags/indonesia.svg')}}" alt="" />
                        </span>
                        <span data-kt-element="lang-name">Indonesia</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    @section('custom_js')
        <script>
            auth_content('login_page');
        </script>
    @endsection
</x-office-layout>