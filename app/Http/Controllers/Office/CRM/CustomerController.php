<?php

namespace App\Http\Controllers\Office\CRM;

use App\Http\Controllers\Controller;
use App\Models\CRM\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.crm.customer.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.crm.customer.input', ['data' => new Customer]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Customer $customer)
    {
        //
    }
    public function edit(Customer $customer)
    {
        return view('pages.office.crm.customer.input', ['data' => $customer]);
    }
    public function update(Request $request, Customer $customer)
    {
        //
    }
    public function destroy(Customer $customer)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Customer::where('email','LIKE','%'.$request->keyword.'%')->orWhere('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.crm.customer.list',compact('collection'));
    }
}
