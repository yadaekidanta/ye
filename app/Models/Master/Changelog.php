<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Changelog extends Model
{
    use HasFactory;
    public function detail_news()
    {
        return $this->hasMany(ChangelogDetail::class)->where('type','New');
    }
    public function detail_update()
    {
        return $this->hasMany(ChangelogDetail::class)->where('type','Update');
    }
    public function detail_fix()
    {
        return $this->hasMany(ChangelogDetail::class)->where('type','Fix');
    }
}
