$("body").on("contextmenu", "img", function(e) {
    return false;
});
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function number_only(obj) {
    $('#' + obj).bind('keypress', function (event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
}
function format_email(obj) {
    $('#' + obj).bind('keypress', function (event) {
        var regex = new RegExp("^[A-Za-z0-9@_.]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
}
function custom_message(type, msg) {
    Swal.fire({
        showClass: {
            popup: 'animate__animated animate__fadeInDownBig'
        },
        hideClass: {
            popup: 'animate__animated animate__hinge'
        },
        position: 'top-end',
        icon: type,
        title: msg,
        showConfirmButton: false,
        timer: 2000
    });
}
function auth_content(cont){
    $('#login_page').hide();
    $('#register_page').hide();
    $('#forgot_page').hide();
    if(cont == "login_page"){
        $('#form_login')[0].reset();
        $("#email_login").focus();
    }
    if(cont == "register_page"){
        $('#form_register')[0].reset();
        $("#fullname").focus();
    }
    if(cont == "forgot_page"){
        $('#form_forgot')[0].reset();
        $("#email_forgot").focus();
    }
    $('#' + cont).show();
}
$("#email_login").focus();
$("#form_reset").on('keydown', 'input', function (event) {
    if (event.which == 9 || event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-reset'));
        var val = $($this).val();
        if(index < 3){
            $('[data-reset="' + (index + 1).toString() + '"]').focus();
        }else{
            $('#tombol_reset').trigger("click");
        }
    }
});
$("#form_login").on('keydown', 'input', function (event) {
    if (event.which == 9 || event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-login'));
        var val = $($this).val();
        if(index < 3){
            if(index == 1){
                if(val.length > 0){
                    var validateMail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if(!validateMail.test(val)){
                        custom_message('info',$($this).data('format'))
                    }else{
                        $('[data-login="' + (index + 1).toString() + '"]').focus();
                    }
                }else{
                    custom_message('info',$($this).data('validation'))
                }
            }else if(index == 2){
                if(val.length < 1){
                    custom_message('info',$($this).data('validation'))
                }else if(val.length < 8){
                    custom_message('info',$($this).data('format'))
                }else{
                    $('#tombol_login').trigger("click");
                }
            }
        }else{
            $('#tombol_login').trigger("click");
        }
    }
});
$("#form_register").on('keydown', 'input', function (event) {
    if (event.which == 9 || event.which == 13) {
        event.preventDefault();
        var $this = $(event.target);
        var index = parseFloat($this.attr('data-register'));
        var val = $($this).val();
        if(index < 15){
            if(index == 1){
                if(val.length < 1){
                    error_toastr($($this).data('validation'));
                }else if(val.length < 16){
                    error_toastr($($this).data('format'));
                }else{
                    $('[data-register="' + (index + 1).toString() + '"]').focus();
                }
            }else if(index == 8){
                if(val.length > 0){
                    var validatePhone = /^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})$/;
                    if(!validatePhone.test(val)){
                        error_toastr($($this).data('format'));
                    }else{
                        $('[data-register="' + (index + 1).toString() + '"]').focus();
                    }
                }else{
                    error_toastr($($this).data('validation'));
                }
            }else if(index == 9){
                if(val.length > 0){
                    var validateMail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if(!validateMail.test(val)){
                        error_toastr($($this).data('format'));
                    }else{
                        $('[data-register="' + (index + 1).toString() + '"]').focus();
                    }
                }else{
                    error_toastr($($this).data('validation'));
                }
            }else if(index == 10 || index == 11){
                if(val.length < 1){
                    error_toastr($($this).data('validation'));
                }else if(val.length < 8){
                    error_toastr($($this).data('format'));
                }else{
                    $('[data-register="' + (index + 1).toString() + '"]').focus();
                }
            }else{
                $('[data-register="' + (index + 1).toString() + '"]').focus();
            }
        }else{
            $('#tombol_register').trigger("click");
        }
    }
});
function handle_post(tombol, form, url, method)
{
    $(document).one('submit', form, function(e) {
        let data = new FormData(this);
        data.append('_method', method);
        $(tombol).prop("disabled", true);
        $(tombol).attr("data-kt-indicator","on");
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(response) {
                if (response.alert=="success") {
                    custom_message(response.alert,response.message);
                    setTimeout(function () {
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                        if(response.callback == "dashboard"){
                            location.href = response.callback;
                        }else if(response.callback == "forgot" || response.callback =="register"){
                            $(form)[0].reset();
                            auth_content('login_page');
                        }else if(response.callback == "auth"){
                            location.href = response.route;
                        }
                    }, 4000);
                } else {
                    custom_message(response.alert,response.message);
                    setTimeout(function () {
                        $(tombol).prop("disabled", false);
                        $(tombol).removeAttr("data-kt-indicator");
                    }, 4000);
                }
            },
        });
        return false;
    });
}