<?php

namespace App\Http\Controllers\Office\HRM;

use App\Models\HRM\Position;
use Illuminate\Http\Request;
use App\Models\HRM\Permission;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.position.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.hrm.position.input',['data' => new Position]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, Position $position)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.position.show',['data' => $position]);
        }
        return view('pages.office.theme');
    }
    public function edit(Position $position)
    {
        return view('pages.office.hrm.position.input',['data' => $position]);
    }
    public function update(Request $request, Position $position)
    {
        //
    }
    public function destroy(Position $position)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Position::get();
        return view('pages.office.hrm.position.list',compact('collection'));
    }
    public function show_list(Request $request, Position $position)
    {
        $collection = $position->permission;
        return view('pages.office.hrm.position.show_list',compact('collection'));
    }
    public function show_create(Position $position)
    {
        return view('pages.office.hrm.position.show_input', ['data' => new Permission, 'position' => $position]);
    }
    public function show_edit(Position $position, Permission $permission)
    {
        return view('pages.office.hrm.position.show_input', ['data' => $permission, 'position' => $position]);
    }
}
