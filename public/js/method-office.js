// $("body").on("contextmenu", "img", (e) => {return false});
// var audio = document.getElementById("audio");
// $('img').attr('draggable', false);
// $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
// $(document).ready(() => { $(window).keydown((event) => { (event.keyCode == 13) ? event.preventDefault() : false }) });
// let page, split;
// $(window).on('hashchange',() => (window.location.hash) ? [page = window.location.hash.replace('#', '') , (page == Number.NaN || page <= 0) ? false : load_list(page)] : false);
// const main_content = (obj) => {$("#content_list").hide(), $("#content_input").hide(), $("#" + obj).show()};
// const load_list = (page) => {$.get('?page=' + page, $('#content_filter').serialize(), (result) => { $('#list_result').html(result), main_content('content_list')}, "html")};
// $(document).ready(() => $(document).on('click', '.paginasi', (event) => [page = event.target.attributes[1].value, split = page.split('page=')[1], event.preventDefault() , load_list(split)]));
// const load_input = (url) => { $.get(url, {}, (result) => { $('#content_input').html(result), main_content('content_input')}, "html")};
// const handle_open_modal = (url, modal,content) => $.ajax({ type: "POST", url: url, success: (html) => { $(modal).modal('show'), $(content).html(html)}, error: () => { $(content).html('<h3>Ajax Bermasalah !!!</h3>')}});
// const handle_save = (tombol,form,url,method) => [$(tombol).submit(() => { return false}), data = $(form).serialize(), $(tombol).prop("disabled", true), $.ajax({type: method, url: url,data: data,dataType: 'json',beforeSend:() => {},success: (response) => {(response.alert == "success") ? success_toastr(response.message) && $(form)[0].reset() && $(tombol).prop("disabled", false) (response.redirect == "input") ? load_input(response.route) : (response.redirect == "reload") ?? location.reload() : setTimeout(() => {load_list(1)},2000)}})];
// const handle_upload = (tombol,form,url,method) => { $(document).one('submit', form, (e) => {[e.preventDefault(), data = new FormData(document.getElementById(e.target.id)), data.append('_method',method), $(tombol).prop("disabled", true), $.ajax({type:'POST', url:url,data: data,enctype: 'multipart/form-data',cache: false,contentType: false,resetForm: true,processData: false,dataType: 'json',beforeSend: () => {},success: (response) => {(response.alert == "success") ? [success_toastr(response.message), $(form)[0].reset(), load_list(1), $("#customModal").modal('hide')] : [error_toastr(response.message) , setTimeout(() => { $(tombol).prop("disabled", false)}, 2000)]}})]})};
// const handle_confirm = (title, confirm_title, deny_title, method, route) => Swal.fire({ title: title, showDenyButton: true, showCancelButton: false, confirmButtonText: confirm_title, denyButtonText: deny_title}).then((result) => { (result.isConfirmed) ? [id = [], $(':checkbox:checked').each((i) => {id[i] = $(this).val()}) (id.length === 0) ? Swal.fire('Please Select atleast one checkbox', '', 'info') : $.ajax({type: method,url: route,data: {id: id},dataType: 'json',success: (response) => { (response.redirect == "cart") ? load_cart(localStorage.getItem("route_cart")) : (response.redirect == "reload") ?? location.reload() ,load_list(1), Swal.fire(response.message, '', response.alert)}})] : Swal.fire('Konfirmasi dibatalkan', '', 'info')});
$("body").on("contextmenu", "img", function(e) {
    return false;
});
function load_url(url){
    window.history.pushState(null, null, url);
    load_main(url);
}
var audio = document.getElementById("audio");
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            // load_main(window.location.href);
        }
    });
});
let page;
$(window).on('hashchange', function() {
    if (window.location.hash) {
        page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            load_list(page);
        }
    }
});
$(document).ready(function() {
    $(document).on('click', '.paginasi', function(event) {
        event.preventDefault();
        $('.paginasi').removeClass('active');
        $(this).parent('.paginasi').addClass('active');
        // var myurl = $(this).attr('href');
        page = $(this).attr('halaman').split('page=')[1];
        var uri = window.location.href;
        var pathname = new URL(uri).pathname;
        var split = pathname.split('/');
        var code = split[3];
        if(code){
            prefix = 'show-list';
        }else{
            prefix = 'list';
        }
        var custom = $('#kt_app_content_container').find(".konten").attr("id");
        load_list(window.location.href+'/'+prefix,page,custom);
    });
    $(document).on('click', 'a.sidebar-link', function(event) {
        $('a.sidebar-link').removeClass('active');
        $(this).addClass('active');
    });
});
function main_content(obj){
    $("#content_list").hide();
    $("#content_input").hide();
    $("#" + obj).show();
}
function load_main(url){
    loading();
    $.get(url,{}, function(result) {
        loaded();
        $('#main-content').html(result);
    }, "html");
}
function load_list(url,page,custom){
    $.get(url + '?page=' + page, $('#content_filter').serialize(), function(result) {
        $('#' + custom).html(result);
        main_content('content_list');
    }, "html");
}
function load_input(url){
    $.get(url, {}, function(result) {
        $('#content_input').html(result);
        main_content('content_input');
    }, "html");
}
function handle_open_modal(url,modal,content,method){
    $.ajax({
        type: method,
        url: url,
        success: function (html) {
            $(modal).modal('show');
            $(content).html(html);
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function save_memo(){
    
}
const options = {
    modules: {
        toolbar: true
    },
    theme: "snow",
    enable: false
};
var editorMemo = document.getElementById('memo_body'),
editorContainer = document.getElementById('form_input_memo'),
editorEditMemo = document.getElementById('edit_memo_body'),
editorEditContainer = document.getElementById('form_edit_memo');
var quill = new Quill(editorMemo, options);
var quills = new Quill(editorEditMemo, options);
function load_memo(url){
    $.get(url,{}, function(result) {
        $("#input_memo").hide();
        $("#edit_memo").hide();
        $("#list_memos").show();
        $("#list_memos").html(result);
    });
    editorContainer.classList.add('inactive');
    quill.enable(false);
    editorEditContainer.classList.add('inactive');
    quills.enable(false);
}
function create_memo(){
    $("#input_memo").show();
    editorEditContainer.classList.add('inactive');
    quills.enable(false);
    editorContainer.classList.remove('inactive')
    quill.enable(true);
    $("#edit_memo").hide();
    $("#list_memos").hide();
    var Delta = Quill.import('delta');

    // Store accumulated changes
    var change = new Delta();
    quill.on('text-change', function (delta) {
        change = change.compose(delta);
    });

    // Save periodically
    setInterval(function () {
        if (change.length() > 0) {
            console.log('Saving changes', change);
            /*
            Send partial changes
            $.post('/your-endpoint', {
            partial: JSON.stringify(change)
            });

            Send entire document
            $.post('/your-endpoint', {
            doc: JSON.stringify(quill.getContents())
            });
            */
            change = new Delta();
        }
    }, 5 * 1000);

    // Check for unsaved data
    window.onbeforeunload = function () {
        if (change.length() > 0) {
            return 'There are unsaved changes. Are you sure you want to leave?';
        }
    }
}
function edit_memo(id,title,body){
    $("#input_memo").hide();
    $("#edit_memo").show();
    $("#id_edit_memo").val(id);
    $("#title_edit_memo").val(title);
    $("#edit_memo_body").val(body);
    editorContainer.classList.add('inactive')
    quill.enable(false);
    editorEditContainer.classList.remove('inactive');
    quills.enable(true);
    $("#list_memos").hide();
}
function handle_open_drawer(url,content,method){
    $.ajax({
        type: method,
        url: url,
        success: function (html) {
            $(content).html(html);
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function handle_save(tombol, form, url, method){
    $(tombol).submit(function() {
        return false;
    });
    $(tombol).attr("data-kt-indicator","on");
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(response) {
            if (response.alert == "success") {
                success_toastr(response.message);
                $(form)[0].reset();
                $(tombol).prop("disabled", false);
                if(response.redirect == "input"){
                    load_input(response.route);
                }else if(response.redirect == "reload"){
                    location.reload();
                }else{
                    setTimeout(function() {
                        load_main(window.location.href,1);
                    }, 2000);
                }
            } else {
                error_toastr(response.message);
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                }, 2000);
            }
        },
    });
}
function handle_confirm(title, confirm_title, deny_title, method, route){
    Swal.fire({
        showClass: {
            popup: 'animate__animated animate__fadeInDownBig'
        },
        hideClass: {
            popup: 'animate__animated animate__hinge'
        },
        position: 'top-end',
        title: title,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: confirm_title,
        denyButtonText: deny_title,
    }).then((result) => {
        if (result.isConfirmed) {
            loading();
            $.ajax({
                type: method,
                url: route,
                dataType: 'json',
                success: function(response) {
                    loaded();
                    if(response.redirect == "cart"){
                        load_cart(localStorage.getItem("route_cart"));
                    }else if(response.redirect == "reload"){
                        location.reload();
                    }else{
                        load_main(window.location.href);
                    }
                    custom_message(response.alert,response.message);
                }
            });
        } else if (result.isDenied) {
            custom_message(response.alert,response.message);
        }
    });
}
function handle_confirm_checked(title, confirm_title, deny_title, method, route){
    Swal.fire({
        title: title,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: confirm_title,
        denyButtonText: deny_title,
    }).then((result) => {
        if (result.isConfirmed) {
            var id = [];
            $('input[name="list_id"]:checkbox:checked').each(function(i){
                id[i] = $(this).val();
            });
            if(id.length === 0){
                Swal.fire('Please Select atleast one checkbox', '', 'info')
            }else{
                $.ajax({
                    type: method,
                    url: route,
                    data: {id: id},
                    dataType: 'json',
                    success: function(response) {
                        if(response.redirect == "cart"){
                            load_cart(localStorage.getItem("route_cart"));
                        }else if(response.redirect == "reload"){
                            location.reload();
                        }else{
                            load_main(window.location.href);
                        }
                        Swal.fire(response.message, '', response.alert)
                    }
                });
            }
        } else if (result.isDenied) {
            Swal.fire('Konfirmasi dibatalkan', '', 'info')
        }
    });
}
function handle_upload(tombol, form, url, method){
    $(document).one('submit', form, function(e) {
        let data = new FormData(this);
        data.append('_method', method);
        $(tombol).attr("data-kt-indicator","on");
        $(tombol).prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(response) {
                custom_message(response.alert,response.message);
                $(tombol).removeAttr("data-kt-indicator");
                if (response.alert == "success") {
                    $(form)[0].reset();
                    if(response.redirect == "input"){
                        load_input(response.route);
                    }else if(response.redirect == "reload"){
                        location.reload();
                    }else{
                        setTimeout(function() {
                            load_main(window.location.href);
                        }, 2000);
                    }
                    $("#customModal").modal('hide');
                } else {
                    setTimeout(function() {
                        $(tombol).prop("disabled", false);
                    }, 2000);
                }
            },
        });
        return false;
    });
}
function handle_upload_inform(tombol, form, url, method,route){
    $(document).one('submit', form, function(e) {
        let data = new FormData(this);
        data.append('_method', method);
        $(tombol).attr("data-kt-indicator","on");
        $(tombol).prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(response) {
                custom_message(response.alert,response.message);
                $(tombol).removeAttr("data-kt-indicator");
                if (response.alert == "success") {
                    $(form)[0].reset();
                    load_input(route);
                } else {
                    setTimeout(function() {
                        $(tombol).prop("disabled", false);
                    }, 2000);
                }
            },
        });
        return false;
    });
}
function send_chat(tombol, form, url, method){
    $(document).one('submit', form, function(e) {
        let data = new FormData(this);
        data.append('_method', method);
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(response) {
                if (response.alert == "success") {
                    $(form)[0].reset();
                    load_main(window.location.href);
                } else {
                    error_toastr(response.message);
                    setTimeout(function() {
                        $(tombol).prop("disabled", false);
                    }, 2000);
                }
            },
        });
        return false;
    });
}
function handle_download(tombol,url){
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.get(url, function(result) {
        if (result.alert == "success") {
            // $(download).attr({target: '_blank', href  : result.url, 'download' : result.url});
            // var href = $('.cssbuttongo').attr('href');
            window.open(result.url, '_blank');
            window.open(result.url1, '_blank');
            window.open(result.url2, '_blank');
            Swal.fire({ text: result.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                load_main(window.location.href);
            });
        }else{
            Swal.fire({ text: result.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
        }
        $(tombol).prop("disabled", false);
        $(tombol).removeAttr("data-kt-indicator");
    }, "json");
}