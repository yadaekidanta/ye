<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Changelog;
use Illuminate\Http\Request;

class ChangelogController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.changelog.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Changelog $changelog)
    {
        //
    }
    public function edit(Changelog $changelog)
    {
        //
    }
    public function update(Request $request, Changelog $changelog)
    {
        //
    }
    public function destroy(Changelog $changelog)
    {
        //
    }
    public function list()
    {
        $collection = Changelog::get();
        return view('pages.office.changelog.list',compact('collection'));
    }
}
