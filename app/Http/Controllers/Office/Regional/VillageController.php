<?php

namespace App\Http\Controllers\Office\Regional;

use App\Http\Controllers\Controller;
use App\Models\Regional\Village;
use Illuminate\Http\Request;

class VillageController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.regional.village.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.regional.village.input', ['data' => new Village]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Village $village)
    {
        //
    }
    public function edit(Village $village)
    {
        return view('pages.office.regional.village.input', ['data' => $village]);
    }
    public function update(Request $request, Village $village)
    {
        //
    }
    public function destroy(Village $village)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Village::where('id','LIKE','%'.$request->keyword.'%')->orWhere('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.regional.village.list',compact('collection'));
    }
}
