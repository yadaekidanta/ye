<?php

namespace App\Models\HRM;

use App\Models\Master\Module;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permission extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function position ()
    {
        return $this->belongsTo(Position::class);
    }
    public function module ()
    {
        return $this->belongsTo(Module::class);
    }
}
