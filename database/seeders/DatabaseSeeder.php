<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionalSeeder::class,
            MasterSeeder::class,
            HrmSeeder::class,
            // CrmSeeder::class,
            // SettingSeeder::class,
        ]);
    }
}
