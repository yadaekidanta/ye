<?php

namespace App\Models\Regional;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    public function regency()
    {
        return $this->hasMany(Regency::class);
    }
}
