<?php

namespace App\Http\Controllers\Office\Communication;

use Illuminate\Http\Request;
use App\Models\Communication\Chat;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.communication.chat.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Chat $chat)
    {
        //
    }
    public function edit(Chat $chat)
    {
        //
    }
    public function update(Request $request, Chat $chat)
    {
        //
    }
    public function destroy(Chat $chat)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Chat::whereRaw('(to_id = '.Auth::guard('office')->user()->id.') OR (from_id = '.Auth::guard('office')->user()->id.')')->orderBy('created_at','DESC')->get();
        return view('pages.office.communication.chat.list',compact('collection'));
    }
}
