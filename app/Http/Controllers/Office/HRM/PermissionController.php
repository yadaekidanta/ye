<?php

namespace App\Http\Controllers\Office\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Permission $permission)
    {
        //
    }
    public function edit(Permission $permission)
    {
        //
    }
    public function update(Request $request, Permission $permission)
    {
        //
    }
    public function destroy(Permission $permission)
    {
        //
    }
}
