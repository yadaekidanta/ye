<?php

namespace App\Http\Controllers\Office\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.employee.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.hrm.employee.input' , ['data' => new Employee]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, Employee $employee)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.employee.show',compact('employee'));
        }
        return view('pages.office.theme');
    }
    public function edit(Employee $employee)
    {
        return view('pages.office.hrm.employee.input' , ['data' => $employee]);
    }
    public function update(Request $request, Employee $employee)
    {
        //
    }
    public function destroy(Employee $employee)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Employee::paginate(5);
        return view('pages.office.hrm.employee.list',compact('collection'));
    }
}
