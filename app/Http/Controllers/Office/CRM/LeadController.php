<?php

namespace App\Http\Controllers\Office\CRM;

use App\Models\CRM\Lead;
use Illuminate\Http\Request;
use App\Models\CRM\ContactGroup;
use App\Models\CRM\LeadActivity;
use App\Http\Controllers\Controller;

class LeadController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.crm.lead.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        $group = ContactGroup::get();
        return view('pages.office.crm.lead.input', ['data' => new Lead, 'group' => $group]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, Lead $lead)
    {
        if($request->ajax())
        {
            return view('pages.office.crm.lead.show', ['data' => $lead]);
        }
        return view('pages.office.theme');
    }
    public function edit(Lead $lead)
    {
        $group = ContactGroup::get();
        return view('pages.office.crm.lead.input', ['data' => $lead, 'group' => $group]);
    }
    public function update(Request $request, Lead $lead)
    {
        //
    }
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Lead Deleted',
        ]);
    }
    public function list(Request $request)
    {
        $collection = Lead::where('email','LIKE','%'.$request->keyword.'%')->orWhere('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.crm.lead.list',compact('collection'));
    }
    public function show_list(Request $request, Lead $lead)
    {
        $collection = LeadActivity::where('id',$request->keyword)->orWhere('name','LIKE','%'.$request->keyword.'%')->where('lead_id',$lead->id)->paginate(10);
        return view('pages.office.crm.lead.show_list',compact('collection'));
    }
    public function show_create(Lead $lead)
    {
        return view('pages.office.crm.lead.show_input', ['data' => new LeadActivity, 'lead' => $lead]);
    }
    public function show_edit(Lead $lead, LeadActivity $leadActivity)
    {
        return view('pages.office.crm.lead.show_input', ['data' => $leadActivity, 'lead' => $lead]);
    }
}
