<?php

namespace App\Http\Controllers\Web;

use Location;
use Exception;
use Illuminate\Http\Request;
use App\Models\HRM\Department;
use App\Http\Controllers\Controller;
use App\Models\HRM\VacancyJob AS Career;

class CareerController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $collection = Career::select('department_id')->where('department_id','LIKE','%'.$request->keyword.'%')->groupBy('department_id')->get();
            return view('pages.web.career.list',compact('collection'));
        }
        $department = Department::get();
        $ip = $request->ip();
        $data = \Location::get($ip);
        return view('pages.web.career.main', compact('department'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Career $career)
    {
        return view('pages.web.career.show', ['data' => $career]);
    }
    public function edit(Career $career)
    {
        //
    }
    public function update(Request $request, Career $career)
    {
        //
    }
    public function destroy(Career $career)
    {
        //
    }
}
