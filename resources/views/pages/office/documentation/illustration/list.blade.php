<div class="card card-docs flex-row-fluid mb-2">
    <!--begin::Card Body-->
    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
        <!--begin::Tabs-->
        <ul class="nav nav-stretch nav-line-tabs fw-bold fs-4 border-bottom-2 border-transparent flex-nowrap mb-8" role="tablist" id="kt_illustration_tabs">
            <!--begin::Tab item-->
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2 active" data-bs-toggle="tab" href="#kt_illustration_sketchy-1" role="tab">Sketchy</a>
            </li>
            <!--end::Tab item-->
            <!--begin::Tab item-->
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_illustration_sigma-1" role="tab">Sigma</a>
            </li>
            <!--end::Tab item-->
            <!--begin::Tab item-->
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_illustration_dozzy-1" role="tab">Dozzy</a>
            </li>
            <!--end::Tab item-->
            <!--begin::Tab item-->
            <li class="nav-item">
                <a class="nav-link text-active-primary border-bottom-2" data-bs-toggle="tab" href="#kt_illustration_unitedpalms-1" role="tab">United Palms</a>
            </li>
            <!--end::Tab item-->
        </ul>
        <!--end::Tabs-->
        <!--begin::Tab content-->
        <div class="tab-content">
            <!--begin::Tab pane-->
            <div class="tab-pane fade show active" id="kt_illustration_sketchy-1">
                <!--begin::Description-->
                <div class="mb-0">30 Hand Drawn Sketch illustrations in 7 formats Figma, Sketch, Adobe Xd, Ai, PNG, JPG, SVG.</div>
                <!--end::Description-->
                <!--begin::More-->
                <div class="mb-5">
                    <a href="https://keenthemes.com/products/sketchy-pro" target="_blank" class="btn btn-flex btn-link btn-color-primary">Learn more
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                    <span class="svg-icon ms-2 svg-icon-3">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon--></a>
                </div>
                <!--end::More-->
                <!--begin::Row-->
                <div class="row g-10">
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/1.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/1-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-1"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/1.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/2.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/2-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-2"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/2.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/3.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/3-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-3"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/3.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/4.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/4-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-4"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/4.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/5.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/5-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-5"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/5.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/6.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/6-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-6"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/6.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/7.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/7-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-7"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/7.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/8.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/8-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-8"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/8.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/9.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/9-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-9"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/9.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/10.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/10-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-10"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/10.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/11.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/11-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-11"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/11.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/12.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/12-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-12"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/12.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/13.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/13-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-13"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/13.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/14.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/14-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-14"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/14.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/15.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/15-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-15"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/15.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/16.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/16-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-16"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/16.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/17.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/17-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-17"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/17.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/18.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/18-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-18"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/18.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/19.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/19-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-19"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/19.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sketchy-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/20.png')}}'); } [data-theme="dark"] .image-placeholder-sketchy-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/sketchy-1/20-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sketchy-1-20"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sketchy-1/20.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Tab pane-->
            <!--begin::Tab pane-->
            <div class="tab-pane fade" id="kt_illustration_sigma-1">
                <!--begin::Description-->
                <div class="mb-0">30 business illustrations in 7 formats Figma, Sketch, Adobe Xd, Ai, PNG, JPG, SVG. Bold and colorful addition to use on your project.</div>
                <!--end::Description-->
                <!--begin::More-->
                <div class="mb-5">
                    <a href="https://keenthemes.com/products/sigma-pro" target="_blank" class="btn btn-flex btn-link btn-color-primary">Learn more
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                    <span class="svg-icon ms-2 svg-icon-3">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon--></a>
                </div>
                <!--end::More-->
                <!--begin::Row-->
                <div class="row g-10">
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/1.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/1-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-1"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/1.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/2.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/2-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-2"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/2.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/3.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/3-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-3"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/3.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/4.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/4-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-4"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/4.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/5.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/5-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-5"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/5.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/6.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/6-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-6"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/6.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/7.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/7-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-7"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/7.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/8.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/8-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-8"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/8.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/9.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/9-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-9"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/9.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/10.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/10-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-10"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/10.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/11.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/11-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-11"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/11.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/12.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/12-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-12"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/12.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/13.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/13-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-13"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/13.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/14.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/14-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-14"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/14.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/15.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/15-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-15"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/15.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/16.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/16-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-16"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/16.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/17.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/17-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-17"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/17.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/18.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/18-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-18"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/18.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/19.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/19-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-19"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/19.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-sigma-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/20.png')}}'); } [data-theme="dark"] .image-placeholder-sigma-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/sigma-1/20-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-sigma-1-20"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/sigma-1/20.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Tab pane-->
            <!--begin::Tab pane-->
            <div class="tab-pane fade" id="kt_illustration_dozzy-1">
                <!--begin::Description-->
                <div class="mb-0">Tool in design projects that will catch your audience’s eye. Package includes 20 illustrations in SVG, PNG and JPG to enhance your UI.</div>
                <!--end::Description-->
                <!--begin::More-->
                <div class="mb-5">
                    <a href="https://keenthemes.com/products/doozy-pro" class="btn btn-flex btn-link btn-color-primary">Learn more
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                    <span class="svg-icon ms-2 svg-icon-3">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon--></a>
                </div>
                <!--end::More-->
                <!--begin::Row-->
                <div class="row g-10">
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/1.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/1-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-1"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/1.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/2.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/2-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-2"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/2.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/3.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/3-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-3"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/3.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/4.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/4-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-4"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/4.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/5.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/5-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-5"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/5.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/6.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/6-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-6"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/6.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/7.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/7-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-7"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/7.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/8.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/8-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-8"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/8.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/9.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/9-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-9"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/9.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/10.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/10-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-10"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/10.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/11.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/11-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-11"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/11.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/12.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/12-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-12"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/12.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/13.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/13-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-13"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/13.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/14.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/14-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-14"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/14.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/15.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/15-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-15"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/15.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/16.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/16-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-16"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/16.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/17.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/17-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-17"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/17.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/18.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/18-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-18"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/18.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/19.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/19-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-19"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/19.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-dozzy-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/20.png')}}'); } [data-theme="dark"] .image-placeholder-dozzy-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/dozzy-1/20-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-dozzy-1-20"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/dozzy-1/20.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Tab pane-->
            <!--begin::Tab pane-->
            <div class="tab-pane fade" id="kt_illustration_unitedpalms-1">
                <!--begin::Description-->
                <div class="mb-0">Graphics that support ethnic diversity. 30 vector illustrations in Figma, Sketch, Adobe XD, Ai, PNG, JPG, SVG add a contemporary twist to UI!</div>
                <!--end::Description-->
                <!--begin::More-->
                <div class="mb-5">
                    <a href="https://keenthemes.com/products/unitedpalms-pro" class="btn btn-flex btn-link btn-color-primary">Learn more
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                    <span class="svg-icon ms-2 svg-icon-3">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                            <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon--></a>
                </div>
                <!--end::More-->
                <!--begin::Row-->
                <div class="row g-10">
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/1.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-1 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/1-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-1"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/1.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/2.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-2 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/2-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-2"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/2.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/3.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-3 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/3-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-3"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/3.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/4.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-4 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/4-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-4"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/4.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/5.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-5 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/5-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-5"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/5.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/6.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-6 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/6-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-6"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/6.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/7.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-7 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/7-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-7"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/7.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/8.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-8 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/8-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-8"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/8.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/9.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-9 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/9-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-9"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/9.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/10.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-10 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/10-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-10"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/10.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/11.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-11 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/11-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-11"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/11.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/12.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-12 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/12-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-12"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/12.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/13.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-13 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/13-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-13"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/13.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/14.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-14 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/14-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-14"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/14.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/15.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-15 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/15-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-15"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/15.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/16.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-16 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/16-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-16"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/16.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/17.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-17 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/17-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-17"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/17.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/18.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-18 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/18-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-18"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/18.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/19.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-19 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/19-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-19"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/19.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                    <div class="col-lg-4">
                        <!--begin::Thumbnail-->
                        <!--begin::Image placeholder-->
                        <style>.image-placeholder-unitedpalms-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/20.png')}}'); } [data-theme="dark"] .image-placeholder-unitedpalms-1-20 { background-image: url('{{asset('keenthemes/media/illustrations/unitedpalms-1/20-dark.png')}}'); }</style>
                        <!--end::Image placeholder-->
                        <div class="card overlay border">
                            <div class="card-body p-0">
                                <div class="overlay-wrapper h-300px bgi-no-repeat bgi-size-contain bgi-position-center image-placeholder-unitedpalms-1-20"></div>
                                <div class="overlay-layer card-rounded bg-dark bg-opacity-10 align-items-end pb-3">
                                    <!--begin::Path-->
                                    <code class="py-2 px-4">assets/media/illustrations/unitedpalms-1/20.png</code>
                                    <!--end::Path-->
                                </div>
                            </div>
                        </div>
                        <!--end::Thumbnail-->
                    </div>
                </div>
                <!--end::Row-->
            </div>
            <!--end::Tab pane-->
        </div>
        <!--end::Tab content-->
    </div>
    <!--end::Card Body-->
</div>