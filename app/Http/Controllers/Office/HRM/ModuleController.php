<?php

namespace App\Http\Controllers\Office\HRM;

use App\Http\Controllers\Controller;
use App\Models\HRM\Module;
use App\Models\HRM\ModulePrivilege;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.module.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        $modules = Module::get();
        return view('pages.office.hrm.module.input',['data' => new Module, 'modules' => $modules]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, Module $module)
    {
        if($request->ajax())
        {
            return view('pages.office.hrm.module.show',['data' => $module]);
        }
        return view('pages.office.theme');
    }
    public function edit(Module $module)
    {
        $modules = Module::get();
        return view('pages.office.hrm.module.input',['data' => $module, 'modules' => $modules]);
    }
    public function update(Request $request, Module $module)
    {
        //
    }
    public function destroy(Module $module)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Module::where('module_id',0)->get();
        return view('pages.office.hrm.module.list',compact('collection'));
    }
    public function show_list(Request $request, Module $module)
    {
        $collection = $module->privileges;
        return view('pages.office.hrm.module.show_list',compact('collection'));
    }
    public function show_create(Module $module)
    {
        return view('pages.office.hrm.module.show_input', ['data' => new ModulePrivilege, 'module' => $module]);
    }
    public function show_edit(Module $module, ModulePrivilege $modulePrivilege)
    {
        return view('pages.office.hrm.module.show_input', ['data' => $modulePrivilege, 'module' => $module]);
    }
}
