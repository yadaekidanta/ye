<?php

namespace App\Models\Corporate;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KpiObjective extends Model
{
    use HasFactory;
    public function kr ()
    {
        return $this->hasMany(KpiKeyResult::class);
    }
}
