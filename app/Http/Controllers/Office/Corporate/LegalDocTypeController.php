<?php

namespace App\Http\Controllers\Office\Corporate;

use App\Http\Controllers\Controller;
use App\Models\Corporate\LegalDocType;
use Illuminate\Http\Request;

class LegalDocTypeController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.corporate.doc-type.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.corporate.doc-type.input',['data' => new LegalDocType]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, LegalDocType $documentType)
    {
        if($request->ajax())
        {
            return view('pages.office.corporate.document.main',['data' => $documentType]);
        }
        return view('pages.office.theme');
    }
    public function edit(LegalDocType $legalDocType)
    {
        return view('pages.office.corporate.doc-type.input',['data' => $legalDocType]);
    }
    public function update(Request $request, LegalDocType $legalDocType)
    {
        //
    }
    public function destroy(LegalDocType $legalDocType)
    {
        //
    }
    public function list(Request $request)
    {
        if($request->ajax())
        {
            $collection = LegalDocType::where('name','LIKE','%'.$request->keyword.'%')->get();
            return view('pages.office.corporate.doc-type.list',compact('collection'));
        }
    }
    public function show_list(Request $request, LegalDocType $documentType)
    {
        if($request->ajax())
        {
            $collection = $documentType->doc;
            return view('pages.office.corporate.document.list',compact('collection','documentType'));
        }
    }
}
