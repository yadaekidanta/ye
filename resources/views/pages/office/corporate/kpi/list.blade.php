<table class="table table-rounded table-striped border gy-7 gs-7">
    <thead>
        <tr class="fw-semibold fs-6 text-gray-800 border-bottom border-gray-200">
            <th class="w-10px pe-2" rowspan="2">
                No
            </th>
            <th class="min-w-125px" rowspan="2">Functions</th>
            <th class="min-w-125px" rowspan="2">Objectives</th>
            <th class="min-w-125px" rowspan="2">Key Result</th>
            <th class="min-w-125px" rowspan="2">Description</th>
            <th class="min-w-125px" rowspan="2">Measure Unit</th>
            <th class="min-w-125px" rowspan="2">Owner</th>
            <th class="min-w-125px" rowspan="2">Weight</th>
            <th class="min-w-125px" rowspan="2">EOY {{date('Y')}}</th>
            <th class="min-w-125px" rowspan="2">EOY {{date('Y') - 1}}</th>
            <th class="min-w-125px" colspan="5">Ruler</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="12">1</td>
            <td rowspan="12">Customer</td>
            <td rowspan="6">1. Customer</td>
            <td rowspan="3">1. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Update values, description, and examples based on the interview</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Conduct 5 talks during the weekly team meetings on the examples of how we use our values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="3">2. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Update values, description, and examples based on the interview</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Conduct 5 talks during the weekly team meetings on the examples of how we use our values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="6">2. Purchase</td>
            <td rowspan="3">1. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="3">2. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="12">2</td>
            <td rowspan="12">Client</td>
            <td rowspan="6">1. Customer</td>
            <td rowspan="3">1. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Update values, description, and examples based on the interview</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Conduct 5 talks during the weekly team meetings on the examples of how we use our values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="3">2. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Update values, description, and examples based on the interview</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Conduct 5 talks during the weekly team meetings on the examples of how we use our values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="6">2. Purchase</td>
            <td rowspan="3">1. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td rowspan="3">2. SOV - Share of Voice</td>
            <td>1. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>2. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
        <tr>
            <td>3. Interview 25 members of the team (15% of total) on our culture and values</td>
            <td>% Voice in digital</td>
            <td>Marketing</td>
            <td>5%</td>
            <td>2.9%</td>
            <td>10%</td>
            <td> < 7 % </td>
            <td> 7 - 9.9% </td>
            <td>10%</td>
            <td>12%</td>
            <td>12%</td>
        </tr>
    </tbody>
</table>
<table class="table table-striped border align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
            <th class="w-10px pe-2" rowspan="2">
                No
            </th>
            <th class="min-w-125px" rowspan="2">Functions</th>
            <th class="min-w-125px" rowspan="2">Objectives</th>
            <th class="min-w-125px" rowspan="2">Key Result</th>
            <th class="min-w-125px" rowspan="2">Description</th>
            <th class="min-w-125px" rowspan="2">Measure Unit</th>
            <th class="min-w-125px" rowspan="2">Owner</th>
            <th class="min-w-125px" rowspan="2">Weight</th>
            <th class="min-w-125px" rowspan="2">EOY {{date('Y')}}</th>
            <th class="min-w-125px" rowspan="2">EOY {{date('Y') - 1}}</th>
            <th class="min-w-125px" colspan="5">Ruler</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
        </tr>
    </thead>
    <tbody class="text-gray-600 fw-semibold">
        @foreach ($collection as $key => $item)
        <tr>
            <td>
                {{$key+ $collection->firstItem()}}
            </td>
            <td onclick="load_url('{{route('office.corporate.kpi.edit',$item->id)}}');">{{$item->function}}</td>
            @if($item->objective->count() > 0)
                @foreach ($item->objective as $keys => $objective)
                <td class="d-flex align-items-center" colspan="{{$objective->kr->count() > 0 ? $objective->kr->count() : 0}}">
                    {{++$keys}}. {{$objective->objective}}
                </td>
                @endforeach
            @endif
            <td>
                @if($item->objective->count() > 0)
                    @foreach ($item->objective as $keys => $objective)
                        @if($objective->kr->count() > 0)
                            @foreach ($objective->kr as $keyss => $kr)
                                {{++$keyss}}. {{$kr->key_result}} <br>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('themes.office.pagination')}}