<?php

namespace App\Http\Controllers\Office\Corporate;

use App\Http\Controllers\Controller;
use App\Models\Corporate\Kpi;
use Illuminate\Http\Request;

class KpiController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.corporate.kpi.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Kpi $kpi)
    {
        //
    }
    public function edit(Kpi $kpi)
    {
        //
    }
    public function update(Request $request, Kpi $kpi)
    {
        //
    }
    public function destroy(Kpi $kpi)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Kpi::paginate(5);
        return view('pages.office.corporate.kpi.list',compact('collection'));
    }
}
