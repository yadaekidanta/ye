<div class="project item">
    <div class="row">
        <figure class="col-lg-8 col-xl-7 offset-xl-1 rounded"> <img src="{{asset('sandbox/img/photos/cs16.jpg')}}" alt="" /></figure>
        <div class="project-details d-flex justify-content-center flex-column" style="right: 10%; bottom: 25%;">
            <div class="card shadow rellax" data-rellax-xs-speed="0" data-rellax-mobile-speed="0">
                <div class="card-body">
                    <div class="post-header">
                        <div class="post-category text-line text-purple mb-3">Cosmetic</div>
                        <h2 class="post-title mb-3">Cras Fermentum Sem</h2>
                    </div>
                    <!-- /.post-header -->
                    <div class="post-content">
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                        <a href="#" class="more hover link-purple">See Project</a>
                    </div>
                    <!-- /.post-content -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.project-details -->
    </div>
    <!-- /.row -->
</div>
<!-- /.project -->
<div class="project item">
    <div class="row">
        <figure class="col-lg-7 offset-lg-5 col-xl-6 offset-xl-5 rounded"> <img src="{{asset('sandbox/img/photos/cs17.jpg')}}" alt="" /></figure>
        <div class="project-details d-flex justify-content-center flex-column" style="left: 18%; bottom: 25%;">
            <div class="card shadow rellax" data-rellax-xs-speed="0" data-rellax-mobile-speed="0">
                <div class="card-body">
                    <div class="post-header">
                        <div class="post-category text-line text-leaf mb-3">Coffee</div>
                        <h2 class="post-title mb-3">Mollis Ipsum Mattis</h2>
                    </div>
                    <!-- /.post-header -->
                    <div class="post-content">
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nullam id dolor id nibh ultricies vehicula.</p>
                        <a href="#" class="more hover link-leaf">See Project</a>
                    </div>
                    <!-- /.post-content -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.project-details -->
    </div>
    <!-- /.row -->
</div>
<!-- /.project -->
{{$collection->links('themes.web.pagination')}}