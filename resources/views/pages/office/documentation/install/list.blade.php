<div class="card card-docs flex-row-fluid mb-2">
    <!--begin::Card Body-->
    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
        <!--begin::Notice-->
        <div class="d-flex align-items-center rounded py-5 px-4 bg-light-info">
            <!--begin::Icon-->
            <div class="d-flex h-80px w-80px flex-shrink-0 flex-center position-relative ms-3 me-6">
                <!--begin::Svg Icon | path: icons/duotune/abstract/abs051.svg-->
                <span class="svg-icon svg-icon-info position-absolute opacity-10">
                    <svg class=". w-80px h-80px ." xmlns="http://www.w3.org/2000/svg" width="70px" height="70px" viewBox="0 0 70 70" fill="none">
                        <path d="M28 4.04145C32.3316 1.54059 37.6684 1.54059 42 4.04145L58.3109 13.4585C62.6425 15.9594 65.3109 20.5812 65.3109 25.5829V44.4171C65.3109 49.4188 62.6425 54.0406 58.3109 56.5415L42 65.9585C37.6684 68.4594 32.3316 68.4594 28 65.9585L11.6891 56.5415C7.3575 54.0406 4.68911 49.4188 4.68911 44.4171V25.5829C4.68911 20.5812 7.3575 15.9594 11.6891 13.4585L28 4.04145Z" fill="currentColor" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
                <!--begin::Svg Icon | path: icons/duotune/art/art006.svg-->
                <span class="svg-icon svg-icon-3x svg-icon-info position-absolute">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M22 19V17C22 16.4 21.6 16 21 16H8V3C8 2.4 7.6 2 7 2H5C4.4 2 4 2.4 4 3V19C4 19.6 4.4 20 5 20H21C21.6 20 22 19.6 22 19Z" fill="currentColor" />
                        <path d="M20 5V21C20 21.6 19.6 22 19 22H17C16.4 22 16 21.6 16 21V8H8V4H19C19.6 4 20 4.4 20 5ZM3 8H4V4H3C2.4 4 2 4.4 2 5V7C2 7.6 2.4 8 3 8Z" fill="currentColor" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </div>
            <!--end::Icon-->
            <!--begin::Description-->
            <div class="text-gray-700 fw-bold fs-6 lh-lg">Metronic's
            <a class="fw-semibold me-1" href="https://gulpjs.com/" target="_blank">Gulp</a>build tools provide easy package management and production deployment for any type of web application that also comes with powerful asset bundle tools to organize assets structure with custom bundling for production.</div>
            <!--end::Description-->
        </div>
        <!--end::Notice-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="gulp-quick-start" data-kt-scroll-offset="50">
            <a href="#gulp-quick-start"></a>Gulp Quick Start</h1>
            <!--end::Heading-->
            <!--begin::List-->
            <ol class="py-5">
                <li class="pt-0 pb-3">Download the latest theme source from the
                <a href="https://1.envato.market/EA4JP" class="fw-semibold">Marketplace</a>.</li>
                <li class="py-3">Download and install Node.js from
                <a href="https://nodejs.org/en/download/" class="fw-semibold">Nodejs</a>. The suggested version to install is
                <code>16.x LTS</code>.</li>
                <li class="py-3">Start a command prompt window or terminal and change directory to
                <code>[unpacked path]/theme/tools/</code>:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">cd theme/tools/</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div></li>
                <li class="py-3">Install the latest NPM:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">npm install --global npm@latest</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div></li>
                <li class="py-3">Install Yarn via the NPM:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">npm install --global yarn</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
                <div class="pt-2 pb-5">
                    <!--begin::Information-->
                    <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                        <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Description-->
                        <div class="text-gray-700 fw-bold fs-6">Don't forget to run
                        <code>yarn upgrade</code>after every Metronic update is released in order to receive newly added or updated 3rd-party plugins.
                        <br />Use
                        <code>npm cache clean --force</code>command, if the installation had failed at any step. Retry again from beginning once it's done.</div>
                        <!--end::Description-->
                    </div>
                    <!--end::Information-->
                </div></li>
                <li class="py-3">Gulp is a toolkit that helps you automate your time-consuming tasks in development workflow. To install gulp globally:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">npm install --global gulp-cli</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>If you have previously installed a version of gulp globally, remove it to make sure old version doesn't collide with new gulp-cli:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">npm rm --global gulp</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>Verify that gulp in successfully installed, and version of installed gulp will appear (
                <code>CLI version: 2.3.0 Local version: 4.0.2</code>):
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --version</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
                <div class="pt-2 pb-5">
                    <!--begin::Information-->
                    <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                        <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Description-->
                        <div class="text-gray-700 fw-bold fs-6">if you're ran into a
                        <code>ReferenceError: primordials is not defined</code>error when running gulp in the terminal, you may have an incompatibility between the node version and gulp. Use below command to force install gulp version.
                        <br />
                        <code>npm install gulp@^4.0.2</code></div>
                        <!--end::Description-->
                    </div>
                    <!--end::Information-->
                </div></li>
                <li class="py-3">Install the Metronic dependencies in
                <code>[unpacked path]/theme/tools/</code>folder.
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">yarn</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
                <div class="pt-2 pb-5">
                    <!--begin::Information-->
                    <div class="d-flex align-items-center rounded py-5 px-5 bg-light-danger">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                        <span class="svg-icon svg-icon-3x svg-icon-danger me-5">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Description-->
                        <div class="text-gray-700 fw-bold fs-6">We recommend using
                        <code>Yarn</code>instead
                        <code>NPM</code>for the Metronic dependencies setup.
                        <code>Yarn</code>supports nested dependencies resolutions in
                        <code>package.json</code>where spesific version of sub dependacies are required such as
                        <code>resolutions: { "gulp-dart-sass/sass": "1.32.13" }</code>.</div>
                        <!--end::Description-->
                    </div>
                    <!--end::Information-->
                </div></li>
                <li class="py-3">The below command will compile all the assets(sass, js, media) to
                <code>dist/assets/</code>folder: State which demo to compile and append at the end of the command. Eg.
                <code>--demo1</code>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
                <div class="pt-2 pb-5">
                    <!--begin::Information-->
                    <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                        <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Description-->
                        <div class="text-gray-700 fw-bold fs-6">If you had any error at this step during compilation. Try to update
                        <code>gulp</code>to the latest version.
                        <code>npm install --global gulp-cli</code></div>
                        <!--end::Description-->
                    </div>
                    <!--end::Information-->
                </div></li>
                <li class="py-3">Start the localhost server:
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp localhost</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
                <div class="pt-2 pb-5">
                    <!--begin::Information-->
                    <div class="d-flex align-items-center rounded py-5 px-5 bg-light-danger">
                        <!--begin::Icon-->
                        <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                        <span class="svg-icon svg-icon-3x svg-icon-danger me-5">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                        <!--end::Icon-->
                        <!--begin::Description-->
                        <div class="text-gray-700 fw-bold fs-6">Keep the console open. Open this link to run
                        <a href="http://localhost:8080/demo1/dist/" target="_blank">
                            <code>http://localhost:8080/demo1/dist/</code>
                        </a>. It will take a few seconds for the build to finish. To stop a localhost environment, press
                        <code>Ctrl+C</code>.</div>
                        <!--end::Description-->
                    </div>
                    <!--end::Information-->
                </div></li>
            </ol>
            <!--end::List-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="build-options" data-kt-scroll-offset="50">
            <a href="#build-options"></a>Build Options</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="pt-5">The build config file is located at
            <code>tools/gulp.config.json</code>and you can fully customize the build settings to meet your project requirements.</div>
            <!--end::Block-->
            <!--begin::Code-->
            <div class="py-5">
                <!--begin::Highlight-->
                <div class="highlight">
                    <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                    <div class="highlight-code">
                        <pre class="language-javascript" style="height:600px">
<code class="language-javascript">{
"name": "{theme}",
"desc": "Gulp build config file",
"version": "{version}",
"config": {
"debug": false,
"compile": {
"rtl": {
"enabled": false,
"skip": [
"select2",
"line-awesome",
"fontawesome5",
"nouislider",
"tinymce",
"sweetalert2"
]
},
"jsUglify": false,
"cssMinify": true,
"jsSourcemaps": false,
"cssSourcemaps": false
},
"path": {
"src": "../../../themes/{theme}/html/src",
"common_src": "../src",
"node_modules": "node_modules"
},
"dist": [
"../../../themes/{theme}/html/dist/assets"
]
},
"build": {
"base": {
"src": {
"styles": [
"{$config.path.src}/sass/style.scss"
],
"scripts": [
"{$config.path.common_src}/js/components/**/*.js",
"{$config.path.common_src}/js/layout/**/*.js",
"{$config.path.src}/js/layout/**/*.js"
]
},
"dist": {
"styles": "{$config.dist}/css/style.bundle.css",
"scripts": "{$config.dist}/js/scripts.bundle.js"
}
},
"plugins": {
"global": {
"src": {
"mandatory": {
"jquery": {
"scripts": [
    "{$config.path.node_modules}/jquery/dist/jquery.js"
]
},
"popper.js": {
"scripts": [
    "{$config.path.node_modules}/@popperjs/core/dist/umd/popper.js"
]
},
"bootstrap": {
"scripts": [
    "{$config.path.node_modules}/bootstrap/dist/js/bootstrap.min.js"
]
},
"moment": {
"scripts": [
    "{$config.path.node_modules}/moment/min/moment-with-locales.min.js"
]
},
"perfect-scrollbar": {
"styles": [
    "{$config.path.node_modules}/perfect-scrollbar/css/perfect-scrollbar.css"
],
"scripts": [
    "{$config.path.node_modules}/perfect-scrollbar/dist/perfect-scrollbar.js"
]
},
"wnumb": {
"scripts": [
    "{$config.path.node_modules}/wnumb/wNumb.js"
]
}
},
"optional": {
"select2": {
"styles": [
    "{$config.path.node_modules}/select2/dist/css/select2.css"
],
"scripts": [
    "{$config.path.node_modules}/select2/dist/js/select2.full.js",
    "{$config.path.common_src}/js/vendors/plugins/select2.init.js"
]
},
"flatpickr": {
"styles": [
    "{$config.path.node_modules}/flatpickr/dist/flatpickr.css"
],
"scripts": [
    "{$config.path.node_modules}/flatpickr/dist/flatpickr.js",
    "{$config.path.common_src}/js/vendors/plugins/flatpickr.init.js"
]
},
"formvalidation": {
"styles": [
    "{$config.path.common_src}/plugins/formvalidation/dist/css/formValidation.css"
],
"scripts": [
    "{$config.path.node_modules}/es6-shim/es6-shim.min.js",
    "{$config.path.common_src}/plugins/formvalidation/dist/js/FormValidation.full.min.js",
    "{$config.path.common_src}/plugins/formvalidation/dist/js/plugins/Bootstrap5.min.js"
]
},
"daterangepicker": {
"styles": [
    "{$config.path.node_modules}/bootstrap-daterangepicker/daterangepicker.css"
],
"scripts": [
    "{$config.path.node_modules}/bootstrap-daterangepicker/daterangepicker.js"
]
},
"bootstrap-maxlength": {
"scripts": [
    "{$config.path.node_modules}/bootstrap-maxlength/src/bootstrap-maxlength.js"
]
},
"bootstrap-multiselectsplitter": {
"scripts": [
    "{$config.path.common_src}/plugins/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js"
]
},
"inputmask": {
"scripts": [
    "{$config.path.node_modules}/inputmask/dist/inputmask.js",
    "{$config.path.node_modules}/inputmask/dist/bindings/inputmask.binding.js",
    "{$config.path.node_modules}/inputmask/dist/lib/extensions/inputmask.date.extensions.js",
    "{$config.path.node_modules}/inputmask/dist/lib/extensions/inputmask.numeric.extensions.js"
]
},
"nouislider": {
"styles": [
    "{$config.path.node_modules}/nouislider/distribute/nouislider.css"
],
"scripts": [
    "{$config.path.node_modules}/nouislider/distribute/nouislider.js"
]
},
"autosize": {
"scripts": [
    "{$config.path.node_modules}/autosize/dist/autosize.js"
]
},
"clipboard": {
"scripts": [
    "{$config.path.node_modules}/clipboard/dist/clipboard.min.js"
]
},
"dropzone": {
"styles": [
    "{$config.path.node_modules}/dropzone/dist/dropzone.css"
],
"scripts": [
    "{$config.path.node_modules}/dropzone/dist/dropzone.js",
    "{$config.path.common_src}/js/vendors/plugins/dropzone.init.js"
]
},
"quil": {
"styles": [
    "{$config.path.node_modules}/quill/dist/quill.snow.css"
],
"scripts": [
    "{$config.path.node_modules}/quill/dist/quill.js"
]
},
"tiny-slider": {
"styles": [
    "{$config.path.node_modules}/tiny-slider/dist/tiny-slider.css"
],
"scripts": [
    "{$config.path.node_modules}/tiny-slider/dist/tiny-slider.js"
]
},
"tagify": {
"styles": [
    "{$config.path.node_modules}/@yaireo/tagify/dist/tagify.css"
],
"scripts": [
    "{$config.path.node_modules}/@yaireo/tagify/dist/tagify.polyfills.min.js",
    "{$config.path.node_modules}/@yaireo/tagify/dist/tagify.min.js"
]
},
"bootstrap-markdown": {
"styles": [
    "{$config.path.node_modules}/bootstrap-markdown/css/bootstrap-markdown.min.css"
],
"scripts": [
    "{$config.path.node_modules}/markdown/lib/markdown.js",
    "{$config.path.node_modules}/bootstrap-markdown/js/bootstrap-markdown.js",
    "{$config.path.common_src}/js/vendors/plugins/bootstrap-markdown.init.js"
]
},
"animate.css": {
"styles": [
    "{$config.path.node_modules}/animate.css/animate.css"
]
},
"toastr": {
"styles": [
    "{$config.path.node_modules}/toastr/build/toastr.css"
],
"scripts": [
    "{$config.path.node_modules}/toastr/build/toastr.min.js"
]
},
"dual-listbox": {
"styles": [
    "{$config.path.node_modules}/dual-listbox/dist/dual-listbox.css"
],
"scripts": [
    "{$config.path.node_modules}/dual-listbox/dist/dual-listbox.js"
]
},
"apexcharts": {
"styles": [
    "{$config.path.node_modules}/apexcharts/dist/apexcharts.css"
],
"scripts": [
    "{$config.path.node_modules}/apexcharts/dist/apexcharts.min.js"
]
},
"chart.js": {
"styles": [
    "{$config.path.node_modules}/chart.js/dist/Chart.css"
],
"scripts": [
    "{$config.path.node_modules}/chart.js/dist/Chart.js"
]
},
"bootstrap-session-timeout": {
"scripts": [
    "{$config.path.common_src}/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"
]
},
"jquery-idletimer": {
"scripts": [
    "{$config.path.common_src}/plugins/jquery-idletimer/idle-timer.min.js"
]
},
"countup.js": {
"scripts": [
    "{$config.path.node_modules}/countup.js/dist/countUp.umd.js"
]
},
"sweetalert2": {
"styles": [
    "{$config.path.node_modules}/sweetalert2/dist/sweetalert2.css"
],
"scripts": [
    "{$config.path.node_modules}/es6-promise-polyfill/promise.min.js",
    "{$config.path.node_modules}/sweetalert2/dist/sweetalert2.min.js",
    "{$config.path.common_src}/js/vendors/plugins/sweetalert2.init.js"
]
},
"line-awesome": {
"styles": [
    "{$config.path.node_modules}/line-awesome/dist/line-awesome/css/line-awesome.css"
],
"fonts": [
    "{$config.path.node_modules}/line-awesome/dist/line-awesome/fonts/**"
]
},
"bootstrap-icons": {
"styles": [
    "{$config.path.node_modules}/bootstrap-icons/font/bootstrap-icons.css"
],
"fonts": [
    "{$config.path.node_modules}/bootstrap-icons/font/fonts/**"
]
},
"@fortawesome": {
"styles": [
    "{$config.path.node_modules}/@fortawesome/fontawesome-free/css/all.min.css"
],
"fonts": [
    "{$config.path.node_modules}/@fortawesome/fontawesome-free/webfonts/**"
]
}
},
"override": {
"styles": [
"{$config.path.src}/sass/plugins.scss"
]
}
},
"dist": {
"styles": "{$config.dist}/plugins/global/plugins.bundle.css",
"scripts": "{$config.dist}/plugins/global/plugins.bundle.js",
"images": "{$config.dist}/plugins/global/images",
"fonts": "{$config.dist}/plugins/global/fonts"
}
},
"custom": {
"draggable": {
"src": {
"scripts": [
"{$config.path.node_modules}/@shopify/draggable/lib/draggable.bundle.js",
"{$config.path.node_modules}/@shopify/draggable/lib/draggable.bundle.legacy.js",
"{$config.path.node_modules}/@shopify/draggable/lib/draggable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/sortable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/droppable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/swappable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/plugins.js",
"{$config.path.node_modules}/@shopify/draggable/lib/plugins/collidable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/plugins/resize-mirror.js",
"{$config.path.node_modules}/@shopify/draggable/lib/plugins/snappable.js",
"{$config.path.node_modules}/@shopify/draggable/lib/plugins/swap-animation.js"
]
},
"dist": {
"scripts": "{$config.dist}/plugins/custom/draggable/draggable.bundle.js"
}
},
"prismjs": {
"src": {
"styles": [
"{$config.path.node_modules}/prism-themes/themes/prism-shades-of-purple.css"
],
"scripts": [
"{$config.path.node_modules}/prismjs/prism.js",
"{$config.path.node_modules}/prismjs/components/prism-bash.js",
"{$config.path.node_modules}/prismjs/components/prism-javascript.js",
"{$config.path.node_modules}/prismjs/components/prism-scss.js",
"{$config.path.node_modules}/prismjs/components/prism-css.js",
"{$config.path.node_modules}/prismjs/plugins/normalize-whitespace/prism-normalize-whitespace.js",
"{$config.path.common_src}/js/vendors/plugins/prism.init.js"
]
},
"dist": {
"styles": "{$config.dist}/plugins/custom/prismjs/prismjs.bundle.css",
"scripts": "{$config.dist}/plugins/custom/prismjs/prismjs.bundle.js"
}
},
"fullcalendar": {
"src": {
"styles": [
"{$config.path.node_modules}/@fullcalendar/core/main.css",
"{$config.path.node_modules}/@fullcalendar/daygrid/main.css",
"{$config.path.node_modules}/@fullcalendar/list/main.css",
"{$config.path.node_modules}/@fullcalendar/timegrid/main.css"
],
"scripts": [
"{$config.path.node_modules}/@fullcalendar/core/main.js",
"{$config.path.node_modules}/@fullcalendar/daygrid/main.js",
"{$config.path.node_modules}/@fullcalendar/google-calendar/main.js",
"{$config.path.node_modules}/@fullcalendar/interaction/main.js",
"{$config.path.node_modules}/@fullcalendar/list/main.js",
"{$config.path.node_modules}/@fullcalendar/timegrid/main.js"
]
},
"dist": {
"styles": "{$config.dist}/plugins/custom/fullcalendar/fullcalendar.bundle.css",
"scripts": "{$config.dist}/plugins/custom/fullcalendar/fullcalendar.bundle.js"
}
},
"gmaps": {
"src": {
"scripts": [
"{$config.path.node_modules}/gmaps/gmaps.js"
]
},
"dist": {
"scripts": "{$config.dist}/plugins/custom/gmaps/gmaps.js"
}
},
"flot": {
"src": {
"scripts": [
"{$config.path.node_modules}/flot/dist/es5/jquery.flot.js",
"{$config.path.node_modules}/flot/source/jquery.flot.resize.js",
"{$config.path.node_modules}/flot/source/jquery.flot.categories.js",
"{$config.path.node_modules}/flot/source/jquery.flot.pie.js",
"{$config.path.node_modules}/flot/source/jquery.flot.stack.js",
"{$config.path.node_modules}/flot/source/jquery.flot.crosshair.js",
"{$config.path.node_modules}/flot/source/jquery.flot.axislabels.js"
]
},
"dist": {
"scripts": "{$config.dist}/plugins/custom/flot/flot.bundle.js"
}
},
"datatables.net": {
"src": {
"styles": [
"{$config.path.node_modules}/datatables.net-bs4/css/dataTables.bootstrap4.css",
"{$config.path.node_modules}/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css",
"{$config.path.node_modules}/datatables.net-select-bs4/css/select.bootstrap4.min.css"
],
"scripts": [
"{$config.path.node_modules}/datatables.net/js/jquery.dataTables.js",
"{$config.path.common_src}/js/vendors/plugins/datatables.init.js",
"{$config.path.node_modules}/datatables.net-autofill/js/dataTables.autoFill.min.js",
"{$config.path.node_modules}/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js",
"{$config.path.node_modules}/jszip/dist/jszip.min.js",
"{$config.path.node_modules}/pdfmake/build/pdfmake.min.js",
"{$config.path.node_modules}/pdfmake/build/vfs_fonts.js",
"{$config.path.node_modules}/datatables.net-buttons/js/dataTables.buttons.min.js",
"{$config.path.node_modules}/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js",
"{$config.path.node_modules}/datatables.net-buttons/js/buttons.colVis.js",
"{$config.path.node_modules}/datatables.net-buttons/js/buttons.flash.js",
"{$config.path.node_modules}/datatables.net-buttons/js/buttons.html5.js",
"{$config.path.node_modules}/datatables.net-buttons/js/buttons.print.js",
"{$config.path.node_modules}/datatables.net-colreorder/js/dataTables.colReorder.min.js",
"{$config.path.node_modules}/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js",
"{$config.path.node_modules}/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
"{$config.path.node_modules}/datatables.net-keytable/js/dataTables.keyTable.min.js",
"{$config.path.node_modules}/datatables.net-responsive/js/dataTables.responsive.min.js",
"{$config.path.node_modules}/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js",
"{$config.path.node_modules}/datatables.net-rowgroup/js/dataTables.rowGroup.min.js",
"{$config.path.node_modules}/datatables.net-rowreorder/js/dataTables.rowReorder.min.js",
"{$config.path.node_modules}/datatables.net-scroller/js/dataTables.scroller.min.js",
"{$config.path.node_modules}/datatables.net-select/js/dataTables.select.min.js"
]
},
"dist": {
"styles": "{$config.dist}/plugins/custom/datatables/datatables.bundle.css",
"scripts": "{$config.dist}/plugins/custom/datatables/datatables.bundle.js"
}
},
"jkanban": {
"src": {
"styles": [
"{$config.path.node_modules}/jkanban/dist/jkanban.min.css"
],
"scripts": [
"{$config.path.node_modules}/jkanban/dist/jkanban.min.js"
]
},
"dist": {
"styles": "{$config.dist}/plugins/custom/kanban/kanban.bundle.css",
"scripts": "{$config.dist}/plugins/custom/kanban/kanban.bundle.js"
}
},
"leaflet": {
"src": {
"styles": [
"{$config.path.node_modules}/leaflet/dist/leaflet.css",
"{$config.path.node_modules}/esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css"
],
"scripts": [
"{$config.path.node_modules}/leaflet/dist/leaflet.js",
"{$config.path.node_modules}/esri-leaflet/dist/esri-leaflet.js",
"{$config.path.node_modules}/esri-leaflet-geocoder/dist/esri-leaflet-geocoder.js"
]
},
"dist": {
"styles": "{$config.dist}/plugins/custom/leaflet/leaflet.bundle.css",
"scripts": "{$config.dist}/plugins/custom/leaflet/leaflet.bundle.js"
}
}
}
},
"custom": {
"src": {
"styles": [
"{$config.path.common_src}/sass/custom/**/*.scss",
"{$config.path.src}/sass/custom/**/*.scss"
],
"scripts": [
"{$config.path.common_src}/js/custom/**/*.js",
"{$config.path.src}/js/custom/**/*.js"
]
},
"dist": {
"styles": "{$config.dist}/css/custom/",
"scripts": "{$config.dist}/js/custom/"
}
},
"media": {
"src": {
"media": [
"{$config.path.src}/media/**/*.*",
"{$config.path.common_src}/media/**/*.*"
]
},
"dist": {
"media": "{$config.dist}/media/"
}
},
"api": {
"src": {
"media": [
"{$config.path.src}/api/**/*.*",
"{$config.path.common_src}/api/**/*.*"
]
},
"dist": {
"media": "{$config.dist}/api/"
}
}
}
}</code>
</pre>
                    </div>
                </div>
                <!--end::Highlight-->
            </div>
            <!--end::Code-->
            <!--begin::Alert-->
            <div class="py-5">
                <!--begin::Information-->
                <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                    <!--begin::Icon-->
                    <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                    <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <!--end::Icon-->
                    <!--begin::Description-->
                    <div class="text-gray-700 fw-bold fs-6">
                    <h4 class="text-dark fs-4 mb-2 fw-bold">Required Core CSS and JS files</h4>The core CSS and JS files defined under
                    <code>build.plugins.base.src.mandatory</code>and
                    <code>build.theme.base</code>are required in order to have the theme's basic functionality up. However the assets defined
                    <code>build.plugins.base.src.optional</code>are optional.</div>
                    <!--end::Description-->
                </div>
                <!--end::Information-->
            </div>
            <!--end::Alert-->
            <!--begin::Table-->
            <div class="py-5">
                <div class="table-responsive border rounded">
                    <table class="table table-striped table-borderless align-middle mb-0">
                        <!--begin::Block-->
                        <thead>
                            <tr>
                                <th class="fs-4 fw-bold text-dark ps-6 py-6" colspan="3">Gulp Build Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="text-gray-600 align-middle">
                                <th class="fw-semibold ps-6 py-6" scope="col">Field</th>
                                <th class="fw-semibold text-gray-600 py-6" scope="col">Type</th>
                                <th class="fw-semibold text-gray-600 ps-6 py-6" scope="col">Description</th>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.debug</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable debug console log.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.compile.rtl.enabled</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable compilation with RTL version of CSS along with default LTR CSS.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.compile.rtl.skip</code>
                                </td>
                                <td>
                                    <code>array</code>
                                </td>
                                <td class="ps-6">An array of plugin to be skipped from being compile as RTL.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.compile.jsMinify</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable output Javascript minify.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.compile.cssMinify</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable output CSS minify.</td>
                            </tr>
                            <tr>
                                <td class="py-5 ps-6">
                                    <code>config.compile.jsSourcemaps</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable output Javascript with sourcemaps.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.compile.cssSourcemaps</code>
                                </td>
                                <td>
                                    <code>boolean</code>
                                </td>
                                <td class="ps-6">Enable/disable output CSS with sourcemaps.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>config.dist</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">
                                <code>dist</code>&#160; stands for
                                <code>distributable</code>&#160; and refers to the directories where the minified and bundled assets will be stored for production uses.</td>
                            </tr>
                        </tbody>
                        <!--end::Block-->
                        <!--begin::Block-->
                        <thead>
                            <tr>
                                <th class="fs-4 fw-semibold text-dark ps-6 py-5" colspan="3">Build Items</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.base</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">This object specifies the global assets of the demo to be added into the global css and js demo bundles.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.plugins.global</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">This object specifies required 3rd party resources to be added into the global css and js plugins bundles.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.plugins.custom</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">This object specifies global 3rd party resources to be added into the custom css and js plugins bundles.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.custom</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">This object specifies custom assets that are included on demand.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.media</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">Media folder.</td>
                            </tr>
                            <tr>
                                <td class="py-6 ps-6">
                                    <code>build.api</code>
                                </td>
                                <td>
                                    <code>object</code>
                                </td>
                                <td class="ps-6">Server side scripts used for examples with remote data source(e.g: ajax datatables, dropdown list, quick search results, etc).</td>
                            </tr>
                        </tbody>
                        <!--end::Block-->
                    </table>
                </div>
            </div>
            <!--end::Table-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="py-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="build-task" data-kt-scroll-offset="50">
            <a href="#build-task"></a>Build Task</h1>
            <!--end::Heading-->
            <!--begin::Notice-->
            <div class="py-5">
                <!--begin::Information-->
                <div class="d-flex align-items-center rounded py-5 px-5 bg-light-warning">
                    <!--begin::Icon-->
                    <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                    <span class="svg-icon svg-icon-3x svg-icon-warning me-5">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="currentColor" />
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                    <!--end::Icon-->
                    <!--begin::Description-->
                    <div class="text-gray-700 fw-bold fs-6">Update the
                    <code class="me-0">Node.js</code>, global
                    <code>npm</code>and
                    <code>yarn</code>to the latest versions for any errors related to
                    <code>node-sass</code>.
                    <br />If the
                    <code>gulp</code>command is not working, try to remove
                    <code>tools/node_modules</code>folder, and reinstall dependencies using
                    <code>yarn</code>.</div>
                    <!--end::Description-->
                </div>
                <!--end::Information-->
            </div>
            <!--end::Notice-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Launch your terminal and change its current directory to the project's tools folder where the build files are located. All commands below must be run in this tools folder.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">cd theme/tools/</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">For the first time, run the command below to install the npm dependencies defined in
                <code>/tools/package.json</code>(if you haven't done it) :</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">yarn</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">After every modification in
                <code>[demo]/src/</code>, run the below task to compile the assets as defined in
                <code>/tools/gulp.config.json</code>:</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Provide parameter
                <code>--rtl</code>to the gulp task. This command will disable RTL CSS from generating. If this param does not exist, by default, RTL CSS will be generated.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --rtl --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Provide parameter
                <code>--js</code>to the gulp task. This command will generate javascript assets only.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --js --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Provide parameter
                <code>--sass</code>to the gulp task. This command will generate SCSS, SASS and CSS assets only.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --sass --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Provide parameter
                <code>--media</code>to the gulp task. This command will generate all media, fonts, images, etc. assets only.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --media --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Provide parameter
                <code>--prod</code>to the gulp task. This command will minify all the JS and CSS.</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp --prod --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="">The customization should be done in
                <code>theme/src/</code>folder. After compilation refer to the destination folder
                <code>theme/dist/</code>. The bellow command is to start a real-time watcher. This task watches the SCSS and Javascript files and automatically recompile whenever the source files are changed. You also can run watcher separately for JS and SCSS.</p>
                <p class="mb-0"></p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp watch --demo1</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
        </div>
        <!--end::Section-->
        <!--begin::Section-->
        <div class="pt-10">
            <!--begin::Heading-->
            <h1 class="anchor fw-bold mb-5" id="localhost" data-kt-scroll-offset="50">
            <a href="#localhost"></a>Localhost</h1>
            <!--end::Heading-->
            <!--begin::Block-->
            <div class="py-5">
                <p class="mb-0">Metronic uses
                <a href="https://www.npmjs.com/package/gulp-connect" target="_blank" class="fw-bold me-1">Gulp-connect</a>plugin use to run a simple webserver for quick browsing the HTML templatest:</p>
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">gulp localhost</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
            <!--begin::Block-->
            <div class="pt-5">
                <p class="mb-0">Keep the console open. Open this link to run
                <a href="http://localhost:8080/'.(Theme::isMultiDemo()?'demo1/':'').'dist/" target="_blank">
                    <code>http://localhost:8080/'.(Theme::isMultiDemo()?'demo1/':'').'dist/</code>
                </a>. The default running port is
                <code>8080</code>and in case, this port being used by other application, it can be changed in
                <code>tools/gulp/watch.js</code></p>
                <!--end::Block-->
                <div class="py-5">
                    <!--begin::Highlight-->
                    <div class="highlight">
                        <button class="highlight-copy btn" data-bs-toggle="tooltip" title="Copy code">copy</button>
                        <div class="highlight-code">
                            <pre class="language-bash">
<code class="language-bash">connect.server({
root: '../dist',
livereload: true,
port: 8081,
});</code>
</pre>
                        </div>
                    </div>
                    <!--end::Highlight-->
                </div>
            </div>
            <!--end::Block-->
        </div>
        <!--end::Section-->
    </div>
    <!--end::Card Body-->
</div>