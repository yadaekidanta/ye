counter_chat(localStorage.getItem("route_counter_chat"));
counter_notif(localStorage.getItem("route_counter_notif"));
load_notif(localStorage.getItem("route_notification"));
$()
$("#notification").click(function(){
    counter_notif(localStorage.getItem("route_counter_notif"));
    load_notif(localStorage.getItem("route_notification"));
});
setInterval(function(){
    counter_chat(localStorage.getItem("route_counter_chat"));
    counter_notif(localStorage.getItem("route_counter_notif"));
}, 5000);
function counter_chat(url){
    // let data = "view="+ view + "&load_keranjang=";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (response){
            if(response.total_notif > 0){
                $('#chat_pulse').addClass('pulse-ring');
                $('#chat_blink').removeClass('d-none');
            }else{
                $('#chat_pulse').removeClass('pulse-ring');
                $('#chat_blink').addClass('d-none');
            }
        }
    });
}
function counter_notif(url){
    // let data = "view="+ view + "&load_keranjang=";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (response){
            if(response.total_notif > 0){
                $('#notification_pulse').addClass('pulse-ring');
                $('#notification_blink').removeClass('d-none');
            }else{
                $('#notification_pulse').removeClass('pulse-ring');
                $('#notification_blink').addClass('d-none');
            }
        }
    });
}

function load_notif(url){
    // let data = "view="+ view + "&load_keranjang=";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (response){
            $('#notification_item').html(response.collection);
            $('#total_notif').html(ribuan(response.total_notif) ?? 0);
        },
    });
}
