<x-web-layout title="{{__('menu.case')}}">
    <section class="wrapper image-wrapper bg-image bg-overlay text-white" data-image-src="{{$project->image}}">
        <div class="container pt-17 pb-12 pt-md-19 pb-md-16 text-center">
            <div class="row">
                <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                    <div class="post-header">
                        <div class="post-category text-line text-white">
                            <a href="#" class="text-reset" rel="category">{{$project->title}}</a>
                        </div>
                        <!-- /.post-category -->
                        {{-- <h1 class="display-1 mb-3 text-white"></h1> --}}
                        {{-- <p class="lead px-md-12 px-lg-12 px-xl-15 px-xxl-18"></p> --}}
                    </div>
                    <!-- /.post-header -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light wrapper-border">
        <div class="container pt-14 pt-md-16 pb-13 pb-md-15">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <article>
                        <h2 class="display-6 mb-4">About the Project</h2>
                        <div class="row gx-0">
                            <div class="col-md-9 text-justify" style="text-align: justify;text-justify: inter-word;">
                                {!!$project->description!!}
                            </div>
                            <!--/column -->
                            <div class="col-md-2 ms-auto">
                                <ul class="list-unstyled">
                                    <li>
                                        <h5 class="mb-1">Date</h5>
                                        <p>{{$project->start_at->isoFormat('dddd, D MMMM Y')}}</p>
                                    </li>
                                    <li>
                                        <h5 class="mb-1">Client Name</h5>
                                        <p>{{$project->client->company_name}}</p>
                                    </li>
                                </ul>
                                <a target="_blank" href="{{$project->website}}" class="more hover">See Project</a>
                            </div>
                            <!--/column -->
                        </div>
                        <!--/.row -->
                    </article>
                    <!-- /.project -->
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        @if($project->gallery->count() > 0)
        <div class="container-fluid px-md-6">
            <div class="swiper-container blog grid-view mb-17 mb-md-19" data-margin="30" data-nav="true" data-dots="true" data-items-xxl="3" data-items-md="2" data-items-xs="1">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        @foreach($project->gallery as $item)
                        <div class="swiper-slide">
                            <figure class="rounded"><img src="{{$item->image}}" alt="" /></figure>
                        </div>
                        <!--/.swiper-slide -->
                        @endforeach
                    </div>
                    <!--/.swiper-wrapper -->
                </div>
                <!-- /.swiper -->
            </div>
            <!-- /.swiper-container -->
        </div>
        @endif
        <!-- /.container-fluid -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container py-10">
            <div class="row gx-md-6 gy-3 gy-md-0">
                <div class="col-md-8 align-self-center text-center text-md-start navigation">
                    @if(\App\Models\CRM\Project::where('id','<',$project->id)->get()->count() > 0)
                    <a href="{{\App\Models\CRM\Project::where('id','<',$project->id)->latest('id')->first()->slug}}" class="btn btn-soft-ash rounded-pill btn-icon btn-icon-start mb-0 me-1"><i class="uil uil-arrow-left"></i> Prev Post</a>
                    @endif
                    @if(\App\Models\CRM\Project::where('id','>',$project->id)->get()->count() > 0)
                    <a href="{{\App\Models\CRM\Project::where('id','>',$project->id)->first()->slug}}" class="btn btn-soft-ash rounded-pill btn-icon btn-icon-end mb-0">Next Post <i class="uil uil-arrow-right"></i></a>
                    @endif
                </div>
                <!--/column -->
                <aside class="col-md-4 sidebar text-center text-md-end">
                    <div class="dropdown share-dropdown btn-group">
                        <button class="btn btn-red rounded-pill btn-icon btn-icon-start dropdown-toggle mb-0 me-0" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="uil uil-share-alt"></i> Share </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#"><i class="uil uil-twitter"></i>Twitter</a>
                            <a class="dropdown-item" href="#"><i class="uil uil-facebook-f"></i>Facebook</a>
                            <a class="dropdown-item" href="#"><i class="uil uil-linkedin"></i>Linkedin</a>
                        </div>
                        <!--/.dropdown-menu -->
                    </div>
                    <!--/.share-dropdown -->
                </aside>
                <!-- /column .sidebar -->
            </div>
            <!--/.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
</x-web-layout>