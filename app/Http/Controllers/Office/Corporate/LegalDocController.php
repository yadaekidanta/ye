<?php

namespace App\Http\Controllers\Office\Corporate;

use App\Http\Controllers\Controller;
use App\Models\Corporate\LegalDoc;
use Illuminate\Http\Request;

class LegalDocController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(LegalDoc $legalDoc)
    {
        //
    }
    public function edit(LegalDoc $legalDoc)
    {
        //
    }
    public function update(Request $request, LegalDoc $legalDoc)
    {
        //
    }
    public function destroy(LegalDoc $legalDoc)
    {
        //
    }
    public function list()
    {
        // 
    }
}
