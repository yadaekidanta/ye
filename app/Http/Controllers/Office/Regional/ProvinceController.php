<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\Country;
use App\Models\Regional\Regency;
use App\Models\Regional\Province;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.regional.province.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        $country = Country::get();
        return view('pages.office.regional.province.input', ['data' => new Province, 'country' => $country]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, Province $province)
    {
        if($request->ajax())
        {
            return view('pages.office.regional.province.show',compact('province'));
        }
        return view('pages.office.theme');
    }
    public function edit(Province $province)
    {
        $country = Country::get();
        return view('pages.office.regional.province.input', ['data' => $province, 'country' => $country]);
    }
    public function update(Request $request, Province $province)
    {
        //
    }
    public function destroy(Province $province)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = Province::where('id','LIKE','%'.$request->keyword.'%')->orWhere('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.regional.province.list',compact('collection'));
    }
    public function show_list(Request $request, Province $province)
    {
        $collection = Regency::where('province_id',$province->id)->where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.regional.province.show_list',compact('collection'));
    }
    public function show_create(Province $province)
    {
        return view('pages.office.regional.province.show_input', ['data' => new Regency, 'province' => $province]);
    }
    public function show_edit(Province $province, Regency $regency)
    {
        return view('pages.office.regional.province.show_input', ['data' => $regency, 'province' => $province]);
    }
}
