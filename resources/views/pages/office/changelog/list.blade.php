<div class="card card-docs flex-row-fluid mb-2">
    <div class="card-body fs-6 py-15 px-10 py-lg-15 px-lg-15 text-gray-700">
        <div class="accordion accordion-flush accordion-icon-toggle" id="kt_accordion">
            @foreach($collection as $key => $item)
            <div class="accordion-item mb-5">
                <div class="accordion-header py-3 d-flex" data-bs-toggle="collapse" data-bs-target="#kt_accordion_body_v8_1_0">
                    <span class="accordion-icon">
                        <span class="svg-icon svg-icon-3">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="currentColor" />
                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="currentColor" />
                            </svg>
                        </span>
                    </span>
                    <h3 class="fs-2 text-gray-800 fw-bold mb-0 ms-4">{{$item->name}} - {{$item->created_at->format('j F Y')}}</h3>
                </div>
                <div id="kt_accordion_body_v8_1_0" class="fs-6 mt-1 mb-1 py-0 ps-10 collapse show" data-bs-parent="#kt_accordion">
                    <div class="accordion-body ps-0 pt-0">
                        @if($item->detail_news->count() > 0)
                        <div class="mb-5">
                            <h3 class="fs-6 fw-bold mb-1">New:</h3>
                            <ul class="my-0 py-0">
                                @foreach ($item->detail_news as $detail)
                                <li class="py-2">
                                    @if($item->title)
                                    <code class="ms-0">{{$detail->title}}</code>
                                    @endif
                                    {{$detail->description}}
                                    @if($item->url)
                                    - <a href="{{$item->url}}" class="fw-bold" target="_blank">Preview</a>
                                    @endif
                                    .
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if($item->detail_update->count() > 0)
                        <div class="mb-5">
                            <h3 class="fs-6 fw-bold mb-1">Update:</h3>
                            <ul class="my-0 py-0">
                                @foreach ($item->detail_update as $detail)
                                <li class="py-2">
                                    @if($item->title)
                                    <code class="ms-0">{{$detail->title}}</code>
                                    @endif
                                    {{$detail->description}}
                                    @if($item->url)
                                    - <a href="{{$item->url}}" class="fw-bold" target="_blank">Preview</a>
                                    @endif
                                    .
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if($item->detail_fix->count() > 0)
                        <div class="mb-5">
                            <h3 class="fs-6 fw-bold mb-1">Fix:</h3>
                            <ul class="my-0 py-0">
                                @foreach ($item->detail_fix as $detail)
                                <li class="py-2">
                                    @if($item->title)
                                    <code class="ms-0">{{$detail->title}}</code>
                                    @endif
                                    {{$detail->description}}
                                    @if($item->url)
                                    - <a href="{{$item->url}}" class="fw-bold" target="_blank">Preview</a>
                                    @endif
                                    .
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!--end::Changelog-->
    </div>
    <!--end::Card Body-->
</div>