<?php

namespace Database\Seeders;

use App\Models\HRM\Module;
use App\Models\HRM\Employee;
use App\Models\HRM\Position;
use App\Models\HRM\Department;
use App\Models\HRM\Permission;
use Illuminate\Database\Seeder;
use App\Models\HRM\ModulePrivilege;
use Illuminate\Support\Facades\Hash;

class HrmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = array(
            [
                'name' => 'Commissioner'
            ],
            [
                'name' => 'Executive'
            ],
            [
                'name' => 'Corporate Secretary'
            ],
            [
                'name' => 'General'
            ],
            [
                'name' => 'Human Resource'
            ],
            [
                'name' => 'Commercial'
            ],
            [
                'name' => 'Finance'
            ],
            [
                'name' => 'Marketing'
            ],
            [
                'name' => 'Technology'
            ],
            [
                'name' => 'Development'
            ],
        );
        $position = array(
            [
                "permission_id" => "2",
                "department_id" => "1",
                "name" => "Commissioners",
            ],
            [
                "permission_id" => "3",
                "department_id" => "1",
                "name" => "Secretary Commissioner",
            ],
            [
                "permission_id" => "4",
                "department_id" => "1",
                "name" => "Audit Committee",
            ],
            [
                "permission_id" => "5",
                "department_id" => "2",
                "name" => "CEO",
            ],
            [
                "permission_id" => "6",
                "department_id" => "2",
                "name" => "COO",
            ],
            [
                "permission_id" => "7",
                "department_id" => "2",
                "name" => "CFO",
            ],
            [
                "permission_id" => "8",
                "department_id" => "2",
                "name" => "CIO",
            ],
            [
                "permission_id" => "9",
                "department_id" => "2",
                "name" => "CTO",
            ],
            [
                "permission_id" => "10",
                "department_id" => "2",
                "name" => "CDO",
            ],
            [
                "permission_id" => "11",
                "department_id" => "2",
                "name" => "CMO",
            ],
            [
                "permission_id" => "12",
                "department_id" => "2",
                "name" => "CHRO",
            ],
            [
                "permission_id" => "13",
                "department_id" => "3",
                "name" => "Head of Corporate Secretary",
            ],
            [
                "permission_id" => "14",
                "department_id" => "3",
                "name" => "Secretary",
            ],
            [
                "permission_id" => "15",
                "department_id" => "4",
                "name" => "Head of Legal",
            ],
            [
                "permission_id" => "16",
                "department_id" => "5",
                "name" => "Head of General Affairs",
            ],
            [
                "permission_id" => "17",
                "department_id" => "5",
                "name" => "Head of Public Relation",
            ],
            [
                "permission_id" => "18",
                "department_id" => "5",
                "name" => "Head of Human Resource",
            ],
            [
                "permission_id" => "19",
                "department_id" => "5",
                "name" => "General Affair",
            ],
            [
                "permission_id" => "20",
                "department_id" => "5",
                "name" => "Human Resource Recruitment",
            ],
            [
                "permission_id" => "21",
                "department_id" => "5",
                "name" => "HRBP",
            ],
            [
                "permission_id" => "22",
                "department_id" => "5",
                "name" => "Organization Development",
            ],
            [
                "permission_id" => "23",
                "department_id" => "5",
                "name" => "Payroll",
            ],
            [
                "permission_id" => "24",
                "department_id" => "5",
                "name" => "Training and Development",
            ],
            [
                "permission_id" => "25",
                "department_id" => "6",
                "name" => "Head of Commercial",
            ],
            [
                "permission_id" => "26",
                "department_id" => "6",
                "name" => "Head of Procurement Sub Division",
            ],
            [
                "permission_id" => "27",
                "department_id" => "6",
                "name" => "Account Manager",
            ],
            [
                "permission_id" => "28",
                "department_id" => "7",
                "name" => "Head of Finance",
            ],
            [
                "permission_id" => "29",
                "department_id" => "7",
                "name" => "Accountant",
            ],
            [
                "permission_id" => "30",
                "department_id" => "7",
                "name" => "Auditor",
            ],
            [
                "permission_id" => "31",
                "department_id" => "7",
                "name" => "Budget analyst",
            ],
            [
                "permission_id" => "32",
                "department_id" => "7",
                "name" => "Finance controller",
            ],
            [
                "permission_id" => "33",
                "department_id" => "7",
                "name" => "Finance administrator",
            ],
            [
                "permission_id" => "34",
                "department_id" => "7",
                "name" => "Payroll officer",
            ],
            [
                "permission_id" => "35",
                "department_id" => "7",
                "name" => "Treasury analyst",
            ],
            [
                "permission_id" => "36",
                "department_id" => "8",
                "name" => "Director of Media",
            ],
            [
                "permission_id" => "37",
                "department_id" => "8",
                "name" => "Director of Public Relation",
            ],
            [
                "permission_id" => "38",
                "department_id" => "8",
                "name" => "Advertising Manager",
            ],
            [
                "permission_id" => "39",
                "department_id" => "8",
                "name" => "Brand Manager",
            ],
            [
                "permission_id" => "40",
                "department_id" => "8",
                "name" => "Community Manager",
            ],
            [
                "permission_id" => "41",
                "department_id" => "8",
                "name" => "Content Manager",
            ],
            [
                "permission_id" => "42",
                "department_id" => "8",
                "name" => "Product Marketing Manager",
            ],
            [
                "permission_id" => "43",
                "department_id" => "8",
                "name" => "Promotions Manager",
            ],
            [
                "permission_id" => "44",
                "department_id" => "8",
                "name" => "Public Relations Manager",
            ],
            [
                "permission_id" => "45",
                "department_id" => "8",
                "name" => "Sales Manager",
            ],
            [
                "permission_id" => "46",
                "department_id" => "8",
                "name" => "Social Media Manager",
            ],
            [
                "permission_id" => "47",
                "department_id" => "8",
                "name" => "Account Coordinator",
            ],
            [
                "permission_id" => "48",
                "department_id" => "8",
                "name" => "Event Marketing Coordinator",
            ],
            [
                "permission_id" => "49",
                "department_id" => "8",
                "name" => "Event Marketing Specialist",
            ],
            [
                "permission_id" => "50",
                "department_id" => "8",
                "name" => "Marketing Coordinator",
            ],
            [
                "permission_id" => "51",
                "department_id" => "8",
                "name" => "Marketing Specialist",
            ],
            [
                "permission_id" => "52",
                "department_id" => "8",
                "name" => "Project Coordinator",
            ],
            [
                "permission_id" => "53",
                "department_id" => "8",
                "name" => "Public Relation Associate",
            ],
            [
                "permission_id" => "54",
                "department_id" => "8",
                "name" => "Social Media Coordinator",
            ],
            [
                "permission_id" => "55",
                "department_id" => "9",
                "name" => "IT Manager",
            ],
            [
                "permission_id" => "56",
                "department_id" => "9",
                "name" => "Product Manager",
            ],
            [
                "permission_id" => "57",
                "department_id" => "9",
                "name" => "Project Manager",
            ],
            [
                "permission_id" => "58",
                "department_id" => "9",
                "name" => "Analyst",
            ],
            [
                "permission_id" => "59",
                "department_id" => "9",
                "name" => "Helpdesk Analyst",
            ],
            [
                "permission_id" => "60",
                "department_id" => "9",
                "name" => "Database Administrator",
            ],
            [
                "permission_id" => "61",
                "department_id" => "9",
                "name" => "Network Administrator",
            ],
            [
                "permission_id" => "62",
                "department_id" => "9",
                "name" => "Backend Developer",
            ],
            [
                "permission_id" => "63",
                "department_id" => "9",
                "name" => "Frontend Developer",
            ],
            [
                "permission_id" => "64",
                "department_id" => "9",
                "name" => "Fullstack Developer",
            ],
            [
                "permission_id" => "65",
                "department_id" => "9",
                "name" => "Mobile App Developer",
            ],
            [
                "permission_id" => "66",
                "department_id" => "9",
                "name" => "Copy Writer",
            ],
            [
                "permission_id" => "67",
                "department_id" => "9",
                "name" => "Content Writer",
            ],
            [
                "permission_id" => "68",
                "department_id" => "9",
                "name" => "IT Support",
            ],
            [
                "permission_id" => "69",
                "department_id" => "9",
                "name" => "UI/UX Design",
            ],
            [
                "permission_id" => "70",
                "department_id" => "10",
                "name" => "Business Development Manager",
            ]
        );
        $permission = array(
            [
                "name" => "Super Admin",
            ],
            [
                "name" => "Commissioners",
            ],
            [
                "name" => "Secretary Commissioner",
            ],
            [
                "name" => "Audit Committee",
            ],
            [
                "name" => "CEO",
            ],
            [
                "name" => "COO",
            ],
            [
                "name" => "CFO",
            ],
            [
                "name" => "CIO",
            ],
            [
                "name" => "CTO",
            ],
            [
                "name" => "CDO",
            ],
            [
                "name" => "CMO",
            ],
            [
                "name" => "CHRO",
            ],
            [
                "name" => "Head of Corporate Secretary",
            ],
            [
                "name" => "Secretary",
            ],
            [
                "name" => "Head of Legal",
            ],
            [
                "name" => "Head of General Affairs",
            ],
            [
                "name" => "Head of Public Relation",
            ],
            [
                "name" => "Head of Human Resource",
            ],
            [
                "name" => "General Affair",
            ],
            [
                "name" => "Human Resource Recruitment",
            ],
            [
                "name" => "HRBP",
            ],
            [
                "name" => "Organization Development",
            ],
            [
                "name" => "Payroll",
            ],
            [
                "name" => "Training and Development",
            ],
            [
                "name" => "Head of Commercial",
            ],
            [
                "name" => "Head of Procurement Sub Division",
            ],
            [
                "name" => "Account Manager",
            ],
            [
                "name" => "Head of Finance",
            ],
            [
                "name" => "Accountant",
            ],
            [
                "name" => "Auditor",
            ],
            [
                "name" => "Budget analyst",
            ],
            [
                "name" => "Finance controller",
            ],
            [
                "name" => "Finance administrator",
            ],
            [
                "name" => "Payroll officer",
            ],
            [
                "name" => "Treasury analyst",
            ],
            [
                "name" => "Director of Media",
            ],
            [
                "name" => "Director of Public Relation",
            ],
            [
                "name" => "Advertising Manager",
            ],
            [
                "name" => "Brand Manager",
            ],
            [
                "name" => "Community Manager",
            ],
            [
                "name" => "Content Manager",
            ],
            [
                "name" => "Product Marketing Manager",
            ],
            [
                "name" => "Promotions Manager",
            ],
            [
                "name" => "Public Relations Manager",
            ],
            [
                "name" => "Sales Manager",
            ],
            [
                "name" => "Social Media Manager",
            ],
            [
                "name" => "Account Coordinator",
            ],
            [
                "name" => "Event Marketing Coordinator",
            ],
            [
                "name" => "Event Marketing Specialist",
            ],
            [
                "name" => "Marketing Coordinator",
            ],
            [
                "name" => "Marketing Specialist",
            ],
            [
                "name" => "Project Coordinator",
            ],
            [
                "name" => "Public Relation Associate",
            ],
            [
                "name" => "Social Media Coordinator",
            ],
            [
                "name" => "IT Manager",
            ],
            [
                "name" => "Product Manager",
            ],
            [
                "name" => "Project Manager",
            ],
            [
                "name" => "Analyst",
            ],
            [
                "name" => "Helpdesk Analyst",
            ],
            [
                "name" => "Database Administrator",
            ],
            [
                "name" => "Network Administrator",
            ],
            [
                "name" => "Backend Developer",
            ],
            [
                "name" => "Frontend Developer",
            ],
            [
                "name" => "Fullstack Developer",
            ],
            [
                "name" => "Mobile App Developer",
            ],
            [
                "name" => "Copy Writer",
            ],
            [
                "name" => "Content Writer",
            ],
            [
                "name" => "IT Support",
            ],
            [
                "name" => "UI/UX Design",
            ],
            [
                "name" => "Business Development Manager",
            ]
        );
        $employee = array(
            [
                'nip' => '190521111',
                'name' => 'Zulhelmi Nasution',
                'email' => 'zulhelmi@yadaekidanta.com',
                'phone' => '085694798901',
                'jobdesc' => 'as Commissioner, Zulhelmi supervise the Board of Directors in carrying out company activities and provide advice to the Board of Directors. Supervise the implementation of the Company Plan',
                'date_birth' => '1966/07/21',
                'bio' => '',
                'impression' => '',
                'address' => 'Komplek Perhubungan Udara, Jl. Warung Jati',
                'postcode' => '40287',
                'department_id' => '1',
                'position_id' => '1',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '190521242',
                'name' => 'Rizky Ramadhan',
                'email' => 'misterrizky@yadaekidanta.com',
                'phone' => '087709020299',
                'jobdesc' => 'as CEO, Rizky manage and provide leadership and insight to the Board of Directors within the Yada Ekidanta',
                'date_birth' => '1996/02/15',
                'bio' => '',
                'impression' => '',
                'address' => 'Permata Buah Batu Residence Block C No. 15B',
                'postcode' => '40287',
                'department_id' => '2',
                'position_id' => '4',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '190521263',
                'name' => 'Ahmad Ridwan Ananta',
                'email' => 'anantalubis@yadaekidanta.com',
                'phone' => '081314616393',
                'jobdesc' => 'as CFO, Ananta role are responsible for all day-to-day management decisions and for implementing the YE long and short term plans',
                'date_birth' => '1999/12/03',
                'bio' => '',
                'impression' => '',
                'address' => 'Kp Baru II No. 66',
                'postcode' => '0',
                'department_id' => '2',
                'position_id' => '6',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '0103223134',
                'name' => 'Maesyarah Messa',
                'email' => 'mysrhmessa@yadaekidanta.com',
                'phone' => '082340975944',
                'jobdesc' => 'as Operational, Mesya Role is responsible',
                'date_birth' => '1998/09/30',
                'bio' => '',
                'impression' => '',
                'address' => 'Jl. Hasan Saleh Neusu Jaya No 9, Banda Aceh',
                'postcode' => '23243',
                'department_id' => '3',
                'position_id' => '13',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '0107225175',
                'name' => 'Galih Eka Saputri',
                'email' => 'galihekasaputri@yadaekidanta.com',
                'phone' => '083875699019',
                'jobdesc' => 'as HR, Galih Role is responsible to ...',
                'date_birth' => '2003/05/03',
                'bio' => '',
                'impression' => 'Always fun and make it better',
                'address' => 'Jln peninggaran timur 2 rt 04 rw 09 no 5',
                'postcode' => '12240',
                'department_id' => '5',
                'position_id' => '17',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '1205225156',
                'name' => 'Afif Derian Kusuma',
                'email' => 'afder@yadaekidanta.com',
                'phone' => '081280702959',
                'jobdesc' => 'as Head of General Affair, Afif Role is',
                'date_birth' => '2000/11/12',
                'bio' => '',
                'impression' => '',
                'address' => 'Aceh',
                'postcode' => '0',
                'department_id' => '4',
                'position_id' => '14',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '0106219567',
                'name' => 'Pakhomios Havel Situmorang',
                'email' => 'hvlicious@yadaekidanta.com',
                'phone' => '089651147065',
                'jobdesc' => 'as ',
                'date_birth' => '2000/11/12',
                'bio' => '',
                'impression' => '',
                'address' => '',
                'postcode' => '0',
                'department_id' => '9',
                'position_id' => '63',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '0106219638',
                'name' => 'Joshua',
                'email' => 'joshua@yadaekidanta.com',
                'phone' => '081361432123',
                'jobdesc' => 'as ',
                'date_birth' => '2000/11/12',
                'bio' => '',
                'impression' => '',
                'address' => '',
                'postcode' => '0',
                'department_id' => '9',
                'position_id' => '63',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '0106219639',
                'name' => 'Ricky',
                'email' => 'ricky@yadaekidanta.com',
                'phone' => '081362926803',
                'jobdesc' => 'as ',
                'date_birth' => '2000/11/12',
                'bio' => '',
                'impression' => '',
                'address' => '',
                'postcode' => '0',
                'department_id' => '9',
                'position_id' => '63',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
            [
                'nip' => '01072296210',
                'name' => 'Osi',
                'email' => 'osi@yadaekidanta.com',
                'phone' => '081917320977',
                'jobdesc' => 'as ',
                'date_birth' => '2000/11/12',
                'bio' => '',
                'impression' => '',
                'address' => '',
                'postcode' => '0',
                'department_id' => '9',
                'position_id' => '62',
                'st' => 'a',
                'password' => Hash::make('password'),
            ],
        );
        $modules = array(
            [
                "module_id" => 0,
                "name" => "Dashboards",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" /><rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" /><rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" /><rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "Default Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "eCommerce Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "Projects Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "Marketing Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "Web Analytics Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 1,
                "name" => "Finance Performance Dashboard",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Calendar",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="currentColor" /><path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="currentColor" /><path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "File Manager",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M17.5 11H6.5C4 11 2 9 2 6.5C2 4 4 2 6.5 2H17.5C20 2 22 4 22 6.5C22 9 20 11 17.5 11ZM15 6.5C15 7.9 16.1 9 17.5 9C18.9 9 20 7.9 20 6.5C20 5.1 18.9 4 17.5 4C16.1 4 15 5.1 15 6.5Z" fill="currentColor" /><path opacity="0.3" d="M17.5 22H6.5C4 22 2 20 2 17.5C2 15 4 13 6.5 13H17.5C20 13 22 15 22 17.5C22 20 20 22 17.5 22ZM4 17.5C4 18.9 5.1 20 6.5 20C7.9 20 9 18.9 9 17.5C9 16.1 7.9 15 6.5 15C5.1 15 4 16.1 4 17.5Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Master",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.3" d="M10 4H21C21.6 4 22 4.4 22 5V7H10V4Z" fill="currentColor"/><path d="M9.2 3H3C2.4 3 2 3.4 2 4V19C2 19.6 2.4 20 3 20H21C21.6 20 22 19.6 22 19V7C22 6.4 21.6 6 21 6H12L10.4 3.60001C10.2 3.20001 9.7 3 9.2 3Z" fill="currentColor"/></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 10,
                "name" => "Countries",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 10,
                "name" => "Provinces",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 10,
                "name" => "Regencies",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 10,
                "name" => "Districts",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 10,
                "name" => "Villages",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 10,
                "name" => "Banks",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 10,
                "name" => "ISIC Type",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 10,
                "name" => "ISIC",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "HRM",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.5 11C8.98528 11 11 8.98528 11 6.5C11 4.01472 8.98528 2 6.5 2C4.01472 2 2 4.01472 2 6.5C2 8.98528 4.01472 11 6.5 11Z" fill="currentColor" /><path opacity="0.3" d="M13 6.5C13 4 15 2 17.5 2C20 2 22 4 22 6.5C22 9 20 11 17.5 11C15 11 13 9 13 6.5ZM6.5 22C9 22 11 20 11 17.5C11 15 9 13 6.5 13C4 13 2 15 2 17.5C2 20 4 22 6.5 22ZM17.5 22C20 22 22 20 22 17.5C22 15 20 13 17.5 13C15 13 13 15 13 17.5C13 20 15 22 17.5 22Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 19,
                "name" => "Employee",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 19,
                "name" => "Departments",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 19,
                "name" => "Positions",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 19,
                "name" => "Modules",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 19,
                "name" => "Permissions",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "CRM",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20 14H18V10H20C20.6 10 21 10.4 21 11V13C21 13.6 20.6 14 20 14ZM21 19V17C21 16.4 20.6 16 20 16H18V20H20C20.6 20 21 19.6 21 19ZM21 7V5C21 4.4 20.6 4 20 4H18V8H20C20.6 8 21 7.6 21 7Z" fill="currentColor" /><path opacity="0.3" d="M17 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H17C17.6 2 18 2.4 18 3V21C18 21.6 17.6 22 17 22ZM10 7C8.9 7 8 7.9 8 9C8 10.1 8.9 11 10 11C11.1 11 12 10.1 12 9C12 7.9 11.1 7 10 7ZM13.3 16C14 16 14.5 15.3 14.3 14.7C13.7 13.2 12 12 10.1 12C8.10001 12 6.49999 13.1 5.89999 14.7C5.59999 15.3 6.19999 16 7.39999 16H13.3Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 25,
                "name" => "Leads",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 25,
                "name" => "Clients",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 25,
                "name" => "Customers",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Corporate",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22Z" fill="currentColor" /><path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 29,
                "name" => "History",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 29,
                "name" => "Legal Documents",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 29,
                "name" => "Legal Policy",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 29,
                "name" => "KPI",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 29,
                "name" => "KPI Objectives",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 29,
                "name" => "KPI Objective Key Results",
                "icon" => null,
                "url" => null,
                "is_show" => false,
            ],
            [
                "module_id" => 0,
                "name" => "Finance",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.0021 10.9128V3.01281C13.0021 2.41281 13.5021 1.91281 14.1021 2.01281C16.1021 2.21281 17.9021 3.11284 19.3021 4.61284C20.7021 6.01284 21.6021 7.91285 21.9021 9.81285C22.0021 10.4129 21.5021 10.9128 20.9021 10.9128H13.0021Z" fill="currentColor" /><path opacity="0.3" d="M11.0021 13.7128V4.91283C11.0021 4.31283 10.5021 3.81283 9.90208 3.91283C5.40208 4.51283 1.90209 8.41284 2.00209 13.1128C2.10209 18.0128 6.40208 22.0128 11.3021 21.9128C13.1021 21.8128 14.7021 21.3128 16.0021 20.4128C16.5021 20.1128 16.6021 19.3128 16.1021 18.9128L11.0021 13.7128Z" fill="currentColor" /><path opacity="0.3" d="M21.9021 14.0128C21.7021 15.6128 21.1021 17.1128 20.1021 18.4128C19.7021 18.9128 19.0021 18.9128 18.6021 18.5128L13.0021 12.9128H20.9021C21.5021 12.9128 22.0021 13.4128 21.9021 14.0128Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 36,
                "name" => "COA Type",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 36,
                "name" => "COA",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Careers",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path opacity="0.3" d="M14 3V20H2V3C2 2.4 2.4 2 3 2H13C13.6 2 14 2.4 14 3ZM11 13V11C11 9.7 10.2 8.59995 9 8.19995V7C9 6.4 8.6 6 8 6C7.4 6 7 6.4 7 7V8.19995C5.8 8.59995 5 9.7 5 11V13C5 13.6 4.6 14 4 14V15C4 15.6 4.4 16 5 16H11C11.6 16 12 15.6 12 15V14C11.4 14 11 13.6 11 13Z" fill="currentColor" /><path d="M2 20H14V21C14 21.6 13.6 22 13 22H3C2.4 22 2 21.6 2 21V20ZM9 3V2H7V3C7 3.6 7.4 4 8 4C8.6 4 9 3.6 9 3ZM6.5 16C6.5 16.8 7.2 17.5 8 17.5C8.8 17.5 9.5 16.8 9.5 16H6.5ZM21.7 12C21.7 11.4 21.3 11 20.7 11H17.6C17 11 16.6 11.4 16.6 12C16.6 12.6 17 13 17.6 13H20.7C21.2 13 21.7 12.6 21.7 12ZM17 8C16.6 8 16.2 7.80002 16.1 7.40002C15.9 6.90002 16.1 6.29998 16.6 6.09998L19.1 5C19.6 4.8 20.2 5 20.4 5.5C20.6 6 20.4 6.60005 19.9 6.80005L17.4 7.90002C17.3 8.00002 17.1 8 17 8ZM19.5 19.1C19.4 19.1 19.2 19.1 19.1 19L16.6 17.9C16.1 17.7 15.9 17.1 16.1 16.6C16.3 16.1 16.9 15.9 17.4 16.1L19.9 17.2C20.4 17.4 20.6 18 20.4 18.5C20.2 18.9 19.9 19.1 19.5 19.1Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Projects",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18 21.6C16.6 20.4 9.1 20.3 6.3 21.2C5.7 21.4 5.1 21.2 4.7 20.8L2 18C4.2 15.8 10.8 15.1 15.8 15.8C16.2 18.3 17 20.5 18 21.6ZM18.8 2.8C18.4 2.4 17.8 2.20001 17.2 2.40001C14.4 3.30001 6.9 3.2 5.5 2C6.8 3.3 7.4 5.5 7.7 7.7C9 7.9 10.3 8 11.7 8C15.8 8 19.8 7.2 21.5 5.5L18.8 2.8Z" fill="currentColor" /><path opacity="0.3" d="M21.2 17.3C21.4 17.9 21.2 18.5 20.8 18.9L18 21.6C15.8 19.4 15.1 12.8 15.8 7.8C18.3 7.4 20.4 6.70001 21.5 5.60001C20.4 7.00001 20.2 14.5 21.2 17.3ZM8 11.7C8 9 7.7 4.2 5.5 2L2.8 4.8C2.4 5.2 2.2 5.80001 2.4 6.40001C2.7 7.40001 3.00001 9.2 3.10001 11.7C3.10001 15.5 2.40001 17.6 2.10001 18C3.20001 16.9 5.3 16.2 7.8 15.8C8 14.2 8 12.7 8 11.7Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "eCommerce",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 10H13V11C13 11.6 12.6 12 12 12C11.4 12 11 11.6 11 11V10H3C2.4 10 2 10.4 2 11V13H22V11C22 10.4 21.6 10 21 10Z" fill="currentColor" /><path opacity="0.3" d="M12 12C11.4 12 11 11.6 11 11V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V11C13 11.6 12.6 12 12 12Z" fill="currentColor" /><path opacity="0.3" d="M18.1 21H5.9C5.4 21 4.9 20.6 4.8 20.1L3 13H21L19.2 20.1C19.1 20.6 18.6 21 18.1 21ZM13 18V15C13 14.4 12.6 14 12 14C11.4 14 11 14.4 11 15V18C11 18.6 11.4 19 12 19C12.6 19 13 18.6 13 18ZM17 18V15C17 14.4 16.6 14 16 14C15.4 14 15 14.4 15 15V18C15 18.6 15.4 19 16 19C16.6 19 17 18.6 17 18ZM9 18V15C9 14.4 8.6 14 8 14C7.4 14 7 14.4 7 15V18C7 18.6 7.4 19 8 19C8.6 19 9 18.6 9 18Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 41,
                "name" => "Catalog",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 42,
                "name" => "Products",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 42,
                "name" => "Categories",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 41,
                "name" => "Sales",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Support Center",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13 5.91517C15.8 6.41517 18 8.81519 18 11.8152C18 12.5152 17.9 13.2152 17.6 13.9152L20.1 15.3152C20.6 15.6152 21.4 15.4152 21.6 14.8152C21.9 13.9152 22.1 12.9152 22.1 11.8152C22.1 7.01519 18.8 3.11521 14.3 2.01521C13.7 1.91521 13.1 2.31521 13.1 3.01521V5.91517H13Z" fill="currentColor" /><path opacity="0.3" d="M19.1 17.0152C19.7 17.3152 19.8 18.1152 19.3 18.5152C17.5 20.5152 14.9 21.7152 12 21.7152C9.1 21.7152 6.50001 20.5152 4.70001 18.5152C4.30001 18.0152 4.39999 17.3152 4.89999 17.0152L7.39999 15.6152C8.49999 16.9152 10.2 17.8152 12 17.8152C13.8 17.8152 15.5 17.0152 16.6 15.6152L19.1 17.0152ZM6.39999 13.9151C6.19999 13.2151 6 12.5152 6 11.8152C6 8.81517 8.2 6.41515 11 5.91515V3.01519C11 2.41519 10.4 1.91519 9.79999 2.01519C5.29999 3.01519 2 7.01517 2 11.8152C2 12.8152 2.2 13.8152 2.5 14.8152C2.7 15.4152 3.4 15.7152 4 15.3152L6.39999 13.9151Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 46,
                "name" => "Tickets",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 46,
                "name" => "FAQ",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 46,
                "name" => "Licenses",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 46,
                "name" => "Inbox",
                "icon" => null,
                "url" => null,
                "is_show" => true,
            ],
            [
                "module_id" => 0,
                "name" => "Subscriber",
                "icon" => '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.7 4.19995L4 6.30005V18.8999L8.7 16.8V19L3.1 21.5C2.6 21.7 2 21.4 2 20.8V6C2 5.4 2.3 4.89995 2.9 4.69995L8.7 2.09998V4.19995Z" fill="currentColor" /><path d="M15.3 19.8L20 17.6999V5.09992L15.3 7.19989V4.99994L20.9 2.49994C21.4 2.29994 22 2.59989 22 3.19989V17.9999C22 18.5999 21.7 19.1 21.1 19.3L15.3 21.8998V19.8Z" fill="currentColor" /><path opacity="0.3" d="M15.3 7.19995L20 5.09998V17.7L15.3 19.8V7.19995Z" fill="currentColor" /><path opacity="0.3" d="M8.70001 4.19995V2L15.4 5V7.19995L8.70001 4.19995ZM8.70001 16.8V19L15.4 22V19.8L8.70001 16.8Z" fill="currentColor" /><path opacity="0.3" d="M8.7 16.8L4 18.8999V6.30005L8.7 4.19995V16.8Z" fill="currentColor" /></svg>',
                "url" => null,
                "is_show" => true,
            ],
        );
        $privilege = array(
            [
                "module_id" => 2,
                "name" => "View",
            ],
            [
                "module_id" => 3,
                "name" => "View",
            ],
            [
                "module_id" => 4,
                "name" => "View",
            ],
            [
                "module_id" => 5,
                "name" => "View",
            ],
            [
                "module_id" => 6,
                "name" => "View",
            ],
            [
                "module_id" => 7,
                "name" => "View",
            ],
            [
                "module_id" => 8,
                "name" => "Create",
            ],
            [
                "module_id" => 8,
                "name" => "Read",
            ],
            [
                "module_id" => 8,
                "name" => "Update",
            ],
            [
                "module_id" => 8,
                "name" => "Delete",
            ],
            [
                "module_id" => 9,
                "name" => "Create",
            ],
            [
                "module_id" => 9,
                "name" => "Read",
            ],
            [
                "module_id" => 9,
                "name" => "Update",
            ],
            [
                "module_id" => 9,
                "name" => "Delete",
            ],
            [
                "module_id" => 11,
                "name" => "Create",
            ],
            [
                "module_id" => 11,
                "name" => "Read",
            ],
            [
                "module_id" => 11,
                "name" => "Update",
            ],
            [
                "module_id" => 11,
                "name" => "Delete",
            ],
            [
                "module_id" => 11,
                "name" => "Show",
            ],
            [
                "module_id" => 12,
                "name" => "Create",
            ],
            [
                "module_id" => 12,
                "name" => "Read",
            ],
            [
                "module_id" => 12,
                "name" => "Update",
            ],
            [
                "module_id" => 12,
                "name" => "Delete",
            ],
            [
                "module_id" => 12,
                "name" => "Show",
            ],
            [
                "module_id" => 13,
                "name" => "Create",
            ],
            [
                "module_id" => 13,
                "name" => "Read",
            ],
            [
                "module_id" => 13,
                "name" => "Update",
            ],
            [
                "module_id" => 13,
                "name" => "Delete",
            ],
            [
                "module_id" => 13,
                "name" => "Show",
            ],
            [
                "module_id" => 14,
                "name" => "Create",
            ],
            [
                "module_id" => 14,
                "name" => "Read",
            ],
            [
                "module_id" => 14,
                "name" => "Update",
            ],
            [
                "module_id" => 14,
                "name" => "Delete",
            ],
            [
                "module_id" => 14,
                "name" => "Show",
            ],
            [
                "module_id" => 15,
                "name" => "Create",
            ],
            [
                "module_id" => 15,
                "name" => "Read",
            ],
            [
                "module_id" => 15,
                "name" => "Update",
            ],
            [
                "module_id" => 15,
                "name" => "Delete",
            ],
            [
                "module_id" => 16,
                "name" => "Create",
            ],
            [
                "module_id" => 16,
                "name" => "Read",
            ],
            [
                "module_id" => 16,
                "name" => "Update",
            ],
            [
                "module_id" => 16,
                "name" => "Delete",
            ],
            [
                "module_id" => 17,
                "name" => "Create",
            ],
            [
                "module_id" => 17,
                "name" => "Read",
            ],
            [
                "module_id" => 17,
                "name" => "Update",
            ],
            [
                "module_id" => 17,
                "name" => "Delete",
            ],
            [
                "module_id" => 17,
                "name" => "Show",
            ],
            [
                "module_id" => 18,
                "name" => "Create",
            ],
            [
                "module_id" => 18,
                "name" => "Read",
            ],
            [
                "module_id" => 18,
                "name" => "Update",
            ],
            [
                "module_id" => 18,
                "name" => "Delete",
            ],
        );
        foreach($modules AS $m){
            Module::create([
                'module_id' => $m['module_id'],
                'name' => $m['name'],
                'icon' => $m['icon'],
                'url' => $m['url'],
                'is_show' => $m['is_show'],
            ]);
        }
        foreach($privilege AS $m){
            ModulePrivilege::create([
                'module_id' => $m['module_id'],
                'name' => $m['name'],
            ]);
        }
        foreach($department AS $d){
            Department::create([
                'name' => $d['name']
            ]);
        }
        foreach($position AS $r){
            Position::create([
                // 'permission_id' => $r['permission_id'],
                'department_id' => $r['department_id'],
                'name' => $r['name']
            ]);
        }
        // foreach($permission AS $r){
        //     Permission::create([
        //         'name' => $r['name']
        //     ]);
        // }
        foreach($employee AS $e){
            Employee::create([
                'nip' => $e['nip'],
                'name' => $e['name'],
                'email' => $e['email'],
                'phone' => $e['phone'],
                'jobdesc' => $e['jobdesc'],
                'impression' => $e['impression'],
                'date_birth' => $e['date_birth'],
                'bio' => $e['bio'],
                'address' => $e['address'],
                'postcode' => $e['postcode'],
                'department_id' => $e['department_id'],
                'position_id' => $e['position_id'],
                'st' => $e['st'],
                'password' => $e['password']
            ]);
        }
    }
}
