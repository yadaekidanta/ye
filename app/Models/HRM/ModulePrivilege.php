<?php

namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModulePrivilege extends Model
{
    use HasFactory;
    public $timestamps = false;
}
