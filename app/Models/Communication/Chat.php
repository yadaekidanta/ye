<?php

namespace App\Models\Communication;

use App\Models\HRM\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Chat extends Model
{
    use HasFactory;
    public function pengirim ()
    {
        return $this->belongsTo(Employee::class,'from_id','id');
    }
}
