<x-web-layout title="{{__('menu.career')}}">
    <section class="wrapper bg-soft-primary position-relative">
        <div class="container pt-10 pb-17 pt-md-14 text-center">
            <div class="row">
                <div class="col-lg-8 mx-auto mb-5">
                    <h1 class="fs-15 text-uppercase text-muted mb-3">Join Our Team</h1>
                    <h3 class="display-1 mb-6">Happy Employees Equal To Great Company</h3>
                    <a href="#" class="btn btn-lg btn-primary rounded-pill">Explore Positions</a>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-light mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 60">
                <path fill="currentColor" d="M0,0V60H1440V0A5771,5771,0,0,1,0,0Z" />
                </svg>
            </div>
        </div>
    </section>
    <!-- /section -->
    <section class="wrapper bg-light">
        <div class="container pb-15 pb-md-17">
            <div class="row gx-md-5 gy-5 mt-n18 mt-md-n19">
                <div class="col-md-6 col-xl-3">
                    <div class="card lift">
                        <div class="card-body">
                            <img src="{{asset('sandbox/img/icons/solid/target.svg')}}" class="svg-inject icon-svg icon-svg-sm solid-mono text-primary mb-3" alt="" />
                            <h4>Career Growth</h4>
                            <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus cras justo.</p>
                        </div>
                        <!--/.card-body -->
                    </div>
                <!--/.card -->
                </div>
                <!--/column -->
                <div class="col-md-6 col-xl-3">
                    <div class="card lift">
                        <div class="card-body">
                            <img src="{{asset('sandbox/img/icons/solid/videocall.svg')}}" class="svg-inject icon-svg icon-svg-sm solid-mono text-primary mb-3" alt="" />
                            <h4>Work From Anywhere</h4>
                            <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus cras justo.</p>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                </div>
                <!--/column -->
                <div class="col-md-6 col-xl-3">
                    <div class="card lift">
                        <div class="card-body">
                            <img src="{{asset('sandbox/img/icons/solid/wallet.svg')}}" class="svg-inject icon-svg icon-svg-sm solid-mono text-primary mb-3" alt="" />
                            <h4>Smart Salary</h4>
                            <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus cras justo.</p>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                </div>
                <!--/column -->
                <div class="col-md-6 col-xl-3">
                    <div class="card lift">
                        <div class="card-body">
                            <img src="{{asset('sandbox/img/icons/solid/alarm.svg')}}" class="svg-inject icon-svg icon-svg-sm solid-mono text-primary mb-3" alt="" />
                            <h4>Flexible Hours</h4>
                            <p class="mb-0">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus cras justo.</p>
                        </div>
                        <!--/.card-body -->
                    </div>
                    <!--/.card -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <div class="row gy-10 gy-sm-13 gx-md-8 gx-xl-12 align-items-center mt-12">
                <div class="col-lg-6">
                    <div class="row gx-md-5 gy-5">
                        <div class="col-12">
                            <figure class="rounded mx-md-5"><img src="{{asset('sandbox/img/photos/g8.jpg')}}" srcset="./assets/img/photos/g8@2x.jpg 2x" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g9.jpg')}}" srcset="./assets/img/photos/g9@2x.jpg 2x" alt=""></figure>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <figure class="rounded"><img src="{{asset('sandbox/img/photos/g10.jpg')}}" srcset="./assets/img/photos/g10@2x.jpg 2x" alt=""></figure>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
                <div class="col-lg-6">
                    <h2 class="fs-16 text-uppercase text-muted mb-3 mt-md-n5">Why work here</h2>
                    <h3 class="display-3 mb-5">YE as a software company is empowered by the like minded and the highly spirited talents of Indonesia.</h3>
                    <p class="mb-6">That, together with our culture, is the main reason we have been able to survive and thrive. Our mission is to make Indonesian businesses more successful, creating stronger economy. Are you with us?</p>
                    <div class="row gy-8">
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{asset('sandbox/img/icons/solid/share.svg')}}" class="svg-inject icon-svg icon-svg-xs solid-mono text-primary me-4" alt="" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Our Mission</h4>
                                    <p class="mb-0">Curabitur blandit lacus porttitor ridiculus mus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                        <div class="col-md-6">
                            <div class="d-flex flex-row">
                                <div>
                                    <img src="{{asset('sandbox/img/icons/solid/partnership.svg')}}" class="svg-inject icon-svg icon-svg-xs solid-mono text-primary me-4" alt="" />
                                </div>
                                <div>
                                    <h4 class="mb-1">Our Values</h4>
                                    <p class="mb-0">Curabitur blandit lacus porttitor ridiculus mus.</p>
                                </div>
                            </div>
                        </div>
                        <!--/column -->
                    </div>
                    <!--/.row -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
            <hr class="my-14 my-md-17" />
            <div class="row text-center">
                <div class="col-xl-10 mx-auto">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Job Positions</h2>
                    <h3 class="display-4 mb-10 px-xxl-15">We’re always searching for amazing people to join our team. Take a look at our current openings.</h3>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
            <div id="content_list">
                <div class="row">
                    <div class="col-xl-10 mx-auto">
                        <form class="filter-form mb-10" id="content_filter">
                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="form-select-wrapper">
                                        <select class="form-select" name="keyword" onchange="load_list(1);" aria-label="">
                                            <option value="" selected>Department</option>
                                            @foreach($department as $d)
                                                <option value="{{$d->id}}">{{$d->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div id="list_result"></div>
                    </div>
                    <!-- /column -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    <section class="wrapper bg-soft-primary">
        <div class="container py-14 py-md-16">
            <div class="row">
                <div class="col-lg-10 col-xl-9 col-xxl-8 mx-auto text-center">
                    <h2 class="fs-15 text-uppercase text-muted mb-3">Can't find the right position?</h2>
                    <h3 class="display-4 mb-7 px-lg-5 px-xl-0 px-xxl-5">We are a community with 5000+ team members. Join and build the future with us.</h3>
                    <a href="#" class="btn btn-lg btn-primary rounded-pill">Contact Us</a>
                </div>
                <!-- /column -->
            </div>
            <!-- /.row -->
        </div>
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.container -->
    </section>
    <!-- /section -->
    @section('custom_js')
        <script>
            load_list(1);
        </script>
    @endsection
</x-web-layout>