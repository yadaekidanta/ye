<?php

namespace App\Http\Controllers\Office\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\ChangelogDetail;
use Illuminate\Http\Request;

class ChangelogDetailController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.changelog.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(ChangelogDetail $changelogDetail)
    {
        //
    }
    public function edit(ChangelogDetail $changelogDetail)
    {
        //
    }
    public function update(Request $request, ChangelogDetail $changelogDetail)
    {
        //
    }
    public function destroy(ChangelogDetail $changelogDetail)
    {
        //
    }
    public function list()
    {
        return view('pages.office.changelog.list');
    }
}
