<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Office\AuthController;
use App\Http\Controllers\Office\ProfileController;
use App\Http\Controllers\Office\CRM\LeadController;
use App\Http\Controllers\Office\HRM\MemoController;
use App\Http\Controllers\Office\DashboardController;
use App\Http\Controllers\Office\CRM\ClientController;
use App\Http\Controllers\Office\HRM\ModuleController;
use App\Http\Controllers\Office\Master\BankController;
use App\Http\Controllers\Office\Master\IsicController;
use App\Http\Controllers\Office\CRM\CustomerController;
use App\Http\Controllers\Office\HRM\EmployeeController;
use App\Http\Controllers\Office\HRM\PositionController;
use App\Http\Controllers\Office\Master\EventController;
use App\Http\Controllers\Office\Corporate\KpiController;
use App\Http\Controllers\Office\DocumentationController;
use App\Http\Controllers\Office\Todo\TodoListController;
use App\Http\Controllers\Office\HRM\DepartmentController;
use App\Http\Controllers\Office\HRM\PermissionController;
use App\Http\Controllers\Office\Master\IsicTypeController;
use App\Http\Controllers\Office\CRM\ContactGroupController;
use App\Http\Controllers\Office\Master\ChangelogController;
use App\Http\Controllers\Office\Master\DirectoryController;
use App\Http\Controllers\Office\Regional\CountryController;
use App\Http\Controllers\Office\Regional\RegencyController;
use App\Http\Controllers\Office\Regional\VillageController;
use App\Http\Controllers\Office\Corporate\HistoryController;
use App\Http\Controllers\Office\Regional\DistrictController;
use App\Http\Controllers\Office\Regional\ProvinceController;
use App\Http\Controllers\Office\Communication\ChatController;
use App\Http\Controllers\Office\Corporate\LegalDocTypeController;

Route::group(['domain' => ''], function() {
    Route::redirect('/','dashboard',301);
    Route::prefix('office')->name('office.')->group(function(){
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::post('forgot',[AuthController::class, 'do_forgot'])->name('forgot');
            Route::get('reset/{token}',[AuthController::class, 'reset'])->name('reset');
            Route::post('reset',[AuthController::class, 'do_reset'])->name('do_reset');
        });
        Route::middleware(['auth:office'])->group(function(){
            Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::name('dashboard.')->group(function(){
                Route::get('dashboard-ecommerce', [DashboardController::class, 'ecommerce'])->name('ecommerce');
                Route::get('dashboard-project', [DashboardController::class, 'project'])->name('project');
                Route::get('dashboard-marketing', [DashboardController::class, 'marketing'])->name('marketing');
                Route::get('dashboard-analytic', [DashboardController::class, 'analytic'])->name('analytic');
                Route::get('dashboard-finance', [DashboardController::class, 'finance'])->name('finance');
            });
            Route::get('todo/list',[TodoListController::class, 'list'])->name('todo.list');
            Route::resource('todo', TodoListController::class);
            Route::get('memo/list',[MemoController::class, 'list'])->name('memos.list');
            Route::resource('memo', MemoController::class);
            Route::get('event/list',[EventController::class, 'list'])->name('event.list');
            Route::resource('event', EventController::class);
            Route::get('directory/list',[DirectoryController::class, 'list'])->name('directory.list');
            Route::resource('directory', DirectoryController::class);
            Route::name('master.')->group(function(){
                Route::get('bank/list',[BankController::class, 'list'])->name('bank.list');
                Route::resource('bank', BankController::class);
                Route::get('isic-type/list',[IsicTypeController::class, 'list'])->name('isic-type.list');
                Route::get('isic-type/{isicType}/show-list',[IsicTypeController::class, 'show_list'])->name('isic-type.show_list');
                Route::get('isic-type/{isicType}/show-create',[IsicTypeController::class, 'show_create'])->name('isic-type.show_create');
                Route::get('isic-type/{isicType}/{isic}/show-edit',[IsicTypeController::class, 'show_edit'])->name('isic-type.show_edit');
                Route::resource('isic-type', IsicTypeController::class);
                Route::resource('isic', IsicController::class);
            });
            Route::name('regional.')->group(function(){
                Route::get('country/list',[CountryController::class, 'list'])->name('country.list');
                Route::get('country/{country}/show-list',[CountryController::class, 'show_list'])->name('country.show_list');
                Route::get('country/{country}/show-create',[CountryController::class, 'show_create'])->name('country.show_create');
                Route::get('country/{country}/{province}/show-edit',[CountryController::class, 'show_edit'])->name('country.show_edit');
                Route::resource('country', CountryController::class);
                Route::get('province/list',[ProvinceController::class, 'list'])->name('province.list');
                Route::get('province/{province}/show-list',[ProvinceController::class, 'show_list'])->name('province.show_list');
                Route::get('province/{province}/show-create',[ProvinceController::class, 'show_create'])->name('province.show_create');
                Route::get('province/{province}/{regency}/show-edit',[ProvinceController::class, 'show_edit'])->name('province.show_edit');
                Route::resource('province', ProvinceController::class);
                Route::get('regency/list',[RegencyController::class, 'list'])->name('regency.list');
                Route::get('regency/{regency}/show-list',[RegencyController::class, 'show_list'])->name('regency.show_list');
                Route::get('regency/{regency}/show-create',[RegencyController::class, 'show_create'])->name('regency.show_create');
                Route::get('regency/{regency}/{district}/show-edit',[RegencyController::class, 'show_edit'])->name('regency.show_edit');
                Route::resource('regency', RegencyController::class);
                Route::get('district/list',[DistrictController::class, 'list'])->name('district.list');
                Route::get('district/{district}/show-list',[DistrictController::class, 'show_list'])->name('district.show_list');
                Route::get('district/{district}/show-create',[DistrictController::class, 'show_create'])->name('district.show_create');
                Route::get('district/{district}/{village}/show-edit',[DistrictController::class, 'show_edit'])->name('district.show_edit');
                Route::resource('district', DistrictController::class);
                Route::get('village/list',[VillageController::class, 'list'])->name('village.list');
                Route::resource('village', VillageController::class);
            });
            Route::name('hrm.')->group(function(){
                Route::get('employee/list',[EmployeeController::class, 'list'])->name('employee.list');
                Route::get('employee/{employee}/show-list',[EmployeeController::class, 'show_list'])->name('employee.show_list');
                Route::post('employee/upload',[EmployeeController::class, 'upload_cv'])->name('employee.upload_cv');
                Route::resource('employee', EmployeeController::class);
                Route::get('department/list',[DepartmentController::class, 'list'])->name('department.list');
                Route::get('department/{department}/show-list',[DepartmentController::class, 'show_list'])->name('department.show_list');
                Route::get('department/{department}/show-create',[DepartmentController::class, 'show_create'])->name('department.show_create');
                Route::get('position/{position}/input-permission',[DepartmentController::class, 'input_permission'])->name('department.input_permission');
                Route::get('department/{department}/{position}/show-edit',[DepartmentController::class, 'show_edit'])->name('department.show_edit');
                Route::resource('department', DepartmentController::class);
                Route::get('position/list',[PositionController::class, 'list'])->name('position.list');
                Route::get('position/{position}/show-list',[PositionController::class, 'show_list'])->name('position.show_list');
                Route::get('position/{position}/show-create',[PositionController::class, 'show_create'])->name('position.show_create');
                Route::get('position/{position}/{permission}/show-edit',[PositionController::class, 'show_edit'])->name('position.show_edit');
                Route::resource('position', PositionController::class);
                Route::get('permission/list',[PermissionController::class, 'list'])->name('permission.list');
                Route::get('permission/{permission}/show-list',[PermissionController::class, 'show_list'])->name('permission.show_list');
                Route::get('permission/{permission}/show-create',[PermissionController::class, 'show_create'])->name('permission.show_create');
                Route::get('permission/{permission}/{permissionDetail}/show-edit',[PermissionController::class, 'show_edit'])->name('permission.show_edit');
                Route::resource('permission', PermissionController::class);
                Route::get('module/list',[ModuleController::class, 'list'])->name('module.list');
                Route::get('module/{module}/show-list',[ModuleController::class, 'show_list'])->name('module.show_list');
                Route::get('module/{module}/show-create',[ModuleController::class, 'show_create'])->name('module.show_create');
                Route::get('module/{module}/{modulePrivilege}/show-edit',[ModuleController::class, 'show_edit'])->name('module.show_edit');
                Route::resource('module', ModuleController::class);
            });
            Route::name('crm.')->group(function(){
                Route::get('group/list',[ContactGroupController::class, 'list'])->name('group.list');
                Route::resource('group', ContactGroupController::class);
                Route::get('lead/list',[LeadController::class, 'list'])->name('lead.list');
                Route::get('lead/{lead}/show-list',[LeadController::class, 'show_list'])->name('lead.show_list');
                Route::get('lead/{lead}/show-create',[LeadController::class, 'show_create'])->name('lead.show_create');
                // Route::get('lead/{lead}/{village}/show-edit',[LeadController::class, 'show_edit'])->name('lead.show_edit');
                Route::resource('lead', LeadController::class);
                Route::resource('client', ClientController::class);
                Route::resource('customer', CustomerController::class);
            });
            Route::name('corporate.')->group(function(){
                Route::get('history/list',[HistoryController::class, 'list'])->name('history.list');
                Route::resource('history', HistoryController::class);
                Route::get('document-type/list',[LegalDocTypeController::class, 'list'])->name('document-type.list');
                Route::get('document-type/{documentType}/show-list',[LegalDocTypeController::class, 'show_list'])->name('document-type.show_list');
                Route::get('document-type/{documentType}/show-create',[LegalDocTypeController::class, 'show_create'])->name('document-type.show_create');
                Route::get('document-type/{documentType}/{document}/show-edit',[LegalDocTypeController::class, 'show_edit'])->name('document-type.show_edit');
                Route::resource('document-type', LegalDocTypeController::class);
                Route::resource('document', DocumentController::class);
                Route::resource('policy', PolicyController::class);
                Route::get('kpi/list',[KpiController::class, 'list'])->name('kpi.list');
                Route::resource('kpi', KpiController::class);
            });
            Route::name('finance.')->group(function(){
                Route::resource('coa-type', CoaTypeController::class);
                Route::resource('cash-flow', CashFlowController::class);
            });
            Route::resource('career', CareerController::class);
            Route::resource('project', ProjectController::class);
            Route::name('ecommerce.')->group(function(){
                Route::resource('product', ProductController::class);
                Route::resource('category', ProductCategoryController::class);
                Route::resource('sale', SaleController::class);
            });
            Route::name('support.')->group(function(){
                Route::resource('ticket', TicketController::class);
                Route::resource('faq', FaqCategoryController::class);
                Route::resource('license', LicenseController::class);
                Route::resource('inbox', InboxController::class);
            });
            Route::resource('subscribe', SubscribeController::class);
            Route::name('communication.')->group(function(){
                Route::get('counter-chat', [ChatController::class, 'counter'])->name('counter_chat');
                Route::get('chat/list',[ChatController::class, 'list'])->name('chat.list');
                Route::resource('chat', ChatController::class);
            });
            Route::name('profile.')->group(function(){
                Route::get('profile', [ProfileController::class, 'index'])->name('index');
                Route::get('overview', [ProfileController::class, 'overview'])->name('overview');
                Route::get('setting', [ProfileController::class, 'index_setting'])->name('setting');
                Route::get('view_setting', [ProfileController::class, 'setting'])->name('view_setting');
                Route::get('security', [ProfileController::class, 'index_security'])->name('security');
                Route::get('view_security', [ProfileController::class, 'security'])->name('view_security');
                Route::get('billing', [ProfileController::class, 'index_billing'])->name('billing');
                Route::get('view_billing', [ProfileController::class, 'billing'])->name('view_billing');
                Route::get('statement', [ProfileController::class, 'index_statement'])->name('statement');
                Route::get('view_statement', [ProfileController::class, 'statement'])->name('view_statement');
                Route::get('referral', [ProfileController::class, 'index_referral'])->name('referral');
                Route::get('view_referral', [ProfileController::class, 'referral'])->name('view_referral');
                Route::get('log', [ProfileController::class, 'index_log'])->name('log');
                Route::get('view_log', [ProfileController::class, 'log'])->name('view_log');
                Route::get('my-project', [ProfileController::class, 'index_project'])->name('project');
                Route::get('view_project', [ProfileController::class, 'project'])->name('view_project');
                Route::get('campaign', [ProfileController::class, 'index_campaign'])->name('campaign');
                Route::get('view_campaign', [ProfileController::class, 'campaign'])->name('view_campaign');
                Route::get('follower', [ProfileController::class, 'index_follower'])->name('follower');
                Route::get('view_follower', [ProfileController::class, 'follower'])->name('view_follower');
                Route::get('activity', [ProfileController::class, 'index_activity'])->name('activity');
                Route::get('view_activity', [ProfileController::class, 'activity'])->name('view_activity');
            });
            Route::name('documentation.')->group(function(){
                Route::get('documentation',[DocumentationController::class, 'index'])->name('index');
                Route::get('documentation/list',[DocumentationController::class, 'list'])->name('list');
                Route::get('documentation/install',[DocumentationController::class, 'install'])->name('install');
                Route::get('documentation/install/list',[DocumentationController::class, 'install_list'])->name('install.list');
                Route::get('documentation/utility',[DocumentationController::class, 'utility'])->name('utility');
                Route::get('documentation/utility/list',[DocumentationController::class, 'utility_list'])->name('utility.list');
                Route::get('documentation/component',[DocumentationController::class, 'component'])->name('component');
                Route::get('documentation/component/list',[DocumentationController::class, 'component_list'])->name('component.list');
                Route::get('documentation/plugin',[DocumentationController::class, 'plugin'])->name('plugin');
                Route::get('documentation/plugin/list',[DocumentationController::class, 'plugin_list'])->name('plugin.list');
                Route::get('documentation/icon',[DocumentationController::class, 'icon'])->name('icon');
                Route::get('documentation/icon/list',[DocumentationController::class, 'icon_list'])->name('icon.list');
                Route::get('documentation/illustration',[DocumentationController::class, 'illustration'])->name('illustration');
                Route::get('documentation/illustration/list',[DocumentationController::class, 'illustration_list'])->name('illustration.list');
            });
            Route::get('changelog',[ChangelogController::class, 'index'])->name('changelog.index');
            Route::get('changelog/list',[ChangelogController::class, 'list'])->name('changelog.list');
            // Route::get('counter-notif', [NotificationController::class, 'counter'])->name('counter_notif');
            // Route::get('notification', [NotificationController::class, 'index'])->name('notification');
            // HRM
            
            // PROFILE
            // Route::resource('profile', ProfileController::class);
            // Route::resource('my-applicants', JobApplicantsController::class);
            // Route::resource('job-applicants', JobApplicationController::class);
            // Route::patch('job_applicant/{job_applicant}/reject',[JobApplicationController::class, 'reject'])->name('job-applicants.reject');
            // Route::patch('job_applicant/{job_applicant}/accept',[JobApplicationController::class, 'accept'])->name('job-applicants.accept');
            
            // Legality
            // Route::resource('legal-doc', LegalDocController::class);
            // Route::resource('legal-doc-type', LegalDocTypeController::class);
            // Route::resource('legal-policy', LegalPolicyController::class);
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
    });
});