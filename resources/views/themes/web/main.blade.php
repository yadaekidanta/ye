<!DOCTYPE html>
<html lang="en">
	@include('themes.web.head')
	<body>
		<div class="content-wrapper">
			@include('themes.web.header')
			<!-- /header -->
			{{$slot}}
			<!-- /section -->
		</div>
		<!-- /.content-wrapper -->
		@include('themes.web.footer')
		@include('themes.web.progress')
		@include('themes.web.js')
		@yield('custom_js')
	</body>
</html>