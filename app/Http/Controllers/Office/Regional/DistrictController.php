<?php

namespace App\Http\Controllers\Office\Regional;

use Illuminate\Http\Request;
use App\Models\Regional\Village;
use App\Models\Regional\District;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            return view('pages.office.regional.district.main');
        }
        return view('pages.office.theme');
    }
    public function create()
    {
        return view('pages.office.regional.district.input', ['data' => new District]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Request $request, District $district)
    {
        if($request->ajax())
        {
            return view('pages.office.regional.district.show',compact('district'));
        }
        return view('pages.office.theme');
    }
    public function edit(District $district)
    {
        return view('pages.office.regional.district.input', ['data' => $district]);
    }
    public function update(Request $request, District $district)
    {
        //
    }
    public function destroy(District $district)
    {
        //
    }
    public function list(Request $request)
    {
        $collection = District::where('id','LIKE','%'.$request->keyword.'%')->orWhere('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.regional.district.list',compact('collection'));
    }
    public function show_list(Request $request, District $district)
    {
        $collection = Village::where('district_id',$district->id)->where('name','LIKE','%'.$request->keyword.'%')->paginate(10);
        return view('pages.office.regional.district.show_list',compact('collection'));
    }
    public function show_create(District $district)
    {
        return view('pages.office.regional.district.show_input', ['data' => new Village, 'district' => $district]);
    }
    public function show_edit(District $district, Village $village)
    {
        return view('pages.office.regional.district.show_input', ['data' => $village, 'district' => $district]);
    }
}
