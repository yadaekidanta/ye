<x-web-layout title="{{__('menu.home')}}">
    <section class="wrapper bg-light">
        <div class="container pt-6 pb-14 pb-md-16">
            <div class="row gx-lg-8 gx-xl-12 gy-10">
                <div class="col-lg-6 mb-0">
                    <h2 class="fs-16 text-uppercase text-primary mb-4">WA YE</h2>
                    <img src="data:image/png;base64, {{base64_encode( \SimpleSoftwareIO\QrCode\Facades\QrCode::errorCorrection('H')->style('round')->format('png')->merge(asset('img/icon.png'),0.2,true)->size(300)->generate('https://wa.me/6287748005611&text=Halo, Saya ingin mengetahui tentang Yada Ekidanta', public_path('qr/ye.png')) ) }}">
                </div>
                <div class="col-lg-6 mb-0">
                    <h2 class="fs-16 text-uppercase text-primary mb-4">WA Employee</h2>
                    <img src="data:image/png;base64, {{base64_encode( \SimpleSoftwareIO\QrCode\Facades\QrCode::errorCorrection('H')->style('dot')->format('png')->merge(asset('img/icon.png'),0.2,true)->size(300)->generate('https://wa.me/6287709020299&text=Halo, Saya ingin mengetahui tentang ... ', public_path('qr/misterrizky.png')) ) }}">
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!-- /.container -->
        <div class="overflow-hidden">
            <div class="divider text-navy mx-n2">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 100">
                    <path fill="currentColor" d="M1260,1.65c-60-5.07-119.82,2.47-179.83,10.13s-120,11.48-180,9.57-120-7.66-180-6.42c-60,1.63-120,11.21-180,16a1129.52,1129.52,0,0,1-180,0c-60-4.78-120-14.36-180-19.14S60,7,30,7H0v93H1440V30.89C1380.07,23.2,1319.93,6.15,1260,1.65Z" />
                </svg>
            </div>
        </div>
        <!-- /.overflow-hidden -->
    </section>
</x-web-layout>